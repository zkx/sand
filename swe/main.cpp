#include <iostream>
#include <stdio.h>
#include <fstream>
#include "stdlib.h"
#include "types.h"
#include "ppm_reader.h"
#include "glwindow.h"
#include "math.h"
#include "simu.h"
#include "landscapecreator.h"
#include "timing.h"
#include "sim.h"


int gridsize;
int simulate;
int stepperframe;
int kernelflops;

vertex *landscape;
vertex *wave;
float *waveheights;
rgb *colors;

int argc;
char ** argv;

sim moni;
void update(vertex* wave_vertices, rgb* wave_colors, bool renderOnly)
{
	moni.compute_exp(wave_vertices, wave_colors, renderOnly);//显式
	//moni.compute(wave_vertices, wave_colors, renderOnly);//迎风
	//moni.compute1(wave_vertices, wave_colors, renderOnly);//中心
	//moni.compute2(wave_vertices, wave_colors, renderOnly);//迎风预测
	//moni.compute3(wave_vertices, wave_colors, renderOnly);//中心预测
}

void shutdown()
{
	moni.destroy();
    free(landscape);
    free(wave);
    free(waveheights);
    free(colors);
    exit(0);
}

void restart()
{
	moni.destroy();
	moni.init();
}

void start()
{
	moni.init();
    if (simulate == 0)
    {
        createWindow(argc, argv, 800, 600 , &update, &restart, &shutdown, kernelflops, moni);
    }
    else
    {
        initTimer();
        for ( int step = 0; step < simulate; step++)
        {
            update(wave, colors, false);
        }
        float runtime = timeSinceInit() / 1000.0f;

        long kernelcalls = simulate * stepperframe;
        long threads = gridsize * gridsize;
        long flops = kernelcalls * threads * kernelflops;

        std::cout << "Gridsize:" << gridsize << "x" << gridsize << std::endl;
        std::cout << "Launched the main kernel " << kernelcalls << " times." << std::endl;
        printf("Execution time: %2.4fs\n", runtime);
        if(flops > 0)
        {
            std::cout << "Total computed flops: " << flops << std::endl;
            printf("Speed: %6.2f GFlop/s\n", flops / runtime / 1000000000);
        }

    }
}

int main(int ac, char ** av)
{
    argc = ac;
    argv = av;

	gridsize = 256;// FLAGS_gridsize;
	simulate = 0;// FLAGS_simulate;
	stepperframe = 10;// FLAGS_wspf;
	kernelflops = 0;// FLAGS_kernelflops;


    float running_time;
    std::string landscape_filename, landscape_color_filename, wave_filename, colors_filename;

	running_time = 30.0f;
	landscape_filename = "res/landscape/heightmap.ppm";
	landscape_color_filename = "res/landscape/texture.ppm";
	wave_filename = "res/landscape/wave.ppm";

    rgb *colors_img;
    int colors_width;
    int colors_height;

    rgb *wave_img;
    int wave_width;
    int wave_height;

    int heightmapheight;
    int heightmapwidth;


    readPPM(wave_filename.c_str(), wave_img, wave_width, wave_height);

    readPPM(landscape_color_filename.c_str(), colors_img, colors_width, colors_height);

    int landscape_filename_lenght = landscape_filename.length();
    std::string filetype;

    if (landscape_filename_lenght > 4)
        { filetype = landscape_filename.substr(landscape_filename_lenght - 4, landscape_filename_lenght - 1); }

    if (filetype.compare(".ppm") == 0 )
    {
        rgb* landscape_img;
        readPPM(landscape_filename.c_str(), landscape_img, heightmapwidth, heightmapheight);
        createLandscapeFromRGB(landscape_img, heightmapwidth, heightmapheight, gridsize, gridsize, landscape);

        free(landscape_img);
    }

    createLandscapeFromRGB(wave_img, wave_width, wave_height, gridsize, gridsize, wave);

    createWaveHeights(wave_img, wave_width, wave_height, gridsize, gridsize, waveheights);

    createLandscapeColors(colors_img, landscape, colors_width, colors_height, gridsize, gridsize, colors);

    free(colors_img);
    free(wave_img);

	moni.gridsize = gridsize;
	moni.colors = colors;
	moni.steps = stepperframe;
	moni.landscape = landscape;
	moni.waveheights = waveheights;
	moni.wave = wave;

    start();

    return 0;
}
