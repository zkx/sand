#include "sim.h"
#include "simu.h"
sim::sim()
{
}
sim::sim(const int size, const int step, vertex *land, float *waveh, rgb *color, vertex *waves)
{
	gridsize = size;
	steps = step;
	landscape = land;
	waveheights = waveh;
	colors = color;
	wave = waves;
}
sim::~sim()
{
}

void sim::init()
{
	initWaterSurface(gridsize, gridsize, landscape, waveheights);
}

void sim::compute_exp(vertex * watersurfacevertices, rgb * watersurfacecolors, bool renderOnly)
{
	computeNext_exp(gridsize, gridsize, watersurfacevertices, watersurfacecolors, steps, renderOnly);
}

//void sim::compute(vertex * watersurfacevertices, rgb * watersurfacecolors, bool renderOnly)
//{
//	computeNext_imp(gridsize, gridsize, watersurfacevertices, watersurfacecolors, steps, renderOnly);
//}
//
//void sim::compute1(vertex * watersurfacevertices, rgb * watersurfacecolors, bool renderOnly)
//{
//	computeNext_imp1(gridsize, gridsize, watersurfacevertices, watersurfacecolors, steps, renderOnly);
//}
//
//void sim::compute2(vertex * watersurfacevertices, rgb * watersurfacecolors, bool renderOnly)
//{
//	computeNext_imp2(gridsize, gridsize, watersurfacevertices, watersurfacecolors, steps, renderOnly);
//}
//
//void sim::compute3(vertex * watersurfacevertices, rgb * watersurfacecolors, bool renderOnly)
//{
//	computeNext_imp3(gridsize, gridsize, watersurfacevertices, watersurfacecolors, steps, renderOnly);
//}

void sim::destroy()
{
	destroyWaterSurface();
}
