#pragma once

#include "types.h"

void initWaterSurface(int width, int height, vertex* heightmapvertices, float *wave);

void computeNext_exp(int width, int height, vertex* watersurfacevertices, rgb* watersurfacecolors, int steps, bool renderOnly);
//void computeNext_imp(int width, int height, vertex* watersurfacevertices, rgb* watersurfacecolors, int steps, bool renderOnly);
//void computeNext_imp1(int width, int height, vertex* watersurfacevertices, rgb* watersurfacecolors, int steps, bool renderOnly);
//void computeNext_imp2(int width, int height, vertex* watersurfacevertices, rgb* watersurfacecolors, int steps, bool renderOnly);
//void computeNext_imp3(int width, int height, vertex* watersurfacevertices, rgb* watersurfacecolors, int steps, bool renderOnly);

void destroyWaterSurface(); 
