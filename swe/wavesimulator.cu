#include "wavesimulator.h"

#include <iostream>
#include "types.h"
#include <stdlib.h>
#include <stdio.h>
#include"timing.h"

#define BLOCKSIZE_X 16
#define BLOCKSIZE_Y 16

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#define grid2Dwrite(array, x, y, pitch, value) array[(y)*pitch+x] = value
#define grid2Dread(array, x, y, pitch) array[(y)*pitch+x]


#define LIM  20000.0f
#define EPSILON1 0.005f
const int UNINTIALISED = 0;
const int INITIALISED = 1;

int state = UNINTIALISED;


int stepsperframe = 50;

__constant__ float GRAVITY = 9.83219f * 0.5f; //0.5f * Fallbeschleunigung
__constant__ float g = 9.83219f;

__constant__ float NN =4.50f;//水流基本高度，不会比这个值小，土地高度为3~7，一般取5

const float timestep = 0.01f;
const int maxdiedai =40;

texture<gridpoint, 2, cudaReadModeElementType> texture_grid;
texture<vertex, 2, cudaReadModeElementType> texture_landscape;

int grid_pitch_elements;
int height_pitch_elements;
int h_pitch_elements;
cudaChannelFormatDesc grid_channeldesc;
cudaChannelFormatDesc heightmap_channeldesc;

gridpoint* device_grid;
gridpoint* device_grid_next;
gridpoint* particles;

vertex* device_heightmap;
vertex* device_watersurfacevertices;


float* device_waves;
rgb* device_watersurfacecolors;
float* h_bef;


#define EPSILON 0.00001f  //粘稠度

__constant__ float deltax = 1;
__constant__ float deltay = 1;
__constant__ float deltat = 0.01;
__constant__ float manning = 0.020;






__host__ __device__ gridpoint operator +(const gridpoint& x, const gridpoint& y)
{
	gridpoint z;
	z.x = x.x + y.x;
	z.y = x.y + y.y;
	z.z = x.z + y.z;
	z.w = x.w + y.w;
	return z;
}
__host__ __device__ gridpoint operator -(const gridpoint& x, const gridpoint& y)
{
	gridpoint z;
	z.x = x.x - y.x;
	z.y = x.y - y.y;
	z.z = x.z - y.z;
	z.w = x.w - y.w;
	return z;
}
__host__ __device__ gridpoint operator *(const gridpoint& x, const gridpoint& y)
{
	gridpoint z;
	z.x = y.x * x.x;
	z.y = y.y * x.y;
	z.z = y.z * x.z;
	z.w = y.w * x.w;
	return z;
}
__host__ __device__ gridpoint operator *(const gridpoint& x, const float& c)
{
	gridpoint z;
	z.x = c * x.x;
	z.y = c * x.y;
	z.z = c * x.z;
	z.w = c * x.w;
	return z;
}
__host__ __device__ gridpoint operator /(const gridpoint& x, const float& c)
{
	gridpoint z;
	z.x = x.x / c;
	z.y = x.y / c;
	z.z = x.z / c;
	z.w = x.w / c;
	return z;
}
__host__ __device__ gridpoint operator *(const float& c, const gridpoint& x)
{
	return x * c;
}

	
__device__ gridpoint chazhisudu1(float x, float y)
{
	//先求h,w
	int ix = x;
	int iy = y;
	gridpoint h1 = tex2D(texture_grid, ix, iy);
	gridpoint h2 = tex2D(texture_grid, ix + 1, iy);
	gridpoint h3 = tex2D(texture_grid, ix, iy + 1);
	gridpoint h4 = tex2D(texture_grid, ix + 1, iy + 1);
	//X方向插值
	float rh1 = x - ix;
	float hxc1 = (1 - rh1)*h1.x + rh1*h2.x;
	float hxc2 = (1 - rh1)*h3.x + rh1*h4.x;
	float hwc1 = (1 - rh1)*h1.w + rh1*h2.w;
	float hwc2 = (1 - rh1)*h3.w + rh1*h4.w;

	//Y方向差值
	float rh2 = y - iy;
	float hyc = (1 - rh2)*hxc1 + rh2*hxc2;
	float hwc = (1 - rh2)*hwc1 + rh2*hwc2;

	//求u
	float ux = x + 0.5;
	float uy = y;
	int iux = ux;
	int iuy = uy;
	gridpoint n1 = tex2D(texture_grid, iux, iuy);
	gridpoint n2 = tex2D(texture_grid, iux + 1, iuy);
	gridpoint n3 = tex2D(texture_grid, iux, iuy + 1);
	gridpoint n4 = tex2D(texture_grid, iux + 1, iuy + 1);
	//X方向插值
	float r1 = ux - iux;
	float uxc1 = (1 - r1)*n1.y + r1*n2.y;
	float uxc2 = (1 - r1)*n3.y + r1*n4.y;

	//Y方向差值
	float r2 = uy - iuy;
	float uyc = (1 - r2)*uxc1 + r2*uxc2;

	//再求v
	float vx = x;
	float vy = y + 0.5;
	int ivx = vx;
	int ivy = vy;
	gridpoint m1 = tex2D(texture_grid, ivx, ivy);
	gridpoint m2 = tex2D(texture_grid, ivx + 1, ivy);
	gridpoint m3 = tex2D(texture_grid, ivx, ivy + 1);
	gridpoint m4 = tex2D(texture_grid, ivx + 1, ivy + 1);

	//X方向插值
	float t1 = vx - ivx;
	float vxc1 = (1 - t1)*m1.z + t1*m2.z;
	float vxc2 = (1 - t1)*m3.z + t1*m4.z;

	//Y方向差值
	float t2 = vy - ivy;
	float vyc = (1 - t2)*vxc1 + t2*vxc2;
	gridpoint re;
	re.x = hyc;
	re.y = uyc;
	re.z = vyc;
	re.w = hwc;
	return re;

}
__host__ __device__ gridpoint chazhisudu2(gridpoint *grid, float x, float y, int pitch)
{
	//先求h
	int ix = x;
	int iy = y;
	gridpoint h1 = grid2Dread(grid, ix, iy, pitch);
	gridpoint h2 = grid2Dread(grid, ix + 1, iy, pitch);
	gridpoint h3 = grid2Dread(grid, ix, iy + 1, pitch);
	gridpoint h4 = grid2Dread(grid, ix + 1, iy + 1, pitch);
	//X方向插值
	float rh1 = x - ix;
	float hxc1 = (1 - rh1)*h1.x + rh1*h2.x;
	float hxc2 = (1 - rh1)*h3.x + rh1*h4.x;
	float hwc1 = (1 - rh1)*h1.w + rh1*h2.w;
	float hwc2 = (1 - rh1)*h3.w + rh1*h4.w;

	//Y方向差值
	float rh2 = y - iy;
	float hyc = (1 - rh2)*hxc1 + rh2*hxc2;
	float hwc = (1 - rh2)*hwc1 + rh2*hwc2;

	//求u
	float ux = x + 0.5;
	float uy = y;
	int iux = ux;
	int iuy = uy;
	gridpoint n1 = grid2Dread(grid, iux, iuy, pitch);
	gridpoint n2 = grid2Dread(grid, iux + 1, iuy, pitch);
	gridpoint n3 = grid2Dread(grid, iux, iuy + 1, pitch);
	gridpoint n4 = grid2Dread(grid, iux + 1, iuy + 1, pitch);

	//X方向插值
	float r1 = ux - iux;
	float uxc1 = (1 - r1)*n1.y + r1*n2.y;
	float uxc2 = (1 - r1)*n3.y + r1*n4.y;

	//Y方向差值
	float r2 = uy - iuy;
	float uyc = (1 - r2)*uxc1 + r2*uxc2;

	//再求v
	float vx = x;
	float vy = y + 0.5;
	int ivx = vx;
	int ivy = vy;
	gridpoint m1 = grid2Dread(grid, ivx, ivy, pitch);
	gridpoint m2 = grid2Dread(grid, ivx + 1, ivy, pitch);
	gridpoint m3 = grid2Dread(grid, ivx, ivy + 1, pitch);
	gridpoint m4 = grid2Dread(grid, ivx + 1, ivy + 1, pitch);

	//X方向插值
	float t1 = vx - ivx;
	float vxc1 = (1 - t1)*m1.z + t1*m2.z;
	float vxc2 = (1 - t1)*m3.z + t1*m4.z;

	//Y方向差值
	float t2 = vy - ivy;
	float vyc = (1 - t2)*vxc1 + t2*vxc2;
	gridpoint re;
	re.x = hyc;
	re.y = uyc;
	re.z = vyc;
	re.w = hwc;
	return re;

}

__global__ void s_advection1(gridpoint* grid_next, int width, int height, float timestep, int pitch)//网格左上侧的速度值存储在当前网格，右下侧的速度值存储在下一个网格，边界处速度恒为0
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;


	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
		gridpoint westsouth = tex2D(texture_grid, gridx - 1, gridy + 1);
		gridpoint eastnorth = tex2D(texture_grid, gridx + 1, gridy - 1);
		gridpoint eastsouth = tex2D(texture_grid, gridx + 1, gridy + 1);
		//gridpoint westsouthsouth = tex2D(texture_grid, gridx - 1, gridy + 2);
		//gridpoint southsouth = tex2D(texture_grid, gridx, gridy + 2);
		//gridpoint easteastnorth = tex2D(texture_grid, gridx + 2, gridy - 1);
		//gridpoint easteast = tex2D(texture_grid, gridx + 2, gridy);

		float u_next, v_next;
		float u = center.y;
		float v = center.z;
		//X方向
		float cha_v = (west.z + center.z + south.z + westsouth.z) / 4;
		float udx = u*timestep;
		float udy = cha_v*timestep;
		float ux = gridx - 0.5 - udx;
		float uy = gridy - udy;
		gridpoint sdx = chazhisudu1(ux, uy);
		float u_new = (sdx.y + u) / 2;
		float cha_v_new = (sdx.z + cha_v) / 2;
		float ux_new = gridx - 0.5 - u_new*timestep;
		float uy_new = gridy - cha_v_new*timestep;
		gridpoint sdx_new = chazhisudu1(ux_new, uy_new);

		u_next = sdx_new.y;
		//Y方向
		float cha_u = (center.y + north.y + east.y + eastnorth.y) / 4;
		float vdy = v*timestep;
		float vdx = cha_u*timestep;
		float vx = gridx - vdx;
		float vy = gridy - 0.5 - vdy;
		gridpoint sdy = chazhisudu1(vx, vy);
		float v_new = (sdy.z + v) / 2;
		float cha_u_new = (sdy.y + cha_u) / 2;
		float vx_new = gridx - cha_u_new*timestep;
		float vy_new = gridy - 0.5 - v_new*timestep;
		gridpoint sdy_new = chazhisudu1(vx_new, vy_new);

		v_next = sdy_new.z;

		/*if ((u_next > 0 && west.x <= EPSILON1)||(u_next<0&&center.x<=EPSILON1))
		{
		u_next = 0;
		}
		if ((v_next > 0 && north.x <= EPSILON1)||(v_next<0&&center.x<=EPSILON1))
		{
		v_next = 0;
		}*/
		

		gridpoint u_center;
		u_center.x = center.x;
		u_center.y = u_next;
		u_center.z = v_next;
		u_center.w = center.w;
		/*	float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
		u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
		u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
		u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
		u_center.z = 0;
		}*/

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void s_advection2(gridpoint* grid, gridpoint* grid_next, int width, int height, float timestep, int pitch)//网格左上侧的速度值存储在当前网格，右下侧的速度值存储在下一个网格，边界处速度恒为0
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;


	if (x < width &&y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
		gridpoint westsouth = tex2D(texture_grid, gridx - 1, gridy + 1);
		gridpoint eastnorth = tex2D(texture_grid, gridx + 1, gridy - 1);
		gridpoint eastsouth = tex2D(texture_grid, gridx + 1, gridy + 1);
		gridpoint center_ = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north_ = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west_ = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south_ = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east_ = grid2Dread(grid, gridx + 1, gridy, pitch);
		gridpoint westsouth_ = grid2Dread(grid, gridx - 1, gridy + 1, pitch);
		gridpoint eastnorth_ = grid2Dread(grid, gridx + 1, gridy - 1, pitch);
		gridpoint westnorth_ = grid2Dread(grid, gridx - 1, gridy - 1, pitch);
		//gridpoint northnorth_ = grid2Dread(grid, gridx, gridy - 2, pitch);
		//gridpoint eastnorthnorth_ = grid2Dread(grid, gridx + 1, gridy - 2, pitch);

		float u_next, v_next;
		float u = center.y;
		float u_ = center_.y;
		float v = center.z;
		float v_ = center_.z;



		//X方向
		float cha_v_ = (west_.z + center_.z + south_.z + westsouth_.z) / 4;
		float udx = u_*timestep;
		float udy = cha_v_*timestep;
		float ux = gridx - 0.5 + udx;
		float uy = gridy + udy;
		gridpoint sdx = chazhisudu2(grid, ux, uy, pitch);
		float u_new = (sdx.y + u_) / 2;
		float cha_v_new = (sdx.z + cha_v_) / 2;
		float ux_new = gridx - 0.5 + u_new*timestep;
		float uy_new = gridy + cha_v_new*timestep;
		gridpoint sdx_new = chazhisudu2(grid, ux_new, uy_new, pitch);

		float wuchax = (u - sdx_new.y) / 2;//u是当前n时刻速度，u_是利用半拉格朗日获得的n+1时刻速度，sdx_new.y是根据u_利用半拉格朗日获得的n时刻速度，相减为误差


		u_next = u_ + wuchax;

		float cha_v = (west.z + center.z + south.z + westsouth.z) / 4;
		udx = u*timestep;
		udy = cha_v*timestep;
		ux = gridx - udx;
		uy = gridy - udy;
		int iux = ux;
		int iuy = uy;
		gridpoint n1 = tex2D(texture_grid, iux, iuy);
		gridpoint n2 = tex2D(texture_grid, iux + 1, iuy);
		gridpoint n3 = tex2D(texture_grid, iux, iuy + 1);
		gridpoint n4 = tex2D(texture_grid, iux + 1, iuy + 1);
		float minu = min(n1.y, min(n2.y, min(n3.y, n4.y)));
		float maxu = max(n1.y, max(n2.y, max(n3.y, n4.y)));
		if (u_next > maxu || u_next < minu)
		{
			u_next = u_;
		}




		//Y方向
		float cha_u_ = (center_.y + north_.y + east_.y + eastnorth_.y) / 4;
		float vdy = v_*timestep;
		float vdx = cha_u_*timestep;
		float vx = gridx + vdx;
		float vy = gridy - 0.5 + vdy;
		gridpoint sdy = chazhisudu2(grid, vx, vy, pitch);
		float v_new = (sdy.z + v_) / 2;
		float cha_u_new = (sdy.y + cha_u_) / 2;
		float vx_new = gridx + cha_u_new*timestep;
		float vy_new = gridy - 0.5 + v_new*timestep;
		gridpoint sdy_new = chazhisudu2(grid, vx_new, vy_new, pitch);

		float wuchay = (v - sdy_new.z) / 2;



		v_next = v_ + wuchay;

		float cha_u = (center.y + north.y + east.y + eastnorth.y) / 4;
		vdy = v*timestep;
		vdx = cha_u*timestep;
		vx = gridx - vdx;
		vy = gridy - vdy;
		int ivx = vx;
		int ivy = vy;
		gridpoint m1 = tex2D(texture_grid, ivx, ivy);
		gridpoint m2 = tex2D(texture_grid, ivx + 1, ivy);
		gridpoint m3 = tex2D(texture_grid, ivx, ivy + 1);
		gridpoint m4 = tex2D(texture_grid, ivx + 1, ivy + 1);

		float minv = min(m1.z, min(m2.z, min(m3.z, m4.z)));
		float maxv = max(m1.z, max(m2.z, max(m3.z, m4.z)));
		if (v_next > maxv || v_next < minv)
		{
			v_next = v_;
		}





		if ((u_next > 0 && west.x <= EPSILON1) || (u_next<0 && center.x <= EPSILON1))
		{
			u_next = 0;
		}
		if ((v_next > 0 && north.x <= EPSILON1) || (v_next<0 && center.x <= EPSILON1))
		{
			v_next = 0;
		}

		gridpoint u_center;
		u_center.x = center.x;
		u_center.y = u_next;
		u_center.z = v_next;
		u_center.w = center.w;

		float totalH = center.x + center.w;
		if ((west_.w > totalH) && (west_.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west_.x + west_.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north_.x + north_.w)))
		{
			u_center.z = 0;
		}
		if ((north_.w > totalH) && (north_.x<EPSILON1))
		{
			u_center.z = 0;
		}

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void s_advection(gridpoint* grid_next, int width, int height, float timestep, int pitch)//对速度高度进行advection
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;


	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
		gridpoint westsouth = tex2D(texture_grid, gridx - 1, gridy + 1);
		gridpoint eastnorth = tex2D(texture_grid, gridx + 1, gridy - 1);

		float u_next, v_next, h_next;
		float u = center.y;
		float v = center.z;
		float ur = east.y;
		float vs = south.z;


		//X方向
		float cha_v = (west.z + center.z + south.z + westsouth.z) / 4;
		float udx = u*timestep;
		float udy = cha_v*timestep;
		float ux = gridx - 0.5 - udx;
		float uy = gridy - udy;
		if (ux < 1) ux = 1;
		if (uy < 1) uy = 1;
		if (ux > width) ux = width ;
		if (uy > height) uy = height ;
		gridpoint sdx = chazhisudu1(ux, uy);
		float u_new = (sdx.y + u) / 2;
		float cha_v_new = (sdx.z + cha_v) / 2;
		float ux_new = gridx - 0.5 - u_new*timestep;
		float uy_new = gridy - cha_v_new*timestep;
		gridpoint sdx_new = chazhisudu1(ux_new, uy_new);

		u_next = sdx.y;
		//Y方向
		float cha_u = (center.y + north.y + east.y + eastnorth.y) / 4;
		float vdy = v*timestep;
		float vdx = cha_u*timestep;
		float vx = gridx - vdx;
		float vy = gridy - 0.5 - vdy;
		if (vx < 1) vx = 1;
		if (vy < 1) vy = 1;
		if (vx >= width ) vx = width ;
		if (vy >= height ) vy = height ;
		gridpoint sdy = chazhisudu1(vx, vy);
		float v_new = (sdy.z + v) / 2;
		float cha_u_new = (sdy.y + cha_u) / 2;
		float vx_new = gridx - cha_u_new*timestep;
		float vy_new = gridy - 0.5 - v_new*timestep;
		gridpoint sdy_new = chazhisudu1(vx_new, vy_new);

		v_next = sdy.z;

		//水深advection
		float u_s = (u + ur) / 2;
		float v_s = (v + vs) / 2;
		float hdy = v_s*timestep;
		float hdx = u_s*timestep;
		float hx = gridx - hdx;
		float hy = gridy - hdy;
		if (hx < 1) hx = 1;
		if (hy < 1) hy = 1;
		if (hx >= width ) hx = width ;
		if (hy >= height ) hy = height ;
		gridpoint sdh = chazhisudu1(hx, hy);
		float u_sn = (sdh.y + u_s) / 2;
		float v_sn = (sdh.z + v_s) / 2;
		float hx_n = gridx - u_sn*timestep;
		float hy_n = gridy - v_sn*timestep;
		gridpoint sdh_new = chazhisudu1(hx_n, hy_n);

		h_next = sdh.x;
		float w_next = sdh.w;





		if (x == 0)
		{
			u_next = 0;
		}
		if (y == 0)
		{
			v_next = 0;
		}
		if ((u_next > 0 && west.x <= EPSILON1) || (u_next<0 && center.x <= EPSILON1))
		{
			u_next = 0;
		}
		if ((v_next > 0 && north.x <= EPSILON1) || (v_next<0 && center.x <= EPSILON1))
		{
			v_next = 0;
		}


		float hl, hn;
		float a = 1.0;
		float b = 1.0 - a;
		if (u_next <= 0)
		{
			hl = a*center.x + b*west.x;
		}
		else
		{
			hl = a*west.x + b*center.x;
		}
		if (v_next <= 0)
		{
			hn = a*center.x + b*north.x;
		}
		else
		{
			hn = a*north.x + b*center.x;
		}





		float hl4 = hl*hl*hl*hl;
		float uu = sqrtf(2.0f) * hl * hl*u_next / (sqrtf(hl4 + max(hl4, EPSILON1)));
		float hn4 = hn*hn*hn*hn;
		float vv = sqrtf(2.0f) * hn * hn*v_next / (sqrtf(hn4 + max(hn4, EPSILON1)));


		gridpoint u_center;

		//u_center.x = center.x + center.w;//总高度
		u_center.x = center.x;
		//u_center.x = h_next;
		u_center.y = u_next;
		u_center.z = v_next;
		//u_center.w = center.w;
		u_center.w = h_next;//w分量来存储advection后的水深
		//u_center.w = h_next + w_next;//平流水深
		float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
			u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
			u_center.z = 0;
		}
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}
}
__global__ void jacob(gridpoint* grid_next, gridpoint* grid, int width, int height, int pitch, float timestep)//取中值
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;


	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		gridpoint center1 = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north1 = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west1 = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south1 = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east1 = grid2Dread(grid, gridx + 1, gridy, pitch);

		float d = center.x;
		float h_ = center1.w;

		float hr = east1.x;
		float hl = west1.x;
		float hs = south1.x;
		float hn = north1.x;

		float b1 = tex2D(texture_landscape, x - 1, y - 1).y;
		float br = tex2D(texture_landscape, x, y - 1).y;
		float bl = tex2D(texture_landscape, x - 2, y - 1).y;
		float bs = tex2D(texture_landscape, x - 1, y).y;
		float bn = tex2D(texture_landscape, x - 1, y - 2).y;

		
		float u = center1.y;
		float ur = east1.y;
		float v = center1.z;
		float vs = south1.z;

		

		float temp = 1 + 4 * g*timestep*timestep*d;
		float a1 = -0.25*timestep*timestep*g*(br - bl) + timestep*timestep*g*d;
		float a2 = 0.25*timestep*timestep*g*(br - bl) + timestep*timestep*g*d;
		float a3 = -0.25*timestep*timestep*g*(bs - bn) + timestep*timestep*g*d;
		float a4 = 0.25*timestep*timestep*g*(bs - bn) + timestep*timestep*g*d;
		float a5 = h_ + 0.5*timestep*((u + ur) / 2 * (br - bl) + (v + vs) / 2 * (bs - bn)) - timestep*d*(ur - u + vs - v);



		float h_next = a1/temp*hr + a2/temp*hl + a3/temp*hs + a4/temp*hn + a5/temp;



		gridpoint u_center;

		u_center.x = h_next;
		u_center.y = center1.y;
		u_center.z = center1.z;
		u_center.w = center1.w;

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void jacob1(gridpoint* grid_next, gridpoint* grid, float* g2, int width, int height, int pitch, int p2, float timestep)//
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;


	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		gridpoint center1 = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north1 = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west1 = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south1 = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east1 = grid2Dread(grid, gridx + 1, gridy, pitch);

		float d = center.x;
		float dr = east.x;
		float dl = west.x;
		float ds = south.x;
		float dn = north.x;

		float d_bef = g2[gridy*p2 + gridx];
		float dr_bef = g2[gridy*p2 + gridx+1];
		float dl_bef = g2[gridy*p2 + gridx-1];
		float ds_bef = g2[(gridy+1)*p2 + gridx];
		float dn_bef = g2[(gridy-1)*p2 + gridx];

		/*float d = max(0,center.x * 2 - d_bef);
		float dr = max(0, east.x * 2 - dr_bef);
		float dl = max(0, west.x * 2 - dl_bef);
		float ds = max(0, south.x * 2 - ds_bef);
		float dn = max(0, north.x * 2 - dn_bef);*/


		float h_ = center1.w;//advection后的水高

		float hr = east1.x;
		float hl = west1.x;
		float hs = south1.x;
		float hn = north1.x;

		//获取地形高度
		float b1 = tex2D(texture_landscape, x - 1, y - 1).y;
		float br = tex2D(texture_landscape, x , y - 1).y;
		float bl = tex2D(texture_landscape, x - 2, y - 1).y;
		float bs = tex2D(texture_landscape, x - 1, y ).y;
		float bn = tex2D(texture_landscape, x - 1, y - 2).y;
		


		float u = center1.y;
		float ur = east1.y;
		float v = center1.z;
		float vs = south1.z;



		float temp = 1 + 4 * g*timestep*timestep*d;
		float a1 = timestep*timestep*g*d;
		float a2 = timestep*timestep*g*d;
		float a3 = timestep*timestep*g*d;
		float a4 = timestep*timestep*g*d;
		float a5 = h_ - timestep*d*(ur - u + vs - v) + g*timestep*timestep*d*(bl + br + bn + bs - b1 * 4);

		//边界速度处理
		if (x == 0||((center.x+b1<bl)&&(west.x<EPSILON1))||((west.x+bl<b1)&&(center.x<EPSILON1)))
		{
			temp = temp - g*timestep*timestep*d;
			a1 = 0;
			a5 = a5 - timestep*d*u - g*timestep*timestep*d*(bl - b1);
		}
		if (x == width - 1|| ((center.x + b1<br) && (east.x<EPSILON1)) || ((east.x + br<b1) && (center.x<EPSILON1)))
		{
			temp = temp - g*timestep*timestep*d;
			a2 = 0;
			a5 = a5 + timestep*d*ur - g*timestep*timestep*d*(br - b1);
		}
		if (y == 0|| ((center.x + b1<bn) && (north.x<EPSILON1)) || ((north.x + bn<b1) && (center.x<EPSILON1)))
		{
			temp = temp - g*timestep*timestep*d;
			a3 = 0;
			a5 = a5 - timestep*d*v - g*timestep*timestep*d*(bn - b1);
		}
		if (y == height-1|| ((center.x + b1<bs) && (south.x<EPSILON1)) || ((south.x + bs<b1) && (center.x<EPSILON1)))
		{
			temp = temp - g*timestep*timestep*d;
			a4 = 0;
			a5 = a5 + timestep*d*vs - g*timestep*timestep*d*(bs - b1);
		}
		//float c1 = ur > 0 ? 2 : 0;
		//float c2 = u > 0 ? 0 : 2;
		//float c3 = vs > 0 ? 2 : 0;
		//float c4 = v > 0 ? 0 : 2;

		//float temp = 1 + 0.5 * g*timestep*timestep*(d*(c1 + c2 + c3 + c4) + dr*(2 - c1) + dl*(2 - c2) + ds*(2 - c3) + dn*(2 - c4));
		//float a1 = 0.5*timestep*timestep*g*(d*c2 + dl*(2 - c2));
		//float a2 = 0.5*timestep*timestep*g*(d*c1 + dr*(2 - c1));
		//float a3 = 0.5*timestep*timestep*g*(d*c4 + dn*(2 - c4));
		//float a4 = 0.5*timestep*timestep*g*(d*c3 + ds*(2 - c3));
		////float a5 = h_ - 0.5*timestep*d*(ur - u + vs - v) + 0.5*g*timestep*timestep*d*(bl + br + bn + bs - b1 * 4);
		//float a5 = h_ - 0.5*timestep*((d*c1 + dr*(2 - c1))*ur - (d*c2 + dl*(2 - c2))*u + (d*c3 + ds*(2 - c3))*vs - (d*c4 + dn*(2 - c4))*v) + 0.5*g*timestep*timestep*((d*c1 + dr*(2 - c1))*(br - b1) + (d*c2 + dl*(2 - c2))*(bl - b1) + (d*c3 + ds*(2 - c3))*(bs - b1) + (d*c4 + dn*(2 - c4))*(bn - b1));
		////边界速度处理
		//if (x == 0 || ((center.x + b1<bl) && (west.x<EPSILON1)) || ((west.x + bl<b1) && (center.x<EPSILON1)))
		//{
		//	temp = temp - 0.5*g*timestep*timestep*(d*c2 + dl*(2 - c2));
		//	a1 = 0;
		//	a5 = a5 - 0.5*timestep*(d*c2 + dl*(2 - c2))*u - 0.5*g*timestep*timestep*(d*c2 + dl*(2 - c2))*(bl - b1);
		//}
		//if (x == width - 1 || ((center.x + b1<br) && (east.x<EPSILON1)) || ((east.x + br<b1) && (center.x<EPSILON1)))
		//{
		//	temp = temp - 0.5*g*timestep*timestep*(d*c1 + dr*(2 - c1));
		//	a2 = 0;
		//	a5 = a5 + 0.5*timestep*(d*c1 + dr*(2 - c1))*ur - 0.5*g*timestep*timestep*(d*c1 + dr*(2 - c1))*(br - b1);
		//}
		//if (y == 0 || ((center.x + b1<bn) && (north.x<EPSILON1)) || ((north.x + bn<b1) && (center.x<EPSILON1)))
		//{
		//	temp = temp - 0.5*g*timestep*timestep*(d*c4 + dn*(2 - c4));
		//	a3 = 0;
		//	a5 = a5 - 0.5*timestep*(d*c4 + dn*(2 - c4))*v - 0.5*g*timestep*timestep*(d*c4 + dn*(2 - c4))*(bn - b1);
		//}
		//if (y == height - 1 || ((center.x + b1<bs) && (south.x<EPSILON1)) || ((south.x + bs<b1) && (center.x<EPSILON1)))
		//{
		//	temp = temp - 0.5*g*timestep*timestep*(d*c3 + ds*(2 - c3));
		//	a4 = 0;
		//	a5 = a5 + 0.5*timestep*(d*c3 + ds*(2 - c3))*vs - 0.5*g*timestep*timestep*(d*c3 + ds*(2 - c3))*(bs - b1);
		//}


		float h_next = (a1*hl + a2*hr + a3*hn + a4*hs + a5) / temp;
		//float h_next = center1.w;



		gridpoint u_center;

		u_center.x = h_next;
		u_center.y = center1.y;
		u_center.z = center1.z;
		u_center.w = center1.w;

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void gaoduzhuan(gridpoint* grid_next, gridpoint* grid, float* g2, int width, int height, int pitch, int p2, float timestep)//
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		float a = tex2D(texture_landscape, x - 1, y - 1).y;
		gridpoint center_b = tex2D(texture_grid, gridx, gridy);
		gridpoint center1 = grid2Dread(grid, gridx, gridy, pitch);

		g2[gridy*p2 + gridx] = center_b.x;

		gridpoint u_center;
		u_center.x = center1.x;
		u_center.y = center1.y;
		u_center.z = center1.z;
		u_center.w = a;//之前此分量存储了advection后水高，改回地形高度。
		

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void jacob_v(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;



	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		float u = center.y;
		float ule = west.y;
		float ur = east.y;
		float v = center.z;
		float vn = north.z;
		float vs = south.z;

		float u_next, v_next;
		if (x == 0)
		{
			u_next = 0;
		}
		else
		{
			u_next = u - timestep*g*(center.x - west.x + center.w - west.w);
		}
		if (y == 0)
		{
			v_next = 0;
		}
		else
		{
			v_next = v - timestep*g*(center.x - north.x + center.w - north.w);
		}


		


		float hl, hn;
		float a = 1.0;
		float b = 1.0 - a;
		if (u_next <= 0)
		{
			hl = a*center.x + b*west.x;
		}
		else
		{
			hl = a*west.x + b*center.x;
		}
		if (v_next <= 0)
		{
			hn = a*center.x + b*north.x;
		}
		else
		{
			hn = a*north.x + b*center.x;
		}





		float hl4 = hl*hl*hl*hl;
		float uu = sqrtf(2.0f) * hl * hl*u_next / (sqrtf(hl4 + max(hl4, EPSILON1)));
		float hn4 = hn*hn*hn*hn;
		float vv = sqrtf(2.0f) * hn * hn*v_next / (sqrtf(hn4 + max(hn4, EPSILON1)));


		gridpoint u_center;

		u_center.x = center.x;
		u_center.y = uu;
		u_center.z = vv;
		u_center.w = center.w;
		float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
			u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
			u_center.z = 0;
		}

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void Red_Gauss_Seidel(gridpoint* grid_next, gridpoint* grid, int width, int height, int pitch, float timestep)//取中值
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	//限制水深，h<=2*(x/(t*g)),约为20，本实验水深不会达到20，故省去此步。
	if (x < width && y < height )
	{
		int gridx = x + 1;
		int gridy = y + 1;
		

		gridpoint center1 = tex2D(texture_grid, gridx, gridy);
		/*gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);*/
		gridpoint center = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east = grid2Dread(grid, gridx + 1, gridy, pitch);

			float h = center1.x;
			float hr = east.x;
			float hl = west.x;
			float hs = south.x;
			float hn = north.x;

			float u = center.y;
			float ur = east.y;
			float v = center.z;
			float vs = south.z;

			/*float temp = 2 + (ur - u + vs - v)*timestep;
			float a1 = timestep*(-ur) / temp;
			float a2 = timestep*(u) / temp;
			float a3 = timestep*(-vs) / temp;
			float a4 = timestep*(v) / temp;
			float a5 = 2 / temp;


			float h_next = a1*hr + a2*hl + a3*hs + a4*hn + a5*h;*/

			float bjl, bjr, bjn, bjs;
			if (u <= 0)
			{
				bjl = 1;
			}
			else
			{
				bjl = 0;
			}
			if (ur <= 0)
			{
				bjr = 0;
			}
			else
			{
				bjr = 1;
			}
			if (v <= 0)
			{
				bjn = 1;
			}
			else
			{
				bjn = 0;
			}
			if (vs <= 0)
			{
				bjs = 0;
			}
			else
			{
				bjs = 1;
			}
			float bjl1 = 1.0 - bjl;
			float bjr1 = 1.0 - bjr;
			float bjn1 = 1.0 - bjn;
			float bjs1 = 1.0 - bjs;

			float temp = 1.0 + (bjr*ur - bjl*u + bjs*vs - bjn*v)*timestep;
			float a1 = timestep*(-bjr1*ur) / temp;
			float a2 = timestep*(bjl1*u) / temp;
			float a3 = timestep*(-bjs1*vs) / temp;
			float a4 = timestep*(bjn1*v) / temp;
			float a5 = 1 / temp;


			float h_next = a1*hr + a2*hl + a3*hs + a4*hn + a5*h;



			gridpoint u_center;

			u_center.x = h_next;
			u_center.y = center.y;
			u_center.z = center.z;
			u_center.w = center.w;

			grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
		
	}
}

__global__ void Black_Gauss_Seidel(gridpoint* grid_next, gridpoint* grid, int width, int height, int pitch, float timestep)//取中值
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height )
	{
		int gridx = x + 1;
		int gridy = y + 1;
		
		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
			gridpoint center1 = grid2Dread(grid, gridx, gridy, pitch);
			gridpoint north1 = grid2Dread(grid, gridx, gridy - 1, pitch);
			gridpoint west1 = grid2Dread(grid, gridx - 1, gridy, pitch);
			gridpoint south1 = grid2Dread(grid, gridx, gridy + 1, pitch);
			gridpoint east1 = grid2Dread(grid, gridx + 1, gridy, pitch);

			float hl, hr, hn, hs;
			float u = center1.y;
			float ur = east1.y;
			float v = center1.z;
			float vs = south1.z;

			float hl_ = west1.x;
			float hr_ = east1.x;
			float hn_ = north1.x;
			float hs_ = south1.x;
			float wc = center.w;
			float wl = west.w;
			float wr = east.w;
			float wn = north.w;
			float ws = south.w;

			float hc = center.x;
			float a = 1.0;
			float b = 1.0 - a;
			if (u <= 0)
			{
				hl = a*center.x + b*west.x;
			}
			else
			{
				hl = a*west.x + b*center.x;
			}
			if (ur <= 0)
			{
				hr = a*east.x + b*center.x;
			}
			else
			{
				hr = a*center.x + b*east.x;
			}
			if (v <= 0)
			{
				hn = a*center.x + b*north.x;
			}
			else
			{
				hn = a*north.x + b*center.x;
			}
			if (vs <= 0)
			{
				hs = a*south.x + b*center.x;
			}
			else
			{
				hs = a*center.x + b*south.x;
			}
		

			float temp = 1.0 + g*timestep*timestep*(hl + hr + hn + hs);
			float a1 = g*timestep*timestep*hl ;
			float a2 = g*timestep*timestep*hr ;
			float a3 = g*timestep*timestep*hn ;
			float a4 = g*timestep*timestep*hs ;
			float a5 = (hc - timestep*(hr*(ur- g*timestep*(wr-wc)) - hl*(u- g*timestep*(wc-wl)) + hs*(vs- g*timestep*(ws-wc)) - hn*(v- g*timestep*(wc-wn)))) ;
			if (x == 0)
			{
				a1 = 0;
				temp = temp - g*timestep*timestep*hl;
				a5 = a5 - timestep*hl*(u - g*timestep*(wc - wl));
			}
			if (y == 0)
			{
				a3 = 0;
				temp = temp - g*timestep*timestep*hn;
				a5 = a5 - timestep*hn*(v - g*timestep*(wc - wn));
			}

			float h_next = (a1*hl_ + a2*hr_ + a3*hn_ + a4*hs_ + a5) / temp;

			gridpoint u_center;

			u_center.x = h_next;
			u_center.y = center1.y;
			u_center.z = center1.z;
			u_center.w = center.w;
			//wucha += center1.x;
			grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
		
	}
}
__global__ void jacob_h(gridpoint* grid_next, gridpoint* grid, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	//限制水深，h<=2*(x/(t*g)),约为20，本实验水深不会达到20，故省去此步。

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center1 = tex2D(texture_grid, gridx, gridy);
		/*gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);*/
		gridpoint center = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east = grid2Dread(grid, gridx + 1, gridy, pitch);

		float u = center.y;
		float ur = east.y;
		float v = center.z;
		float vs = south.z;

		float h = center1.x;

		float h_next = center.x - timestep*h*(ur - u + vs - v);

		


		gridpoint u_center;

		u_center.x = max(h_next, 0);
		u_center.y = center.y;
		u_center.z = center.z;
		u_center.w = center.w;


		float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
			u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
			u_center.z = 0;
		}
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void s_Height_Integration(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	//限制水深，h<=2*(x/(t*g)),约为20，本实验水深不会达到20，故省去此步。

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		float u = center.y;
		float ur = east.y;
		float v = center.z;
		float vs = south.z;

		float hl, hr, hn, hs;
		float a = 1.0;
		float b = 1.0 - a;
		if (u <= 0)
		{
			hl = a*center.x + b*west.x;
		}
		else
		{
			hl = a*west.x + b*center.x;
		}
		if (ur <= 0)
		{
			hr = a*east.x + b*center.x;
		}
		else
		{
			hr = a*center.x + b*east.x;
		}
		if (v <= 0)
		{
			hn = a*center.x + b*north.x;
		}
		else
		{
			hn = a*north.x + b*center.x;
		}
		if (vs <= 0)
		{
			hs = a*south.x + b*center.x;
		}
		else
		{
			hs = a*center.x + b*south.x;
		}
		/*hr = (center.x + east.x) / 2;
		hl = (center.x + west.x) / 2;
		hs = (center.x + south.x) / 2;
		hn = (center.x + north.x) / 2;*/

		float h_next = center.x - timestep*(/*0.5*(east.x-west.x+south.x-north.x)+*/hr*ur - hl*u + hs*vs - hn*v);

		float hl4 = hl*hl*hl*hl;
		float uu = sqrtf(2.0f) * hl * hl*u / (sqrtf(hl4 + max(hl4, EPSILON1)));
		float hn4 = hn*hn*hn*hn;
		float vv = sqrtf(2.0f) * hn * hn*v / (sqrtf(hn4 + max(hn4, EPSILON1)));


		gridpoint u_center;

		u_center.x = max(h_next, 0);
		u_center.y = center.y;
		u_center.z = center.z;
		u_center.w = center.w;


		float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
			u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
			u_center.z = 0;
		}
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void s_Velocity_Integration1(gridpoint* grid_next, gridpoint* grid, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;



	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		gridpoint center1 = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north1 = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west1 = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south1 = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east1 = grid2Dread(grid, gridx + 1, gridy, pitch);

		float u = center1.y;
		float ule = west1.y;
		float ur = east1.y;
		float v = center1.z;
		float vn = north1.z;
		float vs = south1.z;

		float u_next, v_next;
		if (x == 0)
		{
			u_next = 0;
		}
		else
		{
			u_next = u - timestep*g*(max(0, center.x) - max(0, west.x) + center.w - west.w);
		}
		if (y == 0)
		{
			v_next = 0;
		}
		else
		{
			v_next = v - timestep*g*(max(0, center.x) - max(0, north.x) + center.w - north.w);
		}


		if (u_next > LIM) u_next = LIM;
		if (u_next < -LIM) u_next = -LIM;
		if (v_next > LIM) v_next = LIM;
		if (v_next < -LIM) v_next = -LIM;


		float hl, hn;
		float a = 1.0;
		float b = 1.0 - a;
		if (u_next <= 0)
		{
			hl = a*center.x + b*west.x;
		}
		else
		{
			hl = a*west.x + b*center.x;
		}
		if (v_next <= 0)
		{
			hn = a*center.x + b*north.x;
		}
		else
		{
			hn = a*north.x + b*center.x;
		}





		float hl4 = hl*hl*hl*hl;
		float uu = sqrtf(2.0f) * hl * hl*u_next / (sqrtf(hl4 + max(hl4, EPSILON1)));
		float hn4 = hn*hn*hn*hn;
		float vv = sqrtf(2.0f) * hn * hn*v_next / (sqrtf(hn4 + max(hn4, EPSILON1)));


		gridpoint u_center;

		u_center.x = max(0, center.x);
		u_center.y = uu;
		u_center.z = vv;
		u_center.w = center.w;
		float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
			u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
			u_center.z = 0;
		}

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void s_Velocity_Integration(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;



	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		float u = center.y;
		float ule = west.y;
		float ur = east.y;
		float v = center.z;
		float vn = north.z;
		float vs = south.z;

		float u_next, v_next;
		if (x == 0)
		{
			u_next = 0;
		}
		else
		{
			u_next = u - timestep*g*(max(0, center.x) - max(0, west.x) + center.w - west.w);
		}
		if (y == 0)
		{
			v_next = 0;
		}
		else
		{
			v_next = v - timestep*g*(max(0, center.x) - max(0, north.x) + center.w - north.w);
		}


		if (u_next > LIM) u_next = LIM;
		if (u_next < -LIM) u_next = -LIM;
		if (v_next > LIM) v_next = LIM;
		if (v_next < -LIM) v_next = -LIM;


		float hl, hn;
		float a = 1.0;
		float b = 1.0 - a;
		if (u_next <= 0)
		{
			hl = a*center.x + b*west.x;
		}
		else
		{
			hl = a*west.x + b*center.x;
		}
		if (v_next <= 0)
		{
			hn = a*center.x + b*north.x;
		}
		else
		{
			hn = a*north.x + b*center.x;
		}
		
		

		

		float hl4 = hl*hl*hl*hl;
		float uu = sqrtf(2.0f) * hl * hl*u_next / (sqrtf(hl4 + max(hl4, EPSILON1)));
		float hn4 = hn*hn*hn*hn;
		float vv = sqrtf(2.0f) * hn * hn*v_next / (sqrtf(hn4 + max(hn4, EPSILON1)));


		gridpoint u_center;

		u_center.x = max(0,center.x);
		u_center.y = uu;
		u_center.z = vv;
		u_center.w = center.w;
		float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
			u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
			u_center.z = 0;
		}

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}

__global__ void initGrid(gridpoint *grid, float *g2,int gridwidth, int gridheight, int pitch, int p2)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x < gridwidth && y < gridheight)
	{
		float a = tex2D(texture_landscape, x - 1, y - 1).y;//取值3~7

		gridpoint gp;
		gp.x = max(NN - a, 0.0f);//取值为0~2，NN=5的情况下
								 //gp.x = NN - a;
								// gp.x = 0.0f;
		
		gp.y = 0.0f;
		gp.z = 0.0f;

		gp.w = a;//土地高度，3~7
		/*if (x == 0 || y == 0 || x == gridwidth - 1 || y == gridheight - 1)
		{
			gp.x = 0;
			gp.w = 0;
		}*/
		if (x >= 110 && x < 136 && y>=110 && y < 136)
		{
			gp.x = 5;
		}

		/*if (x <= 10 || y <= 10 || (x >= gridwidth - 10) || (y >= gridheight - 10))
		{
			gp.w = 10;
			gp.x = 0;
			gp.y = 0;
			gp.z = 0;
		}*/
		float g2f;
		g2f = gp.x;
		grid2Dwrite(grid, x, y, pitch, gp);//将土地数据存入device_grid里
		g2[(y)*p2 + x] = g2f;
		
	}
}

__host__ __device__ vertex gridpointToVertex(gridpoint gp, float x, float y)
{
	float h = gp.x;//这是波浪的相对高度
	vertex v;
	v.x = x * 20.0f - 10.0f;
	v.z = y * 20.0f - 10.0f;//说实话，不知道啥意思
	v.y = h + gp.w;//实际浪高，即浪高+土地高度
	return v;
}

__host__ __device__ rgb gridpointToColor(gridpoint gp)
{
	rgb c;
	c.x = min(20 + (gp.x + gp.w - NN) * 200.0f, 255);
	c.y = min(40 + (gp.x + gp.w - NN) * 200.0f, 255);
	c.z = min(100 + (gp.x + gp.w - NN) * 200.0f, 255);
	c.w = 255 - max(-50 * gp.x + 50, 0);

	return c;
}//高度很高，就都是白色，高度恰好为NN时，颜色rgb值为（20，40，100）

__global__ void visualise(vertex* watersurfacevertices,
	rgb* watersurfacecolors, int width, int height)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint gp = tex2D(texture_grid, gridx, gridy);

		watersurfacevertices[y * width + x] = gridpointToVertex(gp, x / float(width - 1), y / float(height - 1));
		watersurfacecolors[y * width + x] = gridpointToColor(gp);
	}
}


__global__ void addWave(gridpoint* grid, float* wave, float norm, int width, int height, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		float waveheight = grid2Dread(grid, gridx, gridy, pitch).x;
		//第一次会读取initGrid里的gp.x
		//会使高度为0的水流变为NN
		//float waveheight = 0.0;
		float toadd = max(grid2Dread(wave, x, y, width) - 0.2f, 0.0f) * norm;//0~0.8变为0~0.6*norm
		waveheight += toadd;
		grid[gridx + gridy * pitch].x = waveheight;//将新的高度值存入grid，即device_grid_next

	}
}




__global__ void addWave1(gridpoint* grid, float* g2, int width, int height, int pitch, int p2, int shh)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		/*if (x < 110 && x >100 && y > 200&&y<220)
		{
		gridpoint center = tex2D(texture_grid, gridx, gridy);
		center.x = 45;
		grid[gridx + gridy * pitch]= center;
		}*/
		if (x <=5&&y<=5)
		{
			gridpoint center = tex2D(texture_grid, gridx, gridy);
			center.x = 3;
			//center.y = 2;
			//center.z = 2;
			grid[gridx + gridy * pitch] = center;
			g2[gridx + gridy*p2] = center.x;
		}
	}
}

void addWave(float* wave, float norm, int width, int height, int pitch_elements)
{
	cudaError_t error;
	size_t sizeInBytes = width * height * sizeof(float);

	error = cudaMemcpy(device_waves, wave, sizeInBytes, cudaMemcpyHostToDevice);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//device_waves存储波浪信息

	int x = (width + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int y = (height + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGrid(x, y);

	addWave << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_waves, norm, width, height, pitch_elements);

	error = cudaGetLastError();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	error = cudaThreadSynchronize();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	gridpoint *grid_helper = device_grid;
	device_grid = device_grid_next;
	device_grid_next = grid_helper;
	//更新数据

	error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, width + 2, height + 2, grid_pitch_elements * sizeof(gridpoint));
	//	CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);
	//新的波浪信息在texture_grid里
}

void initWaterSurface(int width, int height, vertex *heightmapvertices, float *wave)
{

	if (state != UNINTIALISED)
	{
		return;
	}
	int gridwidth = width + 2;
	int gridheight = height + 2;

	size_t sizeInBytes;
	size_t grid_pitch;
	cudaError_t error;
	size_t h_pitch;


	grid_channeldesc = cudaCreateChannelDesc<float4>();
	cudaChannelFormatDesc treshholds_channeldesc = cudaCreateChannelDesc<float>();
	cudaChannelFormatDesc reflections_channeldesc = cudaCreateChannelDesc<int>();

	//alloc pitched memory for device_grid
	error = cudaMallocPitch(&device_grid, &grid_pitch, gridwidth * sizeof(gridpoint), gridheight);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//	CHECK_NOTNULL(device_grid);
	error = cudaMallocPitch(&h_bef, &h_pitch, gridwidth * sizeof(float), gridheight);
	h_pitch_elements = h_pitch / sizeof(float);
	size_t oldpitch = grid_pitch;

	//alloc pitched memoty for device_grid_next
	error = cudaMallocPitch(&device_grid_next, &grid_pitch, gridwidth * sizeof(gridpoint), gridheight);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//	CHECK_NOTNULL(device_grid_next);

	//	CHECK_EQ(oldpitch, grid_pitch);

	grid_pitch_elements = grid_pitch / sizeof(gridpoint);

	//alloc pitched memory for landscape data on device
	size_t heightmap_pitch;
	error = cudaMallocPitch(&device_heightmap, &heightmap_pitch, width * sizeof(vertex), height);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//	CHECK_NOTNULL(device_heightmap);
	height_pitch_elements = heightmap_pitch / sizeof(vertex);

	// copy landscape data to device
	error = cudaMemcpy2D(device_heightmap, heightmap_pitch, heightmapvertices, width * sizeof(vertex), width * sizeof(vertex), height, cudaMemcpyHostToDevice);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//device_heightmap存储1维土地高度图

	// bind heightmap to texture_landscape
	heightmap_channeldesc = cudaCreateChannelDesc<float4>();
	error = cudaBindTexture2D(0, &texture_landscape, device_heightmap, &heightmap_channeldesc, width, height, heightmap_pitch);
	//	CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);
	//texture_landscape有2维土地高度图

	// malloc memory for watersurface vertices
	sizeInBytes = width * height * sizeof(vertex);
	error = cudaMalloc(&device_watersurfacevertices, sizeInBytes);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// malloc memory for watersurface colors
	sizeInBytes = height * width * sizeof(rgb);
	error = cudaMalloc(&device_watersurfacecolors, sizeInBytes);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// malloc memory for waves
	sizeInBytes = height * width * sizeof(float);
	error = cudaMalloc(&device_waves, sizeInBytes);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// make dimension
	int x = (gridwidth + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int y = (gridheight + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGrid(x, y);

	int x1 = (gridwidth + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	dim3 threadsPerBlock1(BLOCKSIZE_X, 1);
	dim3 blocksPerGrid1(x1, 1);

	int y1 = (gridheight + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock2(1, BLOCKSIZE_Y);
	dim3 blocksPerGrid2(1, y1);

	//init grid with initial values
	initGrid << < blocksPerGrid, threadsPerBlock >> > (device_grid, h_bef, gridwidth, gridheight, grid_pitch_elements, h_pitch_elements);

	//init grid_next with initial values
	initGrid << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, h_bef, gridwidth, gridheight, grid_pitch_elements, h_pitch_elements);

	error = cudaThreadSynchronize();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);


	//bind the grid to texture_grid
	error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch);
	//	CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);
	//经过initGrid的土地高度信息在texture_grid里

	//add the initial wave to the grid
	addWave(wave,0.0f, width, height, grid_pitch_elements);//修改此处的第二个参数，可以改变波浪的高度

	state = INITIALISED;
}

void computeNext(int width, int height, vertex* watersurfacevertices, rgb* watersurfacecolors, int steps, bool renderonly)
{

	if (state != INITIALISED)
	{
		return;
	}

	int gridwidth = width + 2;
	int gridheight = height + 2;

	cudaError_t error;
	// make dimension
	int x = (width + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int y = (height + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGrid(x, y);



	if (!renderonly)
	{
		//gitter "stepsperframe" zeitschritt
		for (int x = 0; x < steps; x++)
		{

			//		simulateWaveStep << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			//
			//		error = cudaThreadSynchronize();
			////		CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
			//
			//		gridpoint *grid_helper = device_grid;
			//		device_grid = device_grid_next;
			//		device_grid_next = grid_helper;
			s_advection << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			/*s_advection1 << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			error = cudaThreadSynchronize();
			gridpoint *grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;
			s_advection2 << < blocksPerGrid, threadsPerBlock >> > (device_grid, device_grid_next, width, height, timestep, grid_pitch_elements);*/

			error = cudaThreadSynchronize();
			gridpoint *grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;

			//s_Velocity_Integration1 << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, timestep, grid_pitch_elements);
			////s_Height_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			//error = cudaThreadSynchronize();
			//grid_helper = device_grid;
			//device_grid = device_grid_next;
			//device_grid_next = grid_helper;
			//error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
			//		CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
			//gridpoint *grid_next = device_grid;
			//s_Velocity_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			//s_Height_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			//jacob_h << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, timestep, grid_pitch_elements);
			//s_Height_Integration1 << < blocksPerGrid, threadsPerBlock >> > (juzhen, width, height, timestep, grid_ju_elements);
			//error = cudaThreadSynchronize();

			for (int dn = 0; dn < maxdiedai; dn++)
			{
				//float* wucha;
				//*wucha = 0.0;
				jacob1 << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, h_bef, width, height, grid_pitch_elements, h_pitch_elements,timestep);
				//Red_Gauss_Seidel << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);
				//error = cudaThreadSynchronize();
				//grid_helper = device_grid;
				//device_grid = device_grid_next;
				//device_grid_next = grid_helper;
				//error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
				//Black_Gauss_Seidel << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);
				error = cudaThreadSynchronize();
				grid_helper = device_grid;
				device_grid = device_grid_next;
				device_grid_next = grid_helper;
				//if (wucha < 1.0) break;
				//error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
			}
			gaoduzhuan << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, h_bef, width, height, grid_pitch_elements, h_pitch_elements,timestep);
			error = cudaThreadSynchronize();
			grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;
			error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
			//		CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

			s_Velocity_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			//s_Height_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			error = cudaThreadSynchronize();
			grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;

			int x1 = (width + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
			int y1 = (height + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
			dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
			dim3 blocksPerGrid(x1, y1);
			int shh = rand() % 3 + 2;
			//addWave1 << < blocksPerGrid, threadsPerBlock >> > (device_grid, h_bef,width, height, grid_pitch_elements, h_pitch_elements,shh);
			error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));


			//		CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);
		}
		//addland << < blocksPerGrid, threadsPerBlock >> > (device_grid, device_heightmap, width, height, height_pitch_elements, grid_pitch_elements);
		error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
	}
	





	visualise << < blocksPerGrid, threadsPerBlock >> > (device_watersurfacevertices, device_watersurfacecolors, width, height);



	error = cudaGetLastError();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	error = cudaThreadSynchronize();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// copy back data
	//error = cudaMemcpy(landhhs, device_heightmap, width * height * sizeof(vertex), cudaMemcpyDeviceToHost);
	error = cudaMemcpy(watersurfacevertices, device_watersurfacevertices, width * height * sizeof(vertex), cudaMemcpyDeviceToHost);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	error = cudaMemcpy(watersurfacecolors, device_watersurfacecolors, width * height * sizeof(rgb), cudaMemcpyDeviceToHost);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);




}

void destroyWaterSurface()
{
	if (state != INITIALISED)
	{
		return;
	}
	cudaUnbindTexture(texture_grid);
	cudaUnbindTexture(texture_landscape);
	cudaFree(device_grid);
	cudaFree(device_grid_next);

	cudaFree(device_heightmap);
	cudaFree(device_watersurfacevertices);

	cudaFree(device_waves);
	cudaFree(device_watersurfacecolors);
	state = UNINTIALISED;
}
