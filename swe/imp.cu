#include "wavesimulator.h"

#include <iostream>
#include "types.h"
#include <stdlib.h>
#include <stdio.h>
#include<vector>
#include <time.h>

#define BLOCKSIZE_X 16
#define BLOCKSIZE_Y 16

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#define grid2Dwrite(array, x, y, pitch, value) array[(y)*pitch+x] = value
#define grid2Dread(array, x, y, pitch) array[(y)*pitch+x]
#define double2Dwrite(array, x, y, pitch, value) array[(y)*pitch+x] = value
#define double2Dread(array, x, y, pitch) array[(y)*pitch+x]


__constant__ float deltax = 1;
__constant__ float deltay = 1;
__constant__ float deltat = 0.01;
__constant__ float manning = 0.020;


const int UNINTIALISED = 0;
const int INITIALISED = 1;

int state = UNINTIALISED;

int stepsperframe = 10;

__constant__ float GRAVITY = 9.83219f * 0.5f; //0.5f * Fallbeschleunigung
__constant__ float g = 9.83219f;

__constant__ float NN = 5.0f;



__constant__ double _long_step = 0.000547 * 256 / 512;
__constant__ double _lat_step = 0.000422 * 256 / 512;

const float timestep = 0.03f;

const int maxdiedai = 40;

#define LIM  250000000.0f
int huduz = 0.05;
texture<gridpoint, 2, cudaReadModeElementType> texture_grid;
texture<vertex, 2, cudaReadModeElementType> texture_landscape;

int grid_pitch_elements;
int height_pitch_elements;
int model_pitch_elements;
int grid_ju_elements;
cudaChannelFormatDesc grid_channeldesc;
cudaChannelFormatDesc heightmap_channeldesc;

gridpoint* device_grid;
gridpoint* device_grid_next;

float* juzhen;//红黑高斯塞德尔矩阵


vertex* device_heightmap;

vertex* device_watersurfacevertices;

float* device_waves;
rgb* device_watersurfacecolors;

int model_nums;//总模型数量
int* model_index;//model_data的索引
int* face_in;//face_index的索引
vertex* model_data;//每个模型顶点数据
vertex* model_datay;
vertex* face_index;//每个模型的面片索引
double* baow;//每个模型包围盒
vertex* localindex;//每个模型产生的高度场的起始结束坐标
double* model_land;//每个模型产生的高度场
				   //int* model_land_index;//model_land的索引
int model_nsize = 0;
int face_nsize = 0;
vertex* localindexm;
vertex* fxiangl;
double *transt;

vertex* model_data_g;
vertex* model_datay_g;
vertex* face_index_g;
int* baow_g;
vertex* localindex_g;
double* model_land_g;
int* model_land_index_g;
vertex* localindexm_g;
vertex* fxiangl_g;
double *transt_g;
double zhuan;


#define PIOver180 0.017453292519943295769236907684886
#define PIUnder180 57.295779513096787
#define EPSILON 0.0005f
#define EPSILON1 0.05f
#define Deg2Rad(x) x * PIOver180
#define Rad2Deg(x) x * PIUnder180
#define ellipsea  6378137
#define ellipseb  6378137
#define ellipsec ellipsea * ellipsea / ellipseb
#define ellipseee1  (ellipsea * ellipsea - ellipseb * ellipseb) / (ellipsea * ellipsea)
#define ellipseee2  (ellipsea * ellipsea - ellipseb * ellipseb) / (ellipseb * ellipseb)


__host__ __device__ void tojingwei(double x, double y, double z, double& lon, double& lat, double& h)
{
	lon = atan2(y, x);

	//以下使用迭代法求大地维度
	double t0, t, tOld, dt;
	dt = 1.0;
	t0 = z / sqrt(x * x + y * y);

	tOld = t0;

	double p = ellipsec * ellipseee1 / sqrt(x * x + y * y);
	double k = 1 + ellipseee2;

	t = 0.0;
	while (dt > 0.001)
	{
		t = t0 + p * tOld / sqrt(k + tOld * tOld);
		dt = t - tOld;
		tOld = t;
	}

	lat = atan(t);

	//求出h
	double sinlat = sin(lat);
	double n = ellipsea / sqrt(1 - ellipseee1 * sinlat * sinlat);
	h = sqrt(x * x + y * y) / cos(lat) - n;

	lon = Rad2Deg(lon);
	lat = Rad2Deg(lat);
}
__host__ __device__ void tozhijiao(double long_tud, double la_tud, double h, double vec[])
{
	double v;
	double lat, lon;
	lat = Deg2Rad(la_tud);
	lon = Deg2Rad(long_tud);
	double sinlat = sin(lat);
	double coslat = cos(lat);
	double sinlon = sin(lon);
	double coslon = cos(lon);

	v = ellipsea / sqrt(1 - ellipseee1 * sinlat * sinlat);
	vec[0] = ((v + h) * coslat * coslon);
	vec[1] = ((v + h) * coslat * sinlon);
	vec[2] = (((1 - ellipseee1) * v + h) * sinlat);

	return;
}
__host__ __device__ double computeh(vertex a, vertex v, vertex p)
{
	double re;
	re = a.z + ((a.x - p.x)*v.x + (a.y - p.y)*v.y) / v.z;
	return re;
}
__host__ __device__ double v2_dot(vertex a, vertex b)
{
	return a.x*b.x + a.y*b.y;
}
__host__ __device__ double v2_vec(vertex a, vertex b)
{
	return a.x*b.y - a.y*b.x;
}
__host__ __device__ vertex p2_to_v2(vertex a, vertex b)
{
	vertex re;
	re.x = a.x - b.x;
	re.y = a.y - b.y;
	return re;
}
__host__ __device__ bool PointinTriangle(vertex A, vertex B, vertex C, vertex P)
{
	vertex v0 = p2_to_v2(C, A);
	vertex v1 = p2_to_v2(B, A);
	vertex v2 = p2_to_v2(P, A);

	double dot00 = v2_dot(v0, v0);
	double dot01 = v2_dot(v0, v1);
	double dot02 = v2_dot(v0, v2);
	double dot11 = v2_dot(v1, v1);
	double dot12 = v2_dot(v1, v2);

	double inverDeno = 1 / (dot00 * dot11 - dot01 * dot01);

	double u = (dot11 * dot02 - dot01 * dot12) * inverDeno;
	if (u < 0 || u > 1) // if u out of range, return directly
	{
		return false;
	}

	double v = (dot00 * dot12 - dot01 * dot02) * inverDeno;
	if (v < 0 || v > 1) // if v out of range, return directly
	{
		return false;
	}

	return u + v <= 1;
}
__host__ __device__ bool PointonTriangle(vertex A, vertex B, vertex C, vertex P)
{
	vertex v0 = p2_to_v2(P, A);
	vertex v1 = p2_to_v2(P, B);
	vertex v2 = p2_to_v2(P, C);

	double vec1 = v2_vec(v0, v1);
	double vec2 = v2_vec(v1, v2);
	double vec3 = v2_vec(v0, v2);
	if (vec1 == 0 || vec2 == 0 || vec3 == 0)
	{
		return true;
	}
	return false;
}
__host__ __device__ double index_to_coord(int a, double b, double c)
{
	double d = a;
	return b + d*c;
}
__host__ __device__ double coord_to_index(double a, double c)
{
	return a / c;
}
__host__ __device__ double min_t(double p1, double p2, double p3)
{
	double m;
	if (p1 < p2) m = p1;
	else m = p2;
	if (p3 < m) m = p3;
	return m;
}
__host__ __device__ double max_t(double p1, double p2, double p3)
{
	double m;
	if (p1 > p2) m = p1;
	else m = p2;
	if (p3 > m) m = p3;
	return m;
}
__host__ __device__ vertex cross(vertex a, vertex b)
{
	vertex re;
	re.x = a.y*b.z - a.z*b.y;
	re.y = a.z*b.x - a.x*b.z;
	re.z = a.x*b.y - a.y*b.x;
	double len = sqrt(re.x*re.x + re.y*re.y + re.z*re.z);
	re.x = re.x / len;
	re.y = re.y / len;
	re.z = re.z / len;
	return re;
}
__host__ __device__ vertex p_to_v(vertex a, vertex b)
{
	vertex re;
	re.x = a.x - b.x;
	re.y = a.y - b.y;
	re.z = a.z - b.z;
	return re;
}

__host__ __device__ vertex local(vertex a, double x, double y)//将viwo坐标转化为计算时的局部坐标
{
	vertex re;
	re.x = a.x - x;
	re.y = a.y - y;
	re.z = a.z;
	return re;
}


__host__ __device__ gridpoint operator +(const gridpoint& x, const gridpoint& y)
{
	gridpoint z;
	z.x = x.x + y.x;
	z.y = x.y + y.y;
	z.z = x.z + y.z;
	z.w = x.w + y.w;
	return z;
}
__host__ __device__ gridpoint operator -(const gridpoint& x, const gridpoint& y)
{
	gridpoint z;
	z.x = x.x - y.x;
	z.y = x.y - y.y;
	z.z = x.z - y.z;
	z.w = x.w - y.w;
	return z;
}
__host__ __device__ gridpoint operator *(const gridpoint& x, const gridpoint& y)
{
	gridpoint z;
	z.x = y.x * x.x;
	z.y = y.y * x.y;
	z.z = y.z * x.z;
	z.w = y.w * x.w;
	return z;
}
__host__ __device__ gridpoint operator *(const gridpoint& x, const float& c)
{
	gridpoint z;
	z.x = c * x.x;
	z.y = c * x.y;
	z.z = c * x.z;
	z.w = c * x.w;
	return z;
}
__host__ __device__ gridpoint operator /(const gridpoint& x, const float& c)
{
	gridpoint z;
	z.x = x.x / c;
	z.y = x.y / c;
	z.z = x.z / c;
	z.w = x.w / c;
	return z;
}
__host__ __device__ gridpoint operator *(const float& c, const gridpoint& x)
{
	return x * c;
}


__host__ __device__ float computelimit(gridpoint gp1, gridpoint gp2)
{
	float x1 = gp1.x;
	float x2 = gp2.x;
	float l1 = x2 / x1;
	if (l1 >= -100 && l1 <= 100)
	{
		return 1.0;
	}
	else if (l1 > 100)
	{
		return 100 / l1;
	}
	else
	{
		return -100 / l1;
	}
}
__host__ __device__ float phi(float lambda)
{
	// empirical value
	float epsilon = 0.001;
	if (abs(lambda) >= epsilon)
		return abs(lambda);
	else
		return(lambda*lambda + epsilon*epsilon) / (2 * epsilon);
}
__host__ __device__ gridpoint riemannX(gridpoint qL, gridpoint qR)
{
	float hL = qL.x;
	float uL = qL.y / qL.x;
	float vL = qL.z / qL.x;
	float hR = qR.x;
	float uR = qR.y / qR.x;
	float vR = qR.z / qR.x;

	float hBar = 0.5*(hL + hR);
	float uTilde = (sqrt(hL)*uL + sqrt(hR)*uR) / (sqrt(hL) + sqrt(hR));
	float vTilde = (sqrt(hL)*vL + sqrt(hR)*vR) / (sqrt(hL) + sqrt(hR));
	float cTilde = sqrt(g*hBar);

	gridpoint r1;
	r1.x = 1;
	r1.y = uTilde - cTilde;
	r1.z = vTilde;
	gridpoint r2;
	r2.x = 0;
	r2.y = 0;
	r2.z = 1;
	gridpoint r3;
	r3.x = 1;
	r3.y = uTilde + cTilde;
	r3.z = vTilde;

	gridpoint alpha, lambda;
	gridpoint delta = qR;
	delta = delta - qL;
	gridpoint w;
	alpha.x = ((uTilde + cTilde)*delta.x - delta.y) / (2 * cTilde);
	alpha.y = -vTilde*delta.x + delta.z;
	alpha.z = (-(uTilde - cTilde)*delta.x + delta.y) / (2 * cTilde);
	lambda.x = uTilde - cTilde;
	lambda.y = uTilde;
	lambda.z = uTilde + cTilde;
	w = r1*phi(lambda.x)*alpha.x;
	w = w + r2*phi(lambda.y)*alpha.y;
	w = w + r3*phi(lambda.z)*alpha.z;
	w = w*0.5;

	gridpoint F;
	F.x = 0.5*(qL.y + qR.y);
	F.y = 0.5*(qL.y*qL.y / qL.x + 0.5*g*qL.x*qL.x + qR.y*qR.y / qR.x + 0.5*g*qR.x*qR.x);
	F.z = 0.5*(qL.y*qL.z / qL.x + qR.y*qR.z / qR.x);
	F = F - w;
	//lambdaMax = max(lambda);
	return F;
}
__host__ __device__ gridpoint riemannY(gridpoint qL, gridpoint qR)
{
	float hL = qL.x;
	float uL = qL.y / qL.x;
	float vL = qL.z / qL.x;
	float hR = qR.x;
	float uR = qR.y / qR.x;
	float vR = qR.z / qR.x;
	float hBar = 0.5*(hL + hR);
	float uTilde = (sqrt(hL)*uL + sqrt(hR)*uR) / (sqrt(hL) + sqrt(hR));
	float vTilde = (sqrt(hL)*vL + sqrt(hR)*vR) / (sqrt(hL) + sqrt(hR));
	float cTilde = sqrt(g*hBar);

	gridpoint r1;
	r1.x = 1;
	r1.y = uTilde;
	r1.z = vTilde - cTilde;
	gridpoint r2;
	r2.x = 0;
	r2.y = -1;
	r2.z = 0;
	gridpoint r3;
	r3.x = 1;
	r3.y = uTilde;
	r3.z = vTilde + cTilde;

	gridpoint delta = qR;
	delta = delta - qL;

	gridpoint alpha;
	gridpoint lambda;
	gridpoint w;
	alpha.x = ((vTilde + cTilde)*delta.x - delta.z) / (2 * cTilde);
	alpha.y = uTilde*delta.x - delta.y;
	alpha.z = (-(vTilde - cTilde)*delta.x + delta.z) / (2 * cTilde);
	lambda.x = vTilde - cTilde;
	lambda.y = vTilde;
	lambda.z = vTilde + cTilde;
	w = r1*phi(lambda.x)*alpha.x;
	w = w + r2*phi(lambda.y)*alpha.y;
	w = w + r3*phi(lambda.z)*alpha.z;
	w = w*0.5;



	gridpoint G;
	G.x = 0.5*(qL.z + qR.z);
	G.y = 0.5*(qL.y*qL.z / qL.x + qR.y*qR.z / qR.x);
	G.z = 0.5*(qL.z*qL.z / qL.x + 0.5*g*qL.x*qL.x + qR.z*qR.z / qR.x + 0.5*g*qR.x*qR.x);

	G = G - w;
	return G;

	//lambdaMax = max(lambda);
}
__host__ __device__ gridpoint HorizontalPotential_New(gridpoint gp, gridpoint center)
{
	float h = max(gp.x, 0.0f);
	float uh = gp.y;
	float vh = gp.z;

	float h4 = h * h * h * h;
	float u = sqrtf(2.0f) * h * uh / (sqrtf(h4 + max(h4, EPSILON1)));

	float ll = 1;
	if (u > LIM)
	{
		ll = LIM / u;
	}
	float totalH = center.x + center.w;
	if (gp.w > totalH && gp.x < EPSILON)
	{
		gp.w = totalH;
	}

	gridpoint F;

	F.x = u * h;
	//F.y = uh * u + GRAVITY * h * h + 2*GRAVITY * h * gp.w;
	F.y = uh * u + GRAVITY * center.x * h + GRAVITY * center.x * gp.w;
	F.z = vh * u;


	// 	F.x = 0;
	// 	F.y = GRAVITY * center.x * h + GRAVITY * center.x * gp.w;
	// 	F.z = 0;
	F.w = 0.0f;


	/*float limit = 1.0f;
	if (F.x > h)
	{
	limit = h / F.x;
	}
	F = F*limit;*/
	F = F*ll;
	return F;
}
__host__ __device__ gridpoint HorizontalPotential_New1(gridpoint gp, gridpoint center, gridpoint gp1)
{

	float h = max(gp.x, 0.0f);
	float uh = gp.y;
	float vh = gp.z;
	float h1 = max(gp1.x, 0.0f);
	float uh1 = gp1.y;
	float vh1 = gp1.z;

	float h4 = h * h * h * h;
	float u = sqrtf(2.0f) * h * uh / (sqrtf(h4 + max(h4, EPSILON1)));
	float h14 = h1 * h1 * h1 * h1;
	float u1 = sqrtf(2.0f) * h1 * uh1 / (sqrtf(h14 + max(h14, EPSILON1)));

	float v = gp.z / h;
	float v1 = gp1.z / h1;



	/*float ll = 1;
	if (u < -LIM)
	{
	ll = -LIM / u;
	}*/
	float totalH = center.x + center.w;
	if (gp.w > totalH && gp.x < EPSILON)
	{
		gp.w = totalH;
	}
	if (gp1.w > totalH && gp1.x < EPSILON)
	{
		gp1.w = totalH;
	}

	float nh = (h + h1) / 2;
	float nu = (u + u1) / 2;
	float nv = (v + v1) / 2;
	float s = g*nu*sqrt(nu*nu + nv*nv)*manning*manning / (pow(nh, 1 / 3));
	gridpoint F;

	F.x = (u * h - u1*h1);
	//F.y = uh * u + GRAVITY * h * h + 2*GRAVITY * h * gp.w;

	F.y = (uh * u + GRAVITY * center.x * h + GRAVITY * center.x * gp.w - (uh1 * u1 + GRAVITY * center.x * h1 + GRAVITY * center.x * gp1.w));
	F.z = (vh*u - vh1*u1);
	//F.y = F.y - s;


	// 	F.x = 0;
	// 	F.y = GRAVITY * center.x * h + GRAVITY * center.x * gp.w;
	// 	F.z = 0;
	F.w = 0.0f;
	float ll = 1;
	if (center.x > 0)
	{

		float limit = F.x / (center.x + h + h1);
		if (limit < -LIM)
		{
			ll = -LIM / limit;
		}
		else if (limit > LIM)
		{
			ll = LIM / limit;
		}
	}

	/*float hh = (h + max(center.x, 0)) / 2;
	float limit = 1.0f;
	if (F.x > hh)
	{
	limit = hh / F.x;
	}
	F = F*limit;*/
	F = F*ll;
	if (u < 0 && u1 > 0)
	{
		F = F*0.8;
	}
	return F;
}

__host__ __device__ gridpoint VerticalPotential_New(gridpoint gp, gridpoint center)
{
	float h = max(gp.x, 0.0f);
	float uh = gp.y;
	float vh = gp.z;

	float h4 = h * h * h * h;
	float v = sqrtf(2.0f) * h * vh / (sqrtf(h4 + max(h4, EPSILON1)));

	float ll = 1;
	if (v > LIM)
	{
		ll = LIM / v;
	}
	float totalH = center.x + center.w;
	if (gp.w > totalH && gp.x < EPSILON)
	{
		gp.w = totalH;
	}
	gridpoint G;

	G.x = v * h;
	G.y = uh * v;

	//G.z = vh * v + GRAVITY * h* h + 2*GRAVITY * h * gp.w;

	G.z = vh * v + GRAVITY * center.x * h + GRAVITY * center.x * gp.w;
	// 	G.x = 0;
	// 	G.y = 0;
	// 	G.z = GRAVITY * center.x * h + GRAVITY * center.x * gp.w;
	G.w = 0.0f;

	/*float limit = 1.0f;
	if (G.x > h)
	{
	limit = h / G.x;
	}
	G = G*limit;*/
	G = G*ll;
	return G;
}
__host__ __device__ gridpoint VerticalPotential_New1(gridpoint gp, gridpoint center, gridpoint gp1)
{
	float h = max(gp.x, 0.0f);
	float uh = gp.y;
	float vh = gp.z;
	float h1 = max(gp1.x, 0.0f);
	float uh1 = gp1.y;
	float vh1 = gp1.z;

	float h4 = h * h * h * h;
	float v = sqrtf(2.0f) * h * vh / (sqrtf(h4 + max(h4, EPSILON1)));
	float h14 = h1 * h1 * h1 * h1;
	float v1 = sqrtf(2.0f) * h1 * vh1 / (sqrtf(h14 + max(h14, EPSILON1)));

	float u = gp.y / h;
	float u1 = gp1.y / h1;

	/*float ll = 1;
	if (v < -LIM)
	{
	ll = -LIM / v;
	}*/
	float totalH = center.x + center.w;
	if (gp.w > totalH && gp.x < EPSILON)
	{
		gp.w = totalH;
	}
	if (gp1.w > totalH && gp1.x < EPSILON)
	{
		gp1.w = totalH;
	}
	gridpoint G;
	float nh = (h + h1) / 2;
	float nu = (u + u1) / 2;
	float nv = (v + v1) / 2;
	float s = g*nv*sqrt(nu*nu + nv*nv)*manning*manning / (pow(nh, 1 / 3));


	G.x = (v * h - v1*h1);
	G.y = (uh*v - uh1*v1);

	G.z = (vh * v + GRAVITY * center.x * h + GRAVITY * center.x * gp.w - (vh1 * v1 + GRAVITY * center.x * h1 + GRAVITY * center.x * gp1.w));
	//G.z = G.z - s;

	// 	G.x = 0;
	// 	G.y = 0;
	// 	G.z = GRAVITY * center.x * h + GRAVITY * center.x * gp.w;
	G.w = 0.0f;


	float ll = 1;
	if (center.x > 0)
	{
		float limit = G.x / (center.x + h + h1);
		if (limit < -LIM)
		{
			ll = -LIM / limit;
		}
		else if (limit > LIM)
		{
			ll = LIM / limit;
		}
	}
	//float ll1 = 1.0;
	//if (abs(v-v1)/(abs(v)+abs(v1)) >= 0.5) ll1 = min(1.0,10.0/(abs(v-v1)));
	/*float hh = (h + max(center.x, 0)) / 2;
	float limit = 1.0f;
	if (G.x > hh)
	{
	limit = hh / G.x;
	}
	G = G*limit;*/
	G = G*ll;
	if (v < 0 && v1 > 0)
	{
		G = G*0.8;
	}
	return G;
}

__host__ __device__ gridpoint VerticalPotential_New1206(gridpoint gpn, gridpoint center, gridpoint gpe)
{
	float h;
	float u = gpn.y;
	float v = gpn.z;

	gridpoint G;

	if (v > 0)
	{
		h = gpn.x;
		G.x = v * h;
		G.y = 0;
	}
	else
	{
		h = center.x;
		G.x = v * h;
		float h1, h2;
		if (1);
	}



	/*G.x = v * h;
	G.y = uh*v;
	G.z = vh * v + GRAVITY * center.x * h + GRAVITY * center.x * gp.w;
	G.w = 0.0f;*/

	return G;
}
__host__ __device__ gridpoint VerticalPotential_New2(gridpoint gp, gridpoint center, gridpoint gp1)
{
	float h = max(gp.x, 0.0f);
	float uh = gp.y;
	float vh = gp.z;
	float h1 = max(gp1.x, 0.0f);
	float uh1 = gp1.y;
	float vh1 = gp1.z;

	float h4 = h * h * h * h;
	float v = sqrtf(2.0f) * h * vh / (sqrtf(h4 + max(h4, EPSILON1)));
	float h14 = h1 * h1 * h1 * h1;
	float v1 = sqrtf(2.0f) * h1 * vh1 / (sqrtf(h14 + max(h14, EPSILON1)));

	float u = gp.y / h;
	float u1 = gp1.y / h1;

	float totalH = center.x + center.w;
	if (gp.w > totalH && gp.x < EPSILON)
	{
		gp.w = totalH;
	}
	if (gp1.w > totalH && gp1.x < EPSILON)
	{
		gp1.w = totalH;
	}
	gridpoint G;
	float nh = (h + h1) / 2;
	float nu = (u + u1) / 2;
	float nv = (v + v1) / 2;
	float s = g*nv*sqrt(nu*nu + nv*nv)*manning*manning / (pow(nh, 1 / 3));


	G.x = (v * h - v1*h1);
	G.y = (uh*v - uh1*v1);

	G.z = (vh * v + GRAVITY * center.x * h + GRAVITY * center.x * gp.w - (vh1 * v1 + GRAVITY * center.x * h1 + GRAVITY * center.x * gp1.w));
	//G.z = G.z - s;

	// 	G.x = 0;
	// 	G.y = 0;
	// 	G.z = GRAVITY * center.x * h + GRAVITY * center.x * gp.w;
	G.w = 0.0f;


	float ll = 1;
	if (center.x > 0)
	{
		float limit = G.x / (center.x + h + h1);
		if (limit < -LIM)
		{
			ll = -LIM / limit;
		}
		else if (limit > LIM)
		{
			//ll = LIM / limit;
		}
	}

	G = G*ll;
	return G;
}
__host__ __device__ gridpoint HorizontalPotential(gridpoint gp)
{
	float h = max(gp.x, 0.0f);
	float uh = gp.y;
	float vh = gp.z;

	float h4 = h * h * h * h;
	float u = sqrtf(2.0f) * h * uh / (sqrtf(h4 + max(h4, EPSILON)));

	gridpoint F;
	F.x = u * h;
	F.y = uh * u + GRAVITY * h * h;
	F.z = vh * u;
	F.w = 0.0f;
	return F;
}

__host__ __device__ gridpoint VerticalPotential(gridpoint gp)
{
	float h = max(gp.x, 0.0f);
	float uh = gp.y;
	float vh = gp.z;

	float h4 = h * h * h * h;
	float v = sqrtf(2.0f) * h * vh / (sqrtf(h4 + max(h4, EPSILON)));

	gridpoint G;
	G.x = v * h;
	G.y = uh * v;
	G.z = vh * v + GRAVITY * h * h;
	G.w = 0.0f;
	return G;
}


__host__ __device__ gridpoint SlopeForce(gridpoint c, gridpoint n, gridpoint e, gridpoint s, gridpoint w)
{
	float h = max(c.x, 0.0f);

	gridpoint H;
	H.x = 0.0f;
	H.y = -GRAVITY * h * (e.w - w.w);
	H.z = -GRAVITY * h * (s.w - n.w);
	H.w = 0.0f;
	return H;
}


__host__ __device__ void fixShore(gridpoint& l, gridpoint& c, gridpoint& r)
{

	if (r.x < 0.0f || l.x < 0.0f || c.x < 0.0f)
	{
		c.x = c.x + l.x + r.x;
		c.x = max(0.0f, c.x);
		l.x = 0.0f;
		r.x = 0.0f;
	}
	float h = c.x;
	float h4 = h * h * h * h;
	/*float v = sqrtf(2.0f) * h * c.y / (sqrtf(h4 + max(h4, EPSILON)));
	float u = sqrtf(2.0f) * h * c.z / (sqrtf(h4 + max(h4, EPSILON)));*/
	float u = sqrtf(2.0f) * h * c.y / (sqrtf(h4 + max(h4, EPSILON1)));
	float v = sqrtf(2.0f) * h * c.z / (sqrtf(h4 + max(h4, EPSILON1)));

	c.y = u * h;
	c.z = v * h;
}
__global__ void jianmodel(int a, int b, int c, int d, int pitch, int pitch1, int pitch2, vertex* grid, double* grid1, vertex* grid2)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x <= c&&x >= a&&y <= d&&y >= b)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		vertex center = tex2D(texture_landscape, x, y);
		vertex center2 = tex2D(texture_grid, gridx, gridy);
		double center1 = grid2Dread(grid1, y, x, pitch1);
		grid2Dwrite(grid1, y, x, pitch1, 0);



		center.y = center.y - center1;
		center2.w = center2.w - 0.01*center1;

		grid2Dwrite(grid, x, y, pitch, center);
		grid2Dwrite(grid2, gridx, gridy, pitch2, center2);
	}
}
__global__ void jianmodel2(int a, int b, int c, int d, int pitch, int pitch1, int pitch2, vertex* grid, double* grid1, vertex* grid2)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x <= c&&x >= a&&y <= d&&y >= b)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		vertex center = tex2D(texture_landscape, x, y);
		vertex center2 = tex2D(texture_grid, gridx, gridy);
		double center1 = grid2Dread(grid1, y, x, pitch1);
		grid2Dwrite(grid1, y, x, pitch1, 0);




		grid2Dwrite(grid, x, y, pitch, center);
		//grid2Dwrite(grid2, gridx, gridy, pitch2, center2);
	}
}
__global__ void jiamodel(int a, int b, int c, int d, int pitch, int pitch1, int pitch2, vertex* grid, double* grid1, vertex* grid2)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x <= c&&x >= a&&y <= d + 2 && y >= b)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		vertex center = grid2Dread(grid, x, y, pitch);
		vertex center2 = grid2Dread(grid2, gridx, gridy, pitch2);
		double center1 = grid2Dread(grid1, y, x, pitch1);
		vertex center3 = tex2D(texture_grid, gridx, gridy);
		//double center12 = grid2Dread(grid1, y + 1, x, pitch1);



		//center.y = center.y + 0.01*center1;
		//center2.w = center2.w + 0.01*center1;

		/*double temp = center2.w - center3.w;
		if (temp >center2.x)
		{
		center2.x = 0;
		}
		else if (temp > 0)
		{
		center2.x -= temp;
		}*/
		//center2.w = center2.w + 0.01*center1;
		//double temp = center2.w - center3.w;
		if (center1 > 0)
		{
			if (center2.y < 1)
			{
				center2.y += 1;
			}

		}
		//center2.w = center3.w;
		grid2Dwrite(grid, x, y, pitch, center);
		grid2Dwrite(grid2, gridx, gridy, pitch2, center2);
	}
}
__global__ void bianmodel(int a, int b, int c, int d, int pitch, int pitch1, int pitch2, vertex* grid, double* grid1, vertex* grid2)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x <= c&&x >= a&&y <= d && y >= b)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		vertex center = tex2D(texture_landscape, x, y);
		vertex center2 = tex2D(texture_grid, gridx, gridy);
		double center1 = grid2Dread(grid1, y, x, pitch1);

		center2.w = 0.01*center.y + 0.01*center1;
		if (center1 > 0)
		{
			center2.x = 0;
		}
		//center.y = center.y + center1;
		//grid2Dwrite(grid, x, y, pitch, center);
		grid2Dwrite(grid2, gridx, gridy, pitch2, center2);
	}
}
__global__ void gailocalindex(int i, vertex* grid)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	if (x == i)
	{
		vertex a;
		a.x = 256;
		a.y = 256;
		a.z = 0;
		a.w = 0;
		grid[i] = a;
	}
}
__global__ void computemodelh(int width, int i, int begin, int end, vertex* grid, vertex* grid1, vertex* grid2, vertex* grid3, vertex* fxiangliang)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	if (x < end && x >= begin)
	{
		vertex center = grid[x];
		int a = center.x;
		int b = center.y;
		int c = center.z;
		vertex v1 = grid1[a];
		vertex v2 = grid1[b];
		vertex v3 = grid1[c];
		vertex v4 = local(v1, LONG_KF, LAT_KF);
		vertex v5 = local(v2, LONG_KF, LAT_KF);
		vertex v6 = local(v3, LONG_KF, LAT_KF);
		vertex line1 = p_to_v(v4, v5);
		vertex line2 = p_to_v(v5, v6);
		vertex line = cross(line1, line2);//三个点构成的平面的法向量
		fxiangliang[x] = line;
		vertex ind1 = grid3[x];
		int indx = coord_to_index(min_t(v4.x, v5.x, v6.x), _long_step);
		int indz = coord_to_index(max_t(v4.x, v5.x, v6.x), _long_step);
		int indy = coord_to_index(min_t(v4.y, v5.y, v6.y), _lat_step);
		int indw = coord_to_index(max_t(v4.y, v5.y, v6.y), _lat_step);
		ind1.x = max(indx, 0);
		ind1.y = max(indy, 0);
		ind1.z = min(indz, width - 1);
		ind1.w = min(indw, width - 1);
		vertex d2 = grid2[i];
		vertex d1 = grid2[i];
		d2.x = min(d1.x, ind1.x);
		d2.y = min(d1.y, ind1.y);
		d2.z = max(d1.z, ind1.z);
		d2.w = max(d1.w, ind1.w);
		grid2[i] = d2;
		grid3[x] = ind1;
	}
}
__global__ void compute(int i, int b1, int b2, int e1, int e2, int index1, int index2, int index3, vertex* grid, vertex* grid1, double* grid2, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x >= b1&&x <= e1 && y >= b2&&y <= e2)
	{
		vertex P;
		vertex A = grid[index1];
		vertex B = grid[index2];
		vertex C = grid[index3];
		P.x = index_to_coord(x, LONG_KF, _long_step);
		P.y = index_to_coord(y, LAT_KF, _lat_step);
		bool b1 = PointinTriangle(A, B, C, P);
		bool b2 = PointonTriangle(A, B, C, P);
		if (b1 || b2)
		{
			vertex line = grid1[i];
			if (line.z == 0)
			{
				double ch1 = grid2Dread(grid2, y - 1, x, pitch);
				double ch2 = grid2Dread(grid2, y + 1, x, pitch);
				double center1 = grid2Dread(grid2, y, x, pitch);
				double ch = max_t(ch1, ch2, center1);
				grid2Dwrite(grid2, y, x, pitch, ch);

			}
			else
			{
				double ch = computeh(B, line, P);
				double center1 = grid2Dread(grid2, y, x, pitch);
				if (ch > center1)
				{
					grid2Dwrite(grid2, y, x, pitch, ch);
				}
			}

		}
	}
}
__host__ __device__ float minmod(float a1, float a2)
{

	if (a1*a2 <= 0) return 0;
	else if (abs(a1) > abs(a2))
	{
		return 1.0f;
	}
	else
	{
		return abs(a1 / a2);
	}
}

__host__ __device__ gridpoint reconf(gridpoint reL, gridpoint reR)
{

	float hl4 = reL.x *reL.x *reL.x *reL.x;
	float hr4 = reR.x *reR.x *reR.x *reR.x;
	float ul = sqrtf(2.0f) * reL.x * reL.y / (sqrtf(hl4 + max(hl4, EPSILON)));
	float ur = sqrtf(2.0f) * reR.x * reR.y / (sqrtf(hr4 + max(hr4, EPSILON)));
	float vl = sqrtf(2.0f) * reL.x * reL.z / (sqrtf(hl4 + max(hl4, EPSILON)));
	float vr = sqrtf(2.0f) * reR.x * reR.z / (sqrtf(hr4 + max(hr4, EPSILON)));


	float a1, a2, b1, b2;
	//a1 = max(0, max(ur + sqrt(g*reR.x), ul + sqrt(g*reL.x)));
	//a2 = min(0, min(ur - sqrt(g*reR.x), ul - sqrt(g*reL.x)));
	//b1 = max(0, max(vr + sqrt(g*reR.x), vl + sqrt(g*reL.x)));
	//b2 = min(0, min(vr - sqrt(g*reR.x), vl - sqrt(g*reL.x)));

	reL.y = reL.x*ul;
	reL.z = reL.x*vl;
	reR.y = reR.x*ur;
	reR.z = reR.x*vr;

	gridpoint retl, retr;
	retl.x = reL.y;
	retr.x = reR.y;
	retl.y = reL.y*ul + 0.5*g*reL.x*reL.x;
	retr.y = reR.y*ur + 0.5*g*reR.x*reR.x;
	retl.z = reL.y*vl;
	retr.z = reR.y*vr;

	//gridpoint re = (a1*retl - a2*retr) / (a1 - a2) + (a1*a2)*(reR - reL) / (a1 - a2);
	gridpoint re = 0.5*((retl + retr) - 0.0*(reR - reL));
	/*float re1 = max(reR.x, reL.x);
	float re2 = abs(re.x);
	if (re2 > re1)
	{
	re = (re*re1) / re2;
	}*/
	return re;
}
__host__ __device__ gridpoint recong(gridpoint reL, gridpoint reR)
{
	float hl4 = reL.x *reL.x *reL.x *reL.x;
	float hr4 = reR.x *reR.x *reR.x *reR.x;
	float ul = sqrtf(2.0f) * reL.x * reL.y / (sqrtf(hl4 + max(hl4, EPSILON)));
	float ur = sqrtf(2.0f) * reR.x * reR.y / (sqrtf(hr4 + max(hr4, EPSILON)));
	float vl = sqrtf(2.0f) * reL.x * reL.z / (sqrtf(hl4 + max(hl4, EPSILON)));
	float vr = sqrtf(2.0f) * reR.x * reR.z / (sqrtf(hr4 + max(hr4, EPSILON)));

	float a1, a2, b1, b2;
	//a1 = max(0.0f, max(ur + sqrt(g*reR.x), ul + sqrt(g*reL.x)));
	//a2 = min(0.0f, min(ur - sqrt(g*reR.x), ul - sqrt(g*reL.x)));
	//b1 = max(0.0f, max(vr + sqrt(g*reR.x), vl + sqrt(g*reL.x)));
	//b2 = min(0.0f, min(vr - sqrt(g*reR.x), vl - sqrt(g*reL.x)));
	reL.y = reL.x*ul;
	reL.z = reL.x*vl;
	reR.y = reR.x*ur;
	reR.z = reR.x*vr;
	gridpoint retl, retr;
	retl.x = reL.z;
	retr.x = reR.z;
	retl.y = reL.z*ul;
	retr.y = reR.z*ur;
	retl.z = reL.z*vl + 0.5*g*reL.x*reL.x;
	retr.z = reR.z*vr + 0.5*g*reR.x*reR.x;
	//gridpoint re = (b1*retl - b2*retr)/(b1-b2) + (b1*b2)*(reR - reL)/(b1-b2);
	gridpoint re = 0.5*((retl + retr) - 0.0*(reR - reL));
	/*float re1 = max(reR.x, reL.x);
	float re2 = abs(re.x);
	if (re2 > re1)
	{
	re = (re*re1) / re2;
	}*/
	return re;
}
__host__ __device__ void recon(gridpoint l2, gridpoint l1, gridpoint r1, gridpoint r2, gridpoint &reL, gridpoint &reR)
{

	float limL = minmod(l1.x - l2.x, r1.x - l1.x);
	float limR = minmod(r1.x - r2.x, l1.x - r1.x);
	reL = l1 + 0.5*limL*(l1 - l2);
	reR = r1 + 0.5*limR*(r1 - r2);
	float ul, ur, vl, vr;
	if (reL.x < 0.0001)
	{
		ul = 0;
		vl = 0;
	}
	else
	{
		ul = reL.y / reL.x;
		vl = reL.z / reL.x;
	}
	if (reR.x < 0.0001)
	{
		ur = 0;
		vr = 0;
	}
	else
	{
		ur = reR.y / reR.x;
		vr = reR.z / reR.x;
	}
	/*float hwl, hwr, hw;
	hwl = reL.x + reL.w;
	hwr = reR.x + reR.w;
	float ww;
	ww = max(reL.w, reR.w);
	if (reL.x < 0.0001 || reR.x < 0.0001)
	{
	float deltaz = max(0.0f, max(ww - hwl, ww - hwr));
	ww = ww - deltaz;
	hwl = hwl - deltaz;
	hwr = hwr - deltaz;
	}
	reL.x = max(0.0f, hwl - ww);
	reR.x = max(0.0f, hwr - ww);
	hwl = reL.x + ww;
	hwr = reR.x + ww;
	reL.y = reL.x*ul;
	reR.y = reR.x*ur;
	reL.z = reL.x*vl;
	reR.z = reR.x*vr;
	reL.w = ww;
	reR.w = ww;*/
}
__host__ __device__ gridpoint up_wind_u(gridpoint gp, gridpoint gp2)
{
	float h = max(gp.x, 0.0f);
	float uh = gp.y;
	float vh = gp.z;

	float h4 = h * h * h * h;
	float u = sqrtf(2.0f) * h * uh / (sqrtf(h4 + max(h4, EPSILON)));

	gridpoint F;
	F.x = u * h;
	F.y = uh * u + GRAVITY * h * h;
	F.z = vh * u;
	F.w = 0.0f;
	return F;
}
__global__ void simulateWaveStep4(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);


		fixShore(west, center, east);
		fixShore(north, center, south);

		gridpoint u_center = center;
		if (center.y == 0)
		{
			if (east.y > 0 && west.y>0)
			{
				u_center = u_center + timestep*HorizontalPotential_New1(center, center, west);
			}
			else if (east.y <= 0 && west.y <= 0)
			{
				u_center = u_center + timestep*HorizontalPotential_New1(center, center, east);
			}
		}
		else if (center.y > 0)
		{
			if (east.y >= 0 || west.y >= 0)
			{
				u_center = u_center + timestep*HorizontalPotential_New1(center, center, west);
			}
			else if (east.y < 0 && west.y < 0)
			{
				u_center = u_center + timestep*HorizontalPotential_New1(center, center, east);
			}
		}
		else if (center.y < 0)
		{
			if (east.y <= 0 || west.y <= 0)
			{
				u_center = u_center + timestep*HorizontalPotential_New1(center, center, east);
			}
			else if (east.y > 0 && west.y > 0)
			{
				u_center = u_center + timestep*HorizontalPotential_New1(center, center, west);
			}
		}
		if (center.z == 0)
		{
			if (east.z > 0 && west.z>0)
			{
				u_center = u_center + timestep*VerticalPotential_New1(center, center, north);
			}
			else if (east.z <= 0 && west.z <= 0)
			{
				u_center = u_center + timestep*VerticalPotential_New1(center, center, south);
			}
		}
		else if (center.z > 0)
		{
			if (east.z >= 0 || west.z >= 0)
			{
				u_center = u_center + timestep*VerticalPotential_New1(center, center, north);
			}
			else if (east.z < 0 && west.z < 0)
			{
				u_center = u_center + timestep*VerticalPotential_New1(center, center, south);
			}
		}
		else if (center.z < 0)
		{
			if (east.z <= 0 || west.z <= 0)
			{
				u_center = u_center + timestep*VerticalPotential_New1(center, center, south);
			}
			else if (east.z > 0 && west.z > 0)
			{
				u_center = u_center + timestep*VerticalPotential_New1(center, center, north);
			}
		}




		u_center.x = max(0.0f, u_center.x);

		float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		u_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}

}
__global__ void simulateWaveStep3(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		gridpoint wwest;
		gridpoint eeast;
		gridpoint nnorth;
		gridpoint ssouth;

		if (x == 0)
		{
			wwest = west;
		}
		else
		{
			wwest = tex2D(texture_grid, gridx - 2, gridy);;
		}
		if (x == width - 1)
		{
			eeast = east;
		}
		else
		{
			eeast = tex2D(texture_grid, gridx + 2, gridy);
		}
		if (y == 0)
		{
			nnorth = north;
		}
		else
		{
			nnorth = tex2D(texture_grid, gridx, gridy - 2);
		}
		if (y == height - 1)
		{
			ssouth = south;
		}
		else
		{
			ssouth = tex2D(texture_grid, gridx, gridy + 2);
		}

		fixShore(west, center, east);
		fixShore(north, center, south);
		gridpoint s1, s2, n1, n2, e1, e2, w1, w2;

		recon(north, center, south, ssouth, s1, s2);


		recon(nnorth, north, center, south, n1, n2);



		recon(wwest, west, center, east, w1, w2);



		recon(west, center, east, eeast, e1, e2);

		gridpoint u_south = 0.5f * (s1 + s2) - timestep * (VerticalPotential_New1(s2, s1, s1));

		u_south.x = max(0.0f, u_south.x);

		gridpoint u_north = 0.5f * (n1 + n2) - timestep * (VerticalPotential_New1(n2, n2, n1));

		u_north.x = max(0.0f, u_north.x);

		gridpoint u_west = 0.5f * (w1 + w2) - timestep * (HorizontalPotential_New1(w2, w2, w1));

		u_west.x = max(0.0f, u_west.x);

		gridpoint u_east = 0.5f * (e1 + e2) - timestep * (HorizontalPotential_New1(e2, e1, e1));

		u_east.x = max(0.0f, u_east.x);


		gridpoint u_center = center - timestep * (HorizontalPotential_New1(u_east, center, u_west)) - timestep * (VerticalPotential_New1(u_south, center, u_north));

		u_center.x = max(0.0f, u_center.x);

		float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		u_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}

}
__global__ void lvbo(gridpoint* grid, int width, int height, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int x1 = x + 1;
		int y1 = y + 1;
		float a1 = grid[x1 - 1 + (y1 - 1) * pitch].x;
		float a2 = grid[x1 + (y1 - 1) * pitch].x;
		float a3 = grid[x1 + 1 + (y1 - 1) * pitch].x;
		float a4 = grid[x1 - 1 + y1 * pitch].x;
		float a5 = grid[x1 + y1 * pitch].x;
		float a6 = grid[x1 + 1 + y1 * pitch].x;
		float a7 = grid[x1 - 1 + (y1 + 1) * pitch].x;
		float a8 = grid[x1 + (y1 + 1) * pitch].x;
		float a9 = grid[x1 + 1 + (y1 + 1) * pitch].x;
		grid[x + y * pitch].x = (1 * a1 + 2 * a2 + 1 * a3 + 2 * a4 + 4 * a5 + 2 * a6 + 1 * a7 + 2 * a8 + 1 * a9) / 16;
	}
}
__global__ void simulateWaveStep2(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		gridpoint wwest;
		gridpoint eeast;
		gridpoint nnorth;
		gridpoint ssouth;

		if (x == 0)
		{
			wwest = west;
		}
		else
		{
			wwest = tex2D(texture_grid, gridx - 2, gridy);;
		}
		if (x == width - 1)
		{
			eeast = east;
		}
		else
		{
			eeast = tex2D(texture_grid, gridx + 2, gridy);
		}
		if (y == 0)
		{
			nnorth = north;
		}
		else
		{
			nnorth = tex2D(texture_grid, gridx, gridy - 2);
		}
		if (y == height - 1)
		{
			ssouth = south;
		}
		else
		{
			ssouth = tex2D(texture_grid, gridx, gridy + 2);
		}
		gridpoint s1, s2, n1, n2, e1, e2, w1, w2;

		recon(north, center, south, ssouth, s1, s2);
		recon(nnorth, north, center, south, n1, n2);
		recon(wwest, west, center, east, w1, w2);
		recon(west, center, east, eeast, e1, e2);

		gridpoint u_south = recong(s1, s2);
		gridpoint u_north = recong(n1, n2);
		gridpoint u_west = reconf(w1, w2);
		gridpoint u_east = reconf(e1, e2);



		gridpoint SH;
		SH.x = 0;
		SH.y = -0.5*g*(e1.w - w1.w)*(e1.x + w2.x) / deltax;
		SH.z = -0.5*g*(s1.w - n1.w)*(s1.x + n2.x) / deltay;
		SH.w = 0;
		gridpoint moca;
		float h = center.x;
		float hu = center.y;
		float hv = center.z;
		float u = hu / h;
		float v = hv / h;
		moca.x = 0;
		moca.y = g*u*sqrt(u*u + v*v)*manning*manning / (pow(h, 4 / 3));
		moca.z = g*v*sqrt(u*u + v*v)*manning*manning / (pow(h, 4 / 3));

		gridpoint u_center = center - (deltat / deltax) * (u_east - u_west) - (deltat / deltay) * (u_south - u_north) + deltat*SH;
		float huk = u_center.y / (1 + deltat*moca.y);
		float hvk = u_center.z / (1 + deltat*moca.z);
		//u_center.y = huk;
		//u_center.z = hvk;

		u_center.x = max(0.0f, u_center.x);

		float totalH = center.x + center.w;
		if (u_center.x <= EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		u_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}

}

__global__ void simulateWaveStep1(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);




		gridpoint u_south = riemannY(center, south);


		gridpoint u_north = riemannY(north, center);



		gridpoint u_west = riemannX(west, center);



		gridpoint u_east = riemannX(center, east);



		gridpoint u_center = center + timestep * SlopeForce(center, north, east, south, west) - timestep * (u_east - u_west) - timestep * (u_south - u_north);

		u_center.x = max(0.0f, u_center.x);

		float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		u_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}

}
__global__ void simulateWaveStep(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		fixShore(west, center, east);
		fixShore(north, center, south);


		gridpoint u_south = 0.5f * (south + center) - timestep * (VerticalPotential_New1(south, center, center));

		//gridpoint u_south = 0.5f * (south + center) - timestep * (VerticalPotential_New1(south, center) - VerticalPotential_New(center, center));
		u_south.x = max(0.0f, u_south.x);


		gridpoint u_north = 0.5f * (north + center) - timestep * (VerticalPotential_New1(center, center, north));

		//gridpoint u_north = 0.5f * (north + center) - timestep * (VerticalPotential_New1(center, center) - VerticalPotential_New(north, center));
		u_north.x = max(0.0f, u_north.x);


		gridpoint u_west = 0.5f * (west + center) - timestep * (HorizontalPotential_New1(center, center, west));

		//gridpoint u_west = 0.5f * (west + center) - timestep * (HorizontalPotential_New1(center, center) - HorizontalPotential_New(west, center));
		u_west.x = max(0.0f, u_west.x);


		gridpoint u_east = 0.5f * (east + center) - timestep * (HorizontalPotential_New1(east, center, center));

		//gridpoint u_east = 0.5f * (east + center) - timestep * (HorizontalPotential_New1(east, center) - HorizontalPotential_New(center, center));
		u_east.x = max(0.0f, u_east.x);


		gridpoint u_center = center - timestep * (HorizontalPotential_New1(u_east, center, u_west)) - timestep * (VerticalPotential_New1(u_south, center, u_north));


		u_center.x = max(0.0f, u_center.x);

		float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		if (center.x > 0 && u_center.x == 0)
		{
			u_center.x = EPSILON;
		}
		if (u_center.x != 0)
		{
			if (u_center.y / u_center.x > LIM) u_center.y = u_center.x*LIM;
			if (u_center.z / u_center.x > LIM) u_center.z = u_center.x*LIM;
			if (u_center.y / u_center.x < -LIM) u_center.y = -u_center.x*LIM;
			if (u_center.z / u_center.x < -LIM) u_center.z = -u_center.x*LIM;
		}
		u_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}

}
__global__ void simulateWaveStep1206(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		fixShore(west, center, east);
		fixShore(north, center, south);


		gridpoint u_south = 0.5f * (south + center) - timestep * (VerticalPotential_New1(south, center, center));

		//gridpoint u_south = 0.5f * (south + center) - timestep * (VerticalPotential_New1(south, center) - VerticalPotential_New(center, center));
		u_south.x = max(0.0f, u_south.x);


		gridpoint u_north = 0.5f * (north + center) - timestep * (VerticalPotential_New1(center, center, north));

		//gridpoint u_north = 0.5f * (north + center) - timestep * (VerticalPotential_New1(center, center) - VerticalPotential_New(north, center));
		u_north.x = max(0.0f, u_north.x);


		gridpoint u_west = 0.5f * (west + center) - timestep * (HorizontalPotential_New1(center, center, west));

		//gridpoint u_west = 0.5f * (west + center) - timestep * (HorizontalPotential_New1(center, center) - HorizontalPotential_New(west, center));
		u_west.x = max(0.0f, u_west.x);


		gridpoint u_east = 0.5f * (east + center) - timestep * (HorizontalPotential_New1(east, center, center));

		//gridpoint u_east = 0.5f * (east + center) - timestep * (HorizontalPotential_New1(east, center) - HorizontalPotential_New(center, center));
		u_east.x = max(0.0f, u_east.x);


		gridpoint u_center = center - timestep * (HorizontalPotential_New1(u_east, center, u_west)) - timestep * (VerticalPotential_New1(u_south, center, u_north));


		u_center.x = max(0.0f, u_center.x);

		float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		if (center.x > 0 && u_center.x == 0)
		{
			u_center.x = EPSILON;
		}
		if (u_center.x != 0)
		{
			if (u_center.y / u_center.x > LIM) u_center.y = u_center.x*LIM;
			if (u_center.z / u_center.x > LIM) u_center.z = u_center.x*LIM;
			if (u_center.y / u_center.x < -LIM) u_center.y = -u_center.x*LIM;
			if (u_center.z / u_center.x < -LIM) u_center.z = -u_center.x*LIM;
		}
		u_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}

}
__global__ void simulateWaveStepp(gridpoint* grid_next, gridpoint* grid, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		gridpoint old_center = tex2D(texture_grid, gridx, gridy);

		gridpoint center = grid2Dread(grid, gridx, gridy, pitch);

		gridpoint north = grid2Dread(grid, gridx, gridy - 1, pitch);

		gridpoint west = grid2Dread(grid, gridx - 1, gridy, pitch);

		gridpoint south = grid2Dread(grid, gridx, gridy + 1, pitch);

		gridpoint east = grid2Dread(grid, gridx + 1, gridy, pitch);

		fixShore(west, center, east);
		fixShore(north, center, south);


		gridpoint u_south = 0.5f * (south + center) - timestep * (VerticalPotential_New1(south, center, center));

		//gridpoint u_south = 0.5f * (south + center) - timestep * (VerticalPotential_New1(south, center) - VerticalPotential_New(center, center));
		u_south.x = max(0.0f, u_south.x);


		gridpoint u_north = 0.5f * (north + center) - timestep * (VerticalPotential_New1(center, center, north));

		//gridpoint u_north = 0.5f * (north + center) - timestep * (VerticalPotential_New1(center, center) - VerticalPotential_New(north, center));
		u_north.x = max(0.0f, u_north.x);


		gridpoint u_west = 0.5f * (west + center) - timestep * (HorizontalPotential_New1(center, center, west));

		//gridpoint u_west = 0.5f * (west + center) - timestep * (HorizontalPotential_New1(center, center) - HorizontalPotential_New(west, center));
		u_west.x = max(0.0f, u_west.x);


		gridpoint u_east = 0.5f * (east + center) - timestep * (HorizontalPotential_New1(east, center, center));

		//gridpoint u_east = 0.5f * (east + center) - timestep * (HorizontalPotential_New1(east, center) - HorizontalPotential_New(center, center));
		u_east.x = max(0.0f, u_east.x);


		gridpoint u_center = center - timestep * (HorizontalPotential_New1(u_east, center, u_west)) - timestep * (VerticalPotential_New1(u_south, center, u_north));


		u_center.x = max(0.0f, u_center.x);

		float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		u_center.w = center.w;
		gridpoint uu_center = 0.5*old_center + 0.5*u_center;

		uu_center.x = max(0.0f, uu_center.x);

		if (uu_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			uu_center.y = 0;
			uu_center.z = 0;
		}
		uu_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, uu_center);

	}

}

__global__ void simulateWaveStep_1202(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		fixShore(west, center, east);
		fixShore(north, center, south);


		gridpoint u_south = center - timestep * (VerticalPotential_New1(south, center, center));

		//gridpoint u_south = 0.5f * (south + center) - timestep * (VerticalPotential_New1(south, center) - VerticalPotential_New(center, center));
		u_south.x = max(0.0f, u_south.x);


		gridpoint u_north = north - timestep * (VerticalPotential_New1(center, center, north));

		//gridpoint u_north = 0.5f * (north + center) - timestep * (VerticalPotential_New1(center, center) - VerticalPotential_New(north, center));
		u_north.x = max(0.0f, u_north.x);


		gridpoint u_west = west - timestep * (HorizontalPotential_New1(center, center, west));

		//gridpoint u_west = 0.5f * (west + center) - timestep * (HorizontalPotential_New1(center, center) - HorizontalPotential_New(west, center));
		u_west.x = max(0.0f, u_west.x);


		gridpoint u_east = center - timestep * (HorizontalPotential_New1(east, center, center));

		//gridpoint u_east = 0.5f * (east + center) - timestep * (HorizontalPotential_New1(east, center) - HorizontalPotential_New(center, center));
		u_east.x = max(0.0f, u_east.x);


		gridpoint u_center = 0.5*(center + u_south + center + u_west) - 0.5*timestep * (HorizontalPotential_New1(u_east, center, u_west)) - 0.5*timestep * (VerticalPotential_New1(u_south, center, u_north));
		u_center.x = center.x - timestep*(0.5*(east.x + center.x)*east.y / east.x - 0.5*(west.x + center.x)*center.y / center.x + 0.5*(south.x + center.x)*south.z / south.x - 0.5*(north.x + center.x)*center.z / center.x);

		u_center.x = max(0.0f, u_center.x);

		float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		if (center.x > 0 && u_center.x == 0)
		{
			u_center.x = EPSILON;
		}
		if (u_center.x != 0)
		{
			if (u_center.y / u_center.x > LIM) u_center.y = u_center.x*LIM;
			if (u_center.z / u_center.x > LIM) u_center.z = u_center.x*LIM;
			if (u_center.y / u_center.x < -LIM) u_center.y = -u_center.x*LIM;
			if (u_center.z / u_center.x < -LIM) u_center.z = -u_center.x*LIM;
		}
		u_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}

}
__device__ gridpoint chazhisudu1(float x, float y)
{
	//先求h,w
	int ix = x;
	int iy = y;
	gridpoint h1 = tex2D(texture_grid, ix, iy);
	gridpoint h2 = tex2D(texture_grid, ix + 1, iy);
	gridpoint h3 = tex2D(texture_grid, ix, iy + 1);
	gridpoint h4 = tex2D(texture_grid, ix + 1, iy + 1);
	//X方向插值
	float rh1 = x - ix;
	float hxc1 = (1 - rh1)*h1.x + rh1*h2.x;
	float hxc2 = (1 - rh1)*h3.x + rh1*h4.x;
	float hwc1 = (1 - rh1)*h1.w + rh1*h2.w;
	float hwc2 = (1 - rh1)*h3.w + rh1*h4.w;

	//Y方向差值
	float rh2 = y - iy;
	float hyc = (1 - rh2)*hxc1 + rh2*hxc2;
	float hwc = (1 - rh2)*hwc1 + rh2*hwc2;

	//求u
	float ux = x + 0.5;
	float uy = y;
	int iux = ux;
	int iuy = uy;
	gridpoint n1 = tex2D(texture_grid, iux, iuy);
	gridpoint n2 = tex2D(texture_grid, iux + 1, iuy);
	gridpoint n3 = tex2D(texture_grid, iux, iuy + 1);
	gridpoint n4 = tex2D(texture_grid, iux + 1, iuy + 1);
	//X方向插值
	float r1 = ux - iux;
	float uxc1 = (1 - r1)*n1.y + r1*n2.y;
	float uxc2 = (1 - r1)*n3.y + r1*n4.y;

	//Y方向差值
	float r2 = uy - iuy;
	float uyc = (1 - r2)*uxc1 + r2*uxc2;

	//再求v
	float vx = x;
	float vy = y + 0.5;
	int ivx = vx;
	int ivy = vy;
	gridpoint m1 = tex2D(texture_grid, ivx, ivy);
	gridpoint m2 = tex2D(texture_grid, ivx + 1, ivy);
	gridpoint m3 = tex2D(texture_grid, ivx, ivy + 1);
	gridpoint m4 = tex2D(texture_grid, ivx + 1, ivy + 1);

	//X方向插值
	float t1 = vx - ivx;
	float vxc1 = (1 - t1)*m1.z + t1*m2.z;
	float vxc2 = (1 - t1)*m3.z + t1*m4.z;

	//Y方向差值
	float t2 = vy - ivy;
	float vyc = (1 - t2)*vxc1 + t2*vxc2;
	gridpoint re;
	re.x = hyc;
	re.y = uyc;
	re.z = vyc;
	re.w = hwc;
	return re;

}
__host__ __device__ gridpoint chazhisudu2(gridpoint *grid, float x, float y, int pitch)
{
	//先求h
	int ix = x;
	int iy = y;
	gridpoint h1 = grid2Dread(grid, ix, iy, pitch);
	gridpoint h2 = grid2Dread(grid, ix + 1, iy, pitch);
	gridpoint h3 = grid2Dread(grid, ix, iy + 1, pitch);
	gridpoint h4 = grid2Dread(grid, ix + 1, iy + 1, pitch);
	//X方向插值
	float rh1 = x - ix;
	float hxc1 = (1 - rh1)*h1.x + rh1*h2.x;
	float hxc2 = (1 - rh1)*h3.x + rh1*h4.x;
	float hwc1 = (1 - rh1)*h1.w + rh1*h2.w;
	float hwc2 = (1 - rh1)*h3.w + rh1*h4.w;

	//Y方向差值
	float rh2 = y - iy;
	float hyc = (1 - rh2)*hxc1 + rh2*hxc2;
	float hwc = (1 - rh2)*hwc1 + rh2*hwc2;

	//求u
	float ux = x + 0.5;
	float uy = y;
	int iux = ux;
	int iuy = uy;
	gridpoint n1 = grid2Dread(grid, iux, iuy, pitch);
	gridpoint n2 = grid2Dread(grid, iux + 1, iuy, pitch);
	gridpoint n3 = grid2Dread(grid, iux, iuy + 1, pitch);
	gridpoint n4 = grid2Dread(grid, iux + 1, iuy + 1, pitch);

	//X方向插值
	float r1 = ux - iux;
	float uxc1 = (1 - r1)*n1.y + r1*n2.y;
	float uxc2 = (1 - r1)*n3.y + r1*n4.y;

	//Y方向差值
	float r2 = uy - iuy;
	float uyc = (1 - r2)*uxc1 + r2*uxc2;

	//再求v
	float vx = x;
	float vy = y + 0.5;
	int ivx = vx;
	int ivy = vy;
	gridpoint m1 = grid2Dread(grid, ivx, ivy, pitch);
	gridpoint m2 = grid2Dread(grid, ivx + 1, ivy, pitch);
	gridpoint m3 = grid2Dread(grid, ivx, ivy + 1, pitch);
	gridpoint m4 = grid2Dread(grid, ivx + 1, ivy + 1, pitch);

	//X方向插值
	float t1 = vx - ivx;
	float vxc1 = (1 - t1)*m1.z + t1*m2.z;
	float vxc2 = (1 - t1)*m3.z + t1*m4.z;

	//Y方向差值
	float t2 = vy - ivy;
	float vyc = (1 - t2)*vxc1 + t2*vxc2;
	gridpoint re;
	re.x = hyc;
	re.y = uyc;
	re.z = vyc;
	re.w = hwc;
	return re;

}
__global__ void s_advection1(gridpoint* grid_next, int width, int height, float timestep, int pitch)//网格左上侧的速度值存储在当前网格，右下侧的速度值存储在下一个网格，边界处速度恒为0
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;


	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
		gridpoint westsouth = tex2D(texture_grid, gridx - 1, gridy + 1);
		gridpoint eastnorth = tex2D(texture_grid, gridx + 1, gridy - 1);
		gridpoint eastsouth = tex2D(texture_grid, gridx + 1, gridy + 1);
		//gridpoint westsouthsouth = tex2D(texture_grid, gridx - 1, gridy + 2);
		//gridpoint southsouth = tex2D(texture_grid, gridx, gridy + 2);
		//gridpoint easteastnorth = tex2D(texture_grid, gridx + 2, gridy - 1);
		//gridpoint easteast = tex2D(texture_grid, gridx + 2, gridy);

		float u_next, v_next;
		float u = center.y;
		float v = center.z;
		//X方向
		float cha_v = (west.z + center.z + south.z + westsouth.z) / 4;
		float udx = u*timestep;
		float udy = cha_v*timestep;
		float ux = gridx - 0.5 - udx;
		float uy = gridy - udy;
		gridpoint sdx = chazhisudu1(ux, uy);
		float u_new = (sdx.y + u) / 2;
		float cha_v_new = (sdx.z + cha_v) / 2;
		float ux_new = gridx - 0.5 - u_new*timestep;
		float uy_new = gridy - cha_v_new*timestep;
		gridpoint sdx_new = chazhisudu1(ux_new, uy_new);

		u_next = sdx_new.y;
		//Y方向
		float cha_u = (center.y + north.y + east.y + eastnorth.y) / 4;
		float vdy = v*timestep;
		float vdx = cha_u*timestep;
		float vx = gridx - vdx;
		float vy = gridy - 0.5 - vdy;
		gridpoint sdy = chazhisudu1(vx, vy);
		float v_new = (sdy.z + v) / 2;
		float cha_u_new = (sdy.y + cha_u) / 2;
		float vx_new = gridx - cha_u_new*timestep;
		float vy_new = gridy - 0.5 - v_new*timestep;
		gridpoint sdy_new = chazhisudu1(vx_new, vy_new);

		v_next = sdy_new.z;

		/*if ((u_next > 0 && west.x <= EPSILON1)||(u_next<0&&center.x<=EPSILON1))
		{
		u_next = 0;
		}
		if ((v_next > 0 && north.x <= EPSILON1)||(v_next<0&&center.x<=EPSILON1))
		{
		v_next = 0;
		}*/
		if (u_next > LIM) u_next = LIM;
		if (u_next < -LIM) u_next = -LIM;
		if (v_next > LIM) v_next = LIM;
		if (v_next < -LIM) v_next = -LIM;

		gridpoint u_center;
		u_center.x = center.x;
		u_center.y = u_next;
		u_center.z = v_next;
		u_center.w = center.w;
		/*	float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
		u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
		u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
		u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
		u_center.z = 0;
		}*/

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void s_advection2(gridpoint* grid, gridpoint* grid_next, int width, int height, float timestep, int pitch)//网格左上侧的速度值存储在当前网格，右下侧的速度值存储在下一个网格，边界处速度恒为0
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;


	if (x < width &&y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
		gridpoint westsouth = tex2D(texture_grid, gridx - 1, gridy + 1);
		gridpoint eastnorth = tex2D(texture_grid, gridx + 1, gridy - 1);
		gridpoint eastsouth = tex2D(texture_grid, gridx + 1, gridy + 1);
		gridpoint center_ = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north_ = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west_ = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south_ = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east_ = grid2Dread(grid, gridx + 1, gridy, pitch);
		gridpoint westsouth_ = grid2Dread(grid, gridx - 1, gridy + 1, pitch);
		gridpoint eastnorth_ = grid2Dread(grid, gridx + 1, gridy - 1, pitch);
		gridpoint westnorth_ = grid2Dread(grid, gridx - 1, gridy - 1, pitch);
		//gridpoint northnorth_ = grid2Dread(grid, gridx, gridy - 2, pitch);
		//gridpoint eastnorthnorth_ = grid2Dread(grid, gridx + 1, gridy - 2, pitch);

		float u_next, v_next;
		float u = center.y;
		float u_ = center_.y;
		float v = center.z;
		float v_ = center_.z;



		//X方向
		float cha_v_ = (west_.z + center_.z + south_.z + westsouth_.z) / 4;
		float udx = u_*timestep;
		float udy = cha_v_*timestep;
		float ux = gridx - 0.5 + udx;
		float uy = gridy + udy;
		gridpoint sdx = chazhisudu2(grid, ux, uy, pitch);
		float u_new = (sdx.y + u_) / 2;
		float cha_v_new = (sdx.z + cha_v_) / 2;
		float ux_new = gridx - 0.5 + u_new*timestep;
		float uy_new = gridy + cha_v_new*timestep;
		gridpoint sdx_new = chazhisudu2(grid, ux_new, uy_new, pitch);

		float wuchax = (u - sdx_new.y) / 2;


		u_next = u_ + wuchax;

		float cha_v = (west.z + center.z + south.z + westsouth.z) / 4;
		udx = u*timestep;
		udy = cha_v*timestep;
		ux = gridx - udx;
		uy = gridy - udy;
		int iux = ux;
		int iuy = uy;
		gridpoint n1 = tex2D(texture_grid, iux, iuy);
		gridpoint n2 = tex2D(texture_grid, iux + 1, iuy);
		gridpoint n3 = tex2D(texture_grid, iux, iuy + 1);
		gridpoint n4 = tex2D(texture_grid, iux + 1, iuy + 1);
		float minu = min(n1.y, min(n2.y, min(n3.y, n4.y)));
		float maxu = max(n1.y, max(n2.y, max(n3.y, n4.y)));
		if (u_next > maxu || u_next < minu)
		{
			u_next = u_;
		}




		//Y方向
		float cha_u_ = (center_.y + north_.y + east_.y + eastnorth_.y) / 4;
		float vdy = v_*timestep;
		float vdx = cha_u_*timestep;
		float vx = gridx + vdx;
		float vy = gridy - 0.5 + vdy;
		gridpoint sdy = chazhisudu2(grid, vx, vy, pitch);
		float v_new = (sdy.z + v_) / 2;
		float cha_u_new = (sdy.y + cha_u_) / 2;
		float vx_new = gridx + cha_u_new*timestep;
		float vy_new = gridy - 0.5 + v_new*timestep;
		gridpoint sdy_new = chazhisudu2(grid, vx_new, vy_new, pitch);

		float wuchay = (v - sdy_new.z) / 2;



		v_next = v_ + wuchay;

		float cha_u = (center.y + north.y + east.y + eastnorth.y) / 4;
		vdy = v*timestep;
		vdx = cha_u*timestep;
		vx = gridx - vdx;
		vy = gridy - vdy;
		int ivx = vx;
		int ivy = vy;
		gridpoint m1 = tex2D(texture_grid, ivx, ivy);
		gridpoint m2 = tex2D(texture_grid, ivx + 1, ivy);
		gridpoint m3 = tex2D(texture_grid, ivx, ivy + 1);
		gridpoint m4 = tex2D(texture_grid, ivx + 1, ivy + 1);

		float minv = min(m1.z, min(m2.z, min(m3.z, m4.z)));
		float maxv = max(m1.z, max(m2.z, max(m3.z, m4.z)));
		if (v_next > maxv || v_next < minv)
		{
			v_next = v_;
		}





		if ((u_next > 0 && west.x <= EPSILON1) || (u_next<0 && center.x <= EPSILON1))
		{
			u_next = 0;
		}
		if ((v_next > 0 && north.x <= EPSILON1) || (v_next<0 && center.x <= EPSILON1))
		{
			v_next = 0;
		}
		if (u_next > LIM) u_next = LIM;
		if (u_next < -LIM) u_next = -LIM;
		if (v_next > LIM) v_next = LIM;
		if (v_next < -LIM) v_next = -LIM;

		gridpoint u_center;
		u_center.x = center.x;
		u_center.y = u_next;
		u_center.z = v_next;
		u_center.w = center.w;

		float totalH = center.x + center.w;
		if ((west_.w > totalH) && (west_.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west_.x + west_.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north_.x + north_.w)))
		{
			u_center.z = 0;
		}
		if ((north_.w > totalH) && (north_.x<EPSILON1))
		{
			u_center.z = 0;
		}

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}

//__global__ void s_advection(gridpoint* grid_next, int width, int height, float timestep, int pitch)//对速度高度进行advection
//{
//	int x = threadIdx.x + blockIdx.x * blockDim.x;
//	int y = threadIdx.y + blockIdx.y * blockDim.y;
//
//
//	if (x < width && y < height)
//	{
//		int gridx = x + 1;
//		int gridy = y + 1;
//
//		gridpoint center = tex2D(texture_grid, gridx, gridy);
//		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
//		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
//		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
//		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
//		gridpoint westsouth = tex2D(texture_grid, gridx - 1, gridy + 1);
//		gridpoint eastnorth = tex2D(texture_grid, gridx + 1, gridy - 1);
//
//		float u_next, v_next, h_next;
//		float u = center.y;
//		float v = center.z;
//		float ur = east.y;
//		float vs = south.z;
//
//
//		//X方向
//		float cha_v = (west.z + center.z + south.z + westsouth.z) / 4;
//		float udx = u*timestep;
//		float udy = cha_v*timestep;
//		float ux = gridx - 0.5 - udx;
//		float uy = gridy - udy;
//		if (ux < 1) ux = 1;
//		if (uy < 1) uy = 1;
//		if (ux >= width) ux = width;
//		if (uy >= height) uy = height;
//		gridpoint sdx = chazhisudu1(ux, uy);
//		float u_new = (sdx.y + u) / 2;
//		float cha_v_new = (sdx.z + cha_v) / 2;
//		float ux_new = gridx - 0.5 - u_new*timestep;
//		float uy_new = gridy - cha_v_new*timestep;
//		gridpoint sdx_new = chazhisudu1(ux_new, uy_new);
//
//		u_next = sdx.y;
//		//Y方向
//		float cha_u = (center.y + north.y + east.y + eastnorth.y) / 4;
//		float vdy = v*timestep;
//		float vdx = cha_u*timestep;
//		float vx = gridx - vdx;
//		float vy = gridy - 0.5 - vdy;
//		if (vx < 1) vx = 1;
//		if (vy < 1) vy = 1;
//		if (vx >= width) vx = width;
//		if (vy >= height) vy = height;
//		gridpoint sdy = chazhisudu1(vx, vy);
//		float v_new = (sdy.z + v) / 2;
//		float cha_u_new = (sdy.y + cha_u) / 2;
//		float vx_new = gridx - cha_u_new*timestep;
//		float vy_new = gridy - 0.5 - v_new*timestep;
//		gridpoint sdy_new = chazhisudu1(vx_new, vy_new);
//
//		v_next = sdy.z;
//
//		//水深advection
//		float u_s = (u + ur) / 2;
//		float v_s = (v + vs) / 2;
//		float hdy = v_s*timestep;
//		float hdx = u_s*timestep;
//		float hx = gridx - hdx;
//		float hy = gridy - hdy;
//		if (hx < 1) hx = 1;
//		if (hy < 1) hy = 1;
//		if (hx >= width) hx = width;
//		if (hy >= height) hy = height;
//		gridpoint sdh = chazhisudu1(hx, hy);
//		float u_sn = (sdh.y + u_s) / 2;
//		float v_sn = (sdh.z + v_s) / 2;
//		float hx_n = gridx - u_sn*timestep;
//		float hy_n = gridy - v_sn*timestep;
//		gridpoint sdh_new = chazhisudu1(hx_n, hy_n);
//
//		h_next = sdh.x;
//		float w_next = sdh.w;
//
//
//
//
//
//		if (x == 0)
//		{
//			u_next = 0;
//		}
//		if (y == 0)
//		{
//			v_next = 0;
//		}
//		if ((u_next > 0 && west.x <= EPSILON1) || (u_next<0 && center.x <= EPSILON1))
//		{
//			u_next = 0;
//		}
//		if ((v_next > 0 && north.x <= EPSILON1) || (v_next<0 && center.x <= EPSILON1))
//		{
//			v_next = 0;
//		}
//
//
//		float hl, hn;
//		float a = 1.0;
//		float b = 1.0 - a;
//		if (u_next <= 0)
//		{
//			hl = a*center.x + b*west.x;
//		}
//		else
//		{
//			hl = a*west.x + b*center.x;
//		}
//		if (v_next <= 0)
//		{
//			hn = a*center.x + b*north.x;
//		}
//		else
//		{
//			hn = a*north.x + b*center.x;
//		}
//
//
//
//
//
//		float hl4 = hl*hl*hl*hl;
//		float uu = sqrtf(2.0f) * hl * hl*u_next / (sqrtf(hl4 + max(hl4, EPSILON1)));
//		float hn4 = hn*hn*hn*hn;
//		float vv = sqrtf(2.0f) * hn * hn*v_next / (sqrtf(hn4 + max(hn4, EPSILON1)));
//
//
//		gridpoint u_center;
//
//		//u_center.x = center.x + center.w;//总高度
//		u_center.x = center.x;
//		//u_center.x = h_next;
//		u_center.y = u_next;
//		u_center.z = v_next;
//		//u_center.w = center.w;
//		u_center.w = h_next;//w分量来存储advection后的水深
//							//u_center.w = h_next + w_next;//平流水深
//		float totalH = center.x + center.w;
//		if ((west.w > totalH) && (west.x<EPSILON1))
//		{
//			u_center.y = 0;
//		}
//		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
//		{
//			u_center.y = 0;
//		}
//		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
//		{
//			u_center.z = 0;
//		}
//		if ((north.w > totalH) && (north.x<EPSILON1))
//		{
//			u_center.z = 0;
//		}
//		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
//
//	}
//}
__global__ void s_advection(gridpoint* grid_next, int width, int height, float timestep, int pitch)//对速度高度进行advection
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;


	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
		gridpoint westsouth = tex2D(texture_grid, gridx - 1, gridy + 1);
		gridpoint eastnorth = tex2D(texture_grid, gridx + 1, gridy - 1);

		float u_next, v_next, h_next;
		float u = center.y;
		float v = center.z;
		float ur = east.y;
		float vs = south.z;


		//X方向
		float cha_v = (west.z + center.z + south.z + westsouth.z) / 4;
		float udx = u*timestep;
		float udy = cha_v*timestep;
		float ux = gridx - 0.5 - udx;
		float uy = gridy - udy;
		if (ux < 1) ux = 1;
		if (uy < 1) uy = 1;
		if (ux > width) ux = width;
		if (uy > height) uy = height;
		gridpoint sdx = chazhisudu1(ux, uy);
		float u_new = (sdx.y + u) / 2;
		float cha_v_new = (sdx.z + cha_v) / 2;
		float ux_new = gridx - 0.5 - u_new*timestep;
		float uy_new = gridy - cha_v_new*timestep;
		gridpoint sdx_new = chazhisudu1(ux_new, uy_new);

		u_next = sdx.y;
		//Y方向
		float cha_u = (center.y + north.y + east.y + eastnorth.y) / 4;
		float vdy = v*timestep;
		float vdx = cha_u*timestep;
		float vx = gridx - vdx;
		float vy = gridy - 0.5 - vdy;
		if (vx < 1) vx = 1;
		if (vy < 1) vy = 1;
		if (vx >= width) vx = width;
		if (vy >= height) vy = height;
		gridpoint sdy = chazhisudu1(vx, vy);
		float v_new = (sdy.z + v) / 2;
		float cha_u_new = (sdy.y + cha_u) / 2;
		float vx_new = gridx - cha_u_new*timestep;
		float vy_new = gridy - 0.5 - v_new*timestep;
		gridpoint sdy_new = chazhisudu1(vx_new, vy_new);

		v_next = sdy.z;

		//水深advection
		float u_s = (u + ur) / 2;
		float v_s = (v + vs) / 2;
		float hdy = v_s*timestep;
		float hdx = u_s*timestep;
		float hx = gridx - hdx;
		float hy = gridy - hdy;
		if (hx < 1) hx = 1;
		if (hy < 1) hy = 1;
		if (hx >= width) hx = width;
		if (hy >= height) hy = height;
		gridpoint sdh = chazhisudu1(hx, hy);
		float u_sn = (sdh.y + u_s) / 2;
		float v_sn = (sdh.z + v_s) / 2;
		float hx_n = gridx - u_sn*timestep;
		float hy_n = gridy - v_sn*timestep;
		gridpoint sdh_new = chazhisudu1(hx_n, hy_n);

		h_next = sdh.x;
		float w_next = sdh.w;





		if (x == 0)
		{
			u_next = 0;
		}
		if (y == 0)
		{
			v_next = 0;
		}
		if ((u_next > 0 && west.x <= EPSILON1) || (u_next<0 && center.x <= EPSILON1))
		{
			u_next = 0;
		}
		if ((v_next > 0 && north.x <= EPSILON1) || (v_next<0 && center.x <= EPSILON1))
		{
			v_next = 0;
		}


		float hl, hn;
		float a = 1.0;
		float b = 1.0 - a;
		if (u_next <= 0)
		{
			hl = a*center.x + b*west.x;
		}
		else
		{
			hl = a*west.x + b*center.x;
		}
		if (v_next <= 0)
		{
			hn = a*center.x + b*north.x;
		}
		else
		{
			hn = a*north.x + b*center.x;
		}





		float hl4 = hl*hl*hl*hl;
		float uu = sqrtf(2.0f) * hl * hl*u_next / (sqrtf(hl4 + max(hl4, EPSILON1)));
		float hn4 = hn*hn*hn*hn;
		float vv = sqrtf(2.0f) * hn * hn*v_next / (sqrtf(hn4 + max(hn4, EPSILON1)));


		gridpoint u_center;

		//u_center.x = center.x + center.w;//总高度
		u_center.x = center.x;
		//u_center.x = h_next;
		u_center.y = u_next;
		u_center.z = v_next;
		//u_center.w = center.w;
		u_center.w = h_next;//w分量来存储advection后的水深
							//u_center.w = h_next + w_next;//平流水深
		float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
			u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
			u_center.z = 0;
		}
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}
}

__global__ void s_Height_Integration(gridpoint* grid_next, gridpoint* grid, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	//限制水深，h<=2*(x/(t*g)),约为20，本实验水深不会达到20，故省去此步。

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		/*gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);*/
		gridpoint center = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east = grid2Dread(grid, gridx + 1, gridy, pitch);

		float u = center.y;
		float ur = east.y;
		float v = center.z;
		float vs = south.z;

		float hl, hr, hn, hs;
		float a = 1.0;
		float b = 1.0 - a;
		if (u <= 0)
		{
			hl = a*center.x + b*west.x;
		}
		else
		{
			hl = a*west.x + b*center.x;
		}
		if (ur <= 0)
		{
			hr = a*east.x + b*center.x;
		}
		else
		{
			hr = a*center.x + b*east.x;
		}
		if (v <= 0)
		{
			hn = a*center.x + b*north.x;
		}
		else
		{
			hn = a*north.x + b*center.x;
		}
		if (vs <= 0)
		{
			hs = a*south.x + b*center.x;
		}
		else
		{
			hs = a*center.x + b*south.x;
		}
		/*hr = (center.x + east.x) / 2;
		hl = (center.x + west.x) / 2;
		hs = (center.x + south.x) / 2;
		hn = (center.x + north.x) / 2;*/

		float h = center.x;
		hr = east.x;
		hl = west.x;
		hs = south.x;
		hn = north.x;

		b = 2 + (-ur + u - vs + v)*timestep;
		float b1 = timestep*(-ur) / 2;
		float b2 = timestep*(u) / 2;
		float b3 = timestep*(-vs) / 2;
		float b4 = timestep*(v) / 2;
		float b5 = b / 2;
		float h_next = b1*hr + b2*hl + b3*hs + b4*hn + b5*h;
		//float h_next = center.x - timestep*(/*0.5*(east.x-west.x+south.x-north.x)+*/hr*ur - hl*u + hs*vs - hn*v);

		/*float h4 = center.x*center.x*center.x*center.x;
		float uu = sqrtf(2.0f) * center.x * center.x*u / (sqrtf(h4 + max(h4, EPSILON1)));
		float vv = sqrtf(2.0f) * center.x * center.x*v / (sqrtf(h4 + max(h4, EPSILON1)));*/


		gridpoint u_center;

		u_center.x = max(h_next, 0);
		u_center.y = u;
		u_center.z = v;
		u_center.w = center.w;



		/*float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
		u_center.y = 0;
		u_center.z = 0;
		}*/
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}

__global__ void s_Height_Integration1(float* grid, int width, int height, float timestep, int pitch)//取中值
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	//限制水深，h<=2*(x/(t*g)),约为20，本实验水深不会达到20，故省去此步。

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		float u = center.y;
		float ur = east.y;
		float v = center.z;
		float vs = south.z;

		float temp = 2 + (ur - u + vs - v)*timestep;
		//if (temp == 0) temp += 0.00001;
		grid[(x*width + y)*pitch] = timestep*(-ur) / temp;
		grid[(x*width + y)*pitch + 1] = timestep*(u) / temp;
		grid[(x*width + y)*pitch + 2] = timestep*(-vs) / temp;
		grid[(x*width + y)*pitch + 3] = timestep*(v) / temp;
		grid[(x*width + y)*pitch + 4] = 2 / temp;


	}
}
//__global__ void jacob1(gridpoint* grid_next, gridpoint* grid, int width, int height, int pitch, float timestep)//
//{
//	int x = threadIdx.x + blockIdx.x * blockDim.x;
//	int y = threadIdx.y + blockIdx.y * blockDim.y;
//
//
//	if (x < width && y < height)
//	{
//		int gridx = x + 1;
//		int gridy = y + 1;
//
//		gridpoint center = tex2D(texture_grid, gridx, gridy);
//		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
//		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
//		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
//		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
//
//		gridpoint center1 = grid2Dread(grid, gridx, gridy, pitch);
//		gridpoint north1 = grid2Dread(grid, gridx, gridy - 1, pitch);
//		gridpoint west1 = grid2Dread(grid, gridx - 1, gridy, pitch);
//		gridpoint south1 = grid2Dread(grid, gridx, gridy + 1, pitch);
//		gridpoint east1 = grid2Dread(grid, gridx + 1, gridy, pitch);
//
//		float d = center.x;
//		float h_ = center1.w;//advection后的水高
//
//		float hr = east1.x;
//		float hl = west1.x;
//		float hs = south1.x;
//		float hn = north1.x;
//
//		//获取地形高度
//		float b1 = tex2D(texture_landscape, x - 1, y - 1).y;
//		float br = tex2D(texture_landscape, x, y - 1).y;
//		float bl = tex2D(texture_landscape, x - 2, y - 1).y;
//		float bs = tex2D(texture_landscape, x - 1, y).y;
//		float bn = tex2D(texture_landscape, x - 1, y - 2).y;
//
//
//
//		float u = center1.y;
//		float ur = east1.y;
//		float v = center1.z;
//		float vs = south1.z;
//
//
//
//		float temp = 1 + 4 * g*timestep*timestep*d;
//		float a1 = timestep*timestep*g*d;
//		float a2 = timestep*timestep*g*d;
//		float a3 = timestep*timestep*g*d;
//		float a4 = timestep*timestep*g*d;
//		float a5 = h_ - timestep*d*(ur - u + vs - v) + g*timestep*timestep*d*(bl + br + bn + bs - b1 * 4);
//
//		//边界速度处理
//		if (x == 0)
//		{
//			temp = temp - g*timestep*timestep*d;
//			a1 = 0;
//			a5 = a5 - timestep*d*u - g*timestep*timestep*d*(bl - b1);
//		}
//		if (x == width - 1)
//		{
//			temp = temp - g*timestep*timestep*d;
//			a2 = 0;
//			a5 = a5 + timestep*d*ur - g*timestep*timestep*d*(br - b1);
//		}
//		if (y == 0)
//		{
//			temp = temp - g*timestep*timestep*d;
//			a3 = 0;
//			a5 = a5 - timestep*d*v - g*timestep*timestep*d*(bn - b1);
//		}
//		if (y == height - 1)
//		{
//			temp = temp - g*timestep*timestep*d;
//			a4 = 0;
//			a5 = a5 + timestep*d*vs - g*timestep*timestep*d*(bs - b1);
//		}
//
//
//		float h_next = (a1*hl + a2*hr + a3*hn + a4*hs + a5) / temp;
//		//float h_next = center1.w;
//
//
//
//		gridpoint u_center;
//
//		u_center.x = h_next;
//		u_center.y = center1.y;
//		u_center.z = center1.z;
//		u_center.w = center1.w;
//
//		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
//	}
//}
__global__ void jacob1(gridpoint* grid_next, gridpoint* grid, int width, int height, int pitch, float timestep)//
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;


	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		gridpoint center1 = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north1 = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west1 = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south1 = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east1 = grid2Dread(grid, gridx + 1, gridy, pitch);

		float d = center.x;
		float dr = east.x;
		float dl = west.x;
		float ds = south.x;
		float dn = north.x;
		float h_ = center1.w;//advection后的水高

		float hr = east1.x;
		float hl = west1.x;
		float hs = south1.x;
		float hn = north1.x;

		//获取地形高度
		float b1 = tex2D(texture_landscape, x - 1, y - 1).y*0.04;
		float br = tex2D(texture_landscape, x, y - 1).y*0.04;
		float bl = tex2D(texture_landscape, x - 2, y - 1).y*0.04;
		float bs = tex2D(texture_landscape, x - 1, y).y*0.04;
		float bn = tex2D(texture_landscape, x - 1, y - 2).y*0.04;



		float u = center1.y;
		float ur = east1.y;
		float v = center1.z;
		float vs = south1.z;



		//float temp = 1 + 4 * g*timestep*timestep*d;
		//float a1 = timestep*timestep*g*d;
		//float a2 = timestep*timestep*g*d;
		//float a3 = timestep*timestep*g*d;
		//float a4 = timestep*timestep*g*d;
		//float a5 = h_ - timestep*d*(ur - u + vs - v) + g*timestep*timestep*d*(bl + br + bn + bs - b1 * 4);

		////边界速度处理
		//if (/*x == 0 ||*/ ((center.x + b1<bl) && (west.x<EPSILON1)) || ((west.x + bl<b1) && (center.x<EPSILON1)))
		//{
		//	temp = temp - g*timestep*timestep*d;
		//	a1 = 0;
		//	a5 = a5 - timestep*d*u - g*timestep*timestep*d*(bl - b1);
		//}
		//if (/*x == width - 1 ||*/ ((center.x + b1<br) && (east.x<EPSILON1)) || ((east.x + br<b1) && (center.x<EPSILON1)))
		//{
		//	temp = temp - g*timestep*timestep*d;
		//	a2 = 0;
		//	a5 = a5 + timestep*d*ur - g*timestep*timestep*d*(br - b1);
		//}
		//if (/*y == 0 ||*/ ((center.x + b1<bn) && (north.x<EPSILON1)) || ((north.x + bn<b1) && (center.x<EPSILON1)))
		//{
		//	temp = temp - g*timestep*timestep*d;
		//	a3 = 0;
		//	a5 = a5 - timestep*d*v - g*timestep*timestep*d*(bn - b1);
		//}
		//if (/*y == height - 1 ||*/ ((center.x + b1<bs) && (south.x<EPSILON1)) || ((south.x + bs<b1) && (center.x<EPSILON1)))
		//{
		//	temp = temp - g*timestep*timestep*d;
		//	a4 = 0;
		//	a5 = a5 + timestep*d*vs - g*timestep*timestep*d*(bs - b1);
		//}

		float c1 = ur > 0 ? 2 : 0;
		float c2 = u > 0 ? 0 : 2;
		float c3 = vs > 0 ? 2 : 0;
		float c4 = v > 0 ? 0 : 2;

		float temp = 1 + 0.5 * g*timestep*timestep*(d*(c1 + c2 + c3 + c4) + dr*(2 - c1) + dl*(2 - c2) + ds*(2 - c3) + dn*(2 - c4));
		float a1 = 0.5*timestep*timestep*g*(d*c2 + dl*(2 - c2));
		float a2 = 0.5*timestep*timestep*g*(d*c1 + dr*(2 - c1));
		float a3 = 0.5*timestep*timestep*g*(d*c4 + dn*(2 - c4));
		float a4 = 0.5*timestep*timestep*g*(d*c3 + ds*(2 - c3));
		//float a5 = h_ - 0.5*timestep*d*(ur - u + vs - v) + 0.5*g*timestep*timestep*d*(bl + br + bn + bs - b1 * 4);
		float a5 = h_ - 0.5*timestep*((d*c1 + dr*(2 - c1))*ur - (d*c2 + dl*(2 - c2))*u + (d*c3 + ds*(2 - c3))*vs - (d*c4 + dn*(2 - c4))*v) + 0.5*g*timestep*timestep*((d*c1 + dr*(2 - c1))*(br - b1) + (d*c2 + dl*(2 - c2))*(bl - b1) + (d*c3 + ds*(2 - c3))*(bs - b1) + (d*c4 + dn*(2 - c4))*(bn - b1));
		//边界速度处理
		if (x == 0  ((center.x + b1<bl) && (west.x<EPSILON1)) || ((west.x + bl<b1) && (center.x<EPSILON1)))
		{
			temp = temp - 0.5*g*timestep*timestep*(d*c2 + dl*(2 - c2));
			a1 = 0;
			a5 = a5 - 0.5*timestep*(d*c2 + dl*(2 - c2))*u - 0.5*g*timestep*timestep*(d*c2 + dl*(2 - c2))*(bl - b1);
		}
		if (/*x == width - 1 ||*/ ((center.x + b1<br) && (east.x<EPSILON1)) || ((east.x + br<b1) && (center.x<EPSILON1)))
		{
			temp = temp - 0.5*g*timestep*timestep*(d*c1 + dr*(2 - c1));
			a2 = 0;
			a5 = a5 + 0.5*timestep*(d*c1 + dr*(2 - c1))*ur - 0.5*g*timestep*timestep*(d*c1 + dr*(2 - c1))*(br - b1);
		}
		if (y == 0 || ((center.x + b1<bn) && (north.x<EPSILON1)) || ((north.x + bn<b1) && (center.x<EPSILON1)))
		{
			temp = temp - 0.5*g*timestep*timestep*(d*c4 + dn*(2 - c4));
			a3 = 0;
			a5 = a5 - 0.5*timestep*(d*c4 + dn*(2 - c4))*v - 0.5*g*timestep*timestep*(d*c4 + dn*(2 - c4))*(bn - b1);
		}
		if (y == height - 1 || ((center.x + b1<bs) && (south.x<EPSILON1)) || ((south.x + bs<b1) && (center.x<EPSILON1)))
		{
			temp = temp - 0.5*g*timestep*timestep*(d*c3 + ds*(2 - c3));
			a4 = 0;
			a5 = a5 + 0.5*timestep*(d*c3 + ds*(2 - c3))*vs - 0.5*g*timestep*timestep*(d*c3 + ds*(2 - c3))*(bs - b1);
		}


		float h_next = (a1*hl + a2*hr + a3*hn + a4*hs + a5) / temp;
		//float h_next = center1.w;



		gridpoint u_center;

		u_center.x = h_next;
		u_center.y = center1.y;
		u_center.z = center1.z;
		u_center.w = center1.w;

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
//__global__ void gaoduzhuan(gridpoint* grid_next, gridpoint* grid, int width, int height, int pitch, float timestep)//
//{
//	int x = threadIdx.x + blockIdx.x * blockDim.x;
//	int y = threadIdx.y + blockIdx.y * blockDim.y;
//
//
//
//	if (x < width && y < height)
//	{
//		int gridx = x + 1;
//		int gridy = y + 1;
//		float a = tex2D(texture_landscape, x - 1, y - 1).y;
//
//		gridpoint center1 = grid2Dread(grid, gridx, gridy, pitch);
//		gridpoint u_center;
//		u_center.x = center1.x;
//		u_center.y = center1.y;
//		u_center.z = center1.z;
//		u_center.w = a;//之前此分量存储了advection后水高，改回地形高度。
//
//
//		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
//	}
//}
__global__ void gaoduzhuan(gridpoint* grid_next, gridpoint* grid, int width, int height, int pitch, float timestep)//
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;



	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		float a = tex2D(texture_landscape, x - 1, y - 1).y;

		gridpoint center1 = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint u_center;
		u_center.x = center1.x;
		u_center.y = center1.y;
		u_center.z = center1.z;
		u_center.w = a*0.04;//之前此分量存储了advection后水高，改回地形高度。


		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}
__global__ void Red_Gauss_Seidel(gridpoint* grid_next, gridpoint* grid, int width, int height, int pitch, float timestep)//取中值
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	//限制水深，h<=2*(x/(t*g)),约为20，本实验水深不会达到20，故省去此步。

	if (x < width && y < height /*&& ((x + y) % 2 == 0)*/)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east = grid2Dread(grid, gridx + 1, gridy, pitch);

		float h = center.x;
		float hr = east.x;
		float hl = west.x;
		float hs = south.x;
		float hn = north.x;

		float u = center.y;
		float ur = east.y;
		float v = center.z;
		float vs = south.z;

		/*float temp = 2 + (ur - u + vs - v)*timestep;
		float a1 = timestep*(-ur) / temp;
		float a2 = timestep*(u) / temp;
		float a3 = timestep*(-vs) / temp;
		float a4 = timestep*(v) / temp;
		float a5 = 2 / temp;


		float h_next = a1*hr + a2*hl + a3*hs + a4*hn + a5*h;*/

		float bjl, bjr, bjn, bjs;
		if (u <= 0)
		{
			bjl = 1;
		}
		else
		{
			bjl = 0;
		}
		if (ur <= 0)
		{
			bjr = 0;
		}
		else
		{
			bjr = 1;
		}
		if (v <= 0)
		{
			bjn = 1;
		}
		else
		{
			bjn = 0;
		}
		if (vs <= 0)
		{
			bjs = 0;
		}
		else
		{
			bjs = 1;
		}
		float bjl1 = 1.0 - bjl;
		float bjr1 = 1.0 - bjr;
		float bjn1 = 1.0 - bjn;
		float bjs1 = 1.0 - bjs;

		float temp = 1.0 + (bjr*ur - bjl*u + bjs*vs - bjn*v)*timestep;
		float a1 = timestep*(-bjr1*ur) / temp;
		float a2 = timestep*(bjl1*u) / temp;
		float a3 = timestep*(-bjs1*vs) / temp;
		float a4 = timestep*(bjn1*v) / temp;
		float a5 = 1 / temp;


		float h_next = a1*hr + a2*hl + a3*hs + a4*hn + a5*h;



		gridpoint u_center;

		u_center.x = h_next;
		u_center.y = center.y;
		u_center.z = center.z;
		u_center.w = center.w;

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}

__global__ void Black_Gauss_Seidel(gridpoint* grid_next, gridpoint* grid, int width, int height, int pitch, float timestep)//取中值
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height && ((x + y) % 2 == 1))
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = grid2Dread(grid, gridx, gridy, pitch);
		gridpoint north = grid2Dread(grid, gridx, gridy - 1, pitch);
		gridpoint west = grid2Dread(grid, gridx - 1, gridy, pitch);
		gridpoint south = grid2Dread(grid, gridx, gridy + 1, pitch);
		gridpoint east = grid2Dread(grid_next, gridx + 1, gridy, pitch);

		float h = center.x;
		float hr = east.x;
		float hl = west.x;
		float hs = south.x;
		float hn = north.x;

		float u = center.y;
		float ur = east.y;
		float v = center.z;
		float vs = south.z;

		/*float temp = 2 + (ur - u + vs - v)*timestep;
		float a1 = timestep*(-ur) / temp;
		float a2 = timestep*(u) / temp;
		float a3 = timestep*(-vs) / temp;
		float a4 = timestep*(v) / temp;
		float a5 = 2 / temp;

		float h_next = a1*hr + a2*hl + a3*hs + a4*hn + a5*h;*/
		float bjl, bjr, bjn, bjs;
		if (u <= 0)
		{
			bjl = 1;
		}
		else
		{
			bjl = 0;
		}
		if (ur <= 0)
		{
			bjr = 0;
		}
		else
		{
			bjr = 1;
		}
		if (v <= 0)
		{
			bjn = 1;
		}
		else
		{
			bjn = 0;
		}
		if (vs <= 0)
		{
			bjs = 0;
		}
		else
		{
			bjs = 1;
		}
		float bjl1 = 1.0 - bjl;
		float bjr1 = 1.0 - bjr;
		float bjn1 = 1.0 - bjn;
		float bjs1 = 1.0 - bjs;

		float temp = 1.0 + (bjr*ur - bjl*u + bjs*vs - bjn*v)*timestep;
		float a1 = timestep*(-bjr1*ur) / temp;
		float a2 = timestep*(bjl1*u) / temp;
		float a3 = timestep*(-bjs1*vs) / temp;
		float a4 = timestep*(bjn1*v) / temp;
		float a5 = 1 / temp;


		float h_next = a1*hr + a2*hl + a3*hs + a4*hn + a5*h;

		gridpoint u_center;

		u_center.x = h_next;
		u_center.y = center.y;
		u_center.z = center.z;
		u_center.w = center.w;

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
}

//__global__ void s_Velocity_Integration(gridpoint* grid_next, int width, int height, float timestep, int pitch)
//{
//	int x = threadIdx.x + blockIdx.x * blockDim.x;
//	int y = threadIdx.y + blockIdx.y * blockDim.y;
//
//	
//
//	if (x < width && y < height)
//	{
//		int gridx = x + 1;
//		int gridy = y + 1;
//
//		gridpoint center = tex2D(texture_grid, gridx, gridy);
//
//		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
//
//		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
//
//		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
//
//		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);
//
//		float u = center.y ;
//		float ule = west.y ;
//		float ur = east.y ;
//		float v = center.z ;
//		float vn = north.z ;
//		float vs = south.z ;
//
//		float u_next, v_next;
//		if (x == 0)
//		{
//			u_next = 0;
//		}
//		else
//		{
//			u_next = u - timestep*g*(max(center.x,0) - max(0,west.x) +center.w-west.w);
//		}
//		if (y == 0)
//		{
//			v_next = 0;
//		}
//		else
//		{
//			v_next = v - timestep*g*(max(0,center.x) - max(0,north.x) +center.w-north.w);
//		}
//
//
//		if (u_next > LIM) u_next = LIM;
//		if (u_next < -LIM) u_next = -LIM;
//		if (v_next > LIM) v_next = LIM;
//		if (v_next < -LIM) v_next = -LIM;
//
//
//
//
//		gridpoint u_center;
//
//		u_center.x = max(center.x,0);
//		u_center.y = u_next;
//		u_center.z = v_next;
//		u_center.w = center.w;
//
//		float totalH = center.x + center.w;
//		if ((west.w > totalH) && (west.x<EPSILON1))
//		{
//			u_center.y = 0;
//		}
//		if ((north.w > totalH) && (north.x<EPSILON1))
//		{
//			u_center.z = 0;
//		}
//		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
//		{
//			u_center.y = 0;
//		}
//		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
//		{
//			u_center.z = 0;
//		}
//		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
//		/*if ((x == width - 1) && (center.x > 0))
//		{
//			east.y = center.y;
//			grid2Dwrite(grid_next, gridx + 1, gridy, pitch, east);
//		}*/
//		/*if ((y == height - 1) && (center.x > 0))
//		{
//			south.z = center.z;
//			grid2Dwrite(grid_next, gridx, gridy + 1, pitch, south);
//		}*/
//	}
//}
__global__ void s_Velocity_Integration(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;



	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);
		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);
		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);
		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);
		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		float u = center.y;
		float ule = west.y;
		float ur = east.y;
		float v = center.z;
		float vn = north.z;
		float vs = south.z;

		float u_next, v_next;
		if (x == 0)
		{
			u_next = 0;
		}
		else
		{
			u_next = u - timestep*g*(max(0, center.x) - max(0, west.x) + center.w - west.w);
		}
		if (y == 0 || y == height - 1)
		{
			v_next = 0;
		}
		else
		{
			v_next = v - timestep*g*(max(0, center.x) - max(0, north.x) + center.w - north.w);
		}


		if (u_next > LIM) u_next = LIM;
		if (u_next < -LIM) u_next = -LIM;
		if (v_next > LIM) v_next = LIM;
		if (v_next < -LIM) v_next = -LIM;


		float hl, hn;
		float a = 1.0;
		float b = 1.0 - a;
		if (u_next <= 0)
		{
			hl = a*center.x + b*west.x;
		}
		else
		{
			hl = a*west.x + b*center.x;
		}
		if (v_next <= 0)
		{
			hn = a*center.x + b*north.x;
		}
		else
		{
			hn = a*north.x + b*center.x;
		}





		float hl4 = hl*hl*hl*hl;
		float uu = sqrtf(2.0f) * hl * hl*u_next / (sqrtf(hl4 + max(hl4, EPSILON1)));
		float hn4 = hn*hn*hn*hn;
		float vv = sqrtf(2.0f) * hn * hn*v_next / (sqrtf(hn4 + max(hn4, EPSILON1)));


		gridpoint u_center;

		u_center.x = max(0, center.x);
		u_center.y = uu;
		u_center.z = vv;
		u_center.w = center.w;
		float totalH = center.x + center.w;
		if ((west.w > totalH) && (west.x<EPSILON1))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(west.x + west.w)))
		{
			u_center.y = 0;
		}
		if ((center.x < EPSILON1) && (center.w >(north.x + north.w)))
		{
			u_center.z = 0;
		}
		if ((north.w > totalH) && (north.x<EPSILON1))
		{
			u_center.z = 0;
		}

		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
		if (x == width - 1)
		{
			int gx = x + 2;
			int gy = y + 1;

			gridpoint c1 = tex2D(texture_grid, gx, gy);
			c1.y = u_center.y;
			grid2Dwrite(grid_next, gx, gy, pitch, c1);
		}
		/*if (y == height - 1)
		{
		int gx = x + 1;
		int gy = y + 2;

		gridpoint c2 = tex2D(texture_grid, gx, gy);
		c2.z = u_center.z;
		grid2Dwrite(grid_next, gx, gy, pitch, c2);
		}*/
	}
}


__global__ void initGrid(gridpoint *grid, int gridwidth, int gridheight, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x < gridwidth && y < gridheight)
	{
		float a = tex2D(texture_landscape, x - 1, y - 1).y;

		gridpoint gp;
		//gp.x = max(1.0f - a*0.04, 0.0f);
		gp.x = 0.0f;
		gp.y = 0.0f;
		gp.z = 0.0f;
		gp.w = a*0.04;
		/*if (x > 100 && x < 200 && y>300 && y < 400)
		{
		gp.w = gp.w + 2;
		}*/
		if (x > 200 && x < 220 && y>200 && y < 220)
		{
			//gp.x = (50 - (abs(x - 225) + abs(y - 225)))*0.5 + 1;
			gp.x = 0;
		}
		/*if ((x == 0) && (y <= 50) || (x <= 50) && (y == 0))
		{
		gp.x = 5;
		}*/
		/*if (x == 0 || y == 0 || x == gridwidth - 1 || y == gridwidth - 1)
		{
		gp.w = -1;
		}*/
		/*if (x == 1)
		{

		gp.x = 0.0;
		gp.y = 0;
		gp.z = 0;
		}*/
		/*if ((x == gridwidth - 1))
		{

		gp.x = 0;
		gp.y = 0;
		gp.z = 0;
		}*/
		/*if (y == 1)
		{

		gp.x = 1.0;
		gp.y = 0;
		gp.z = 0;
		}*/
		/*	if (y == gridheight - 1)
		{

		gp.x = 0;
		gp.y = 0;
		gp.z = 0;
		}*/
		grid2Dwrite(grid, x, y, pitch, gp);
	}
}

__global__ void initjuzhen(float *grid, int gridwidth, int gridheight, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x < gridwidth && y < gridheight)
	{
		grid[(y)*pitch + x] = 0;
	}
}

__host__ __device__ vertex gridpointToVertex(gridpoint gp, float x, float y)
{
	float h = gp.x;
	vertex v;
	v.x = x * 4.0f - 2.0f;
	v.z = y * 4.0f - 2.0f;
	v.y = h*25.0 + gp.w*25.0;
	return v;
}

__host__ __device__ rgb gridpointToColor(gridpoint gp)	// 这里调整颜色的计算
{
	rgb c;
	//c.x = min(20 + (gp.x  ) * 200.0f, 255);
	//c.y = min(40 + (gp.x ) * 200.0f, 255);
	//c.z = min(100 + (gp.x ) * 200.0f, 255);
	//c.w = 255 - max(-50 * gp.x + 50, 0);
	c.x = min(130 + (gp.x) *20.0f, 255);
	c.y = min(80 + (gp.x) * 20.0f, 255);
	c.z = min(40 + (gp.x) * 20.0f, 255);
	c.w = 255 - max(-25.0 * gp.x + 40, 0);
	if (gp.x <= 0.00001) c.w = 0;	//这里设置让水面高度为0的位置透明
	return c;
}

__global__ void visualise(vertex* watersurfacevertices,
	rgb* watersurfacecolors, int width, int height)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint gp = tex2D(texture_grid, gridx, gridy);

		watersurfacevertices[y * width + x] = gridpointToVertex(gp, x / float(width - 1), y / float(height - 1));
		watersurfacecolors[y * width + x] = gridpointToColor(gp);
	}
}


__global__ void addWave(gridpoint* grid, float* wave, float norm, int width, int height, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		float waveheight = grid2Dread(grid, gridx, gridy, pitch).x;

		float toadd = max(grid2Dread(wave, x, y, width)*norm*0.0/*- 0.2f*/, 1.0 - grid2Dread(grid, gridx, gridy, pitch).w);// *norm;	// 有改动
																														   //waveheight += toadd;
		grid[gridx + gridy * pitch].x = waveheight;

	}
}
__global__ void addland(gridpoint* grid1, vertex* grid2, vertex* grid3, int width, int height, int pitch2, int pitch1)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x < width && y < height)//&&(x==50||y==50||x==200||y==200)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		gridpoint center1 = tex2D(texture_grid, gridx, gridy);
		vertex center2 = tex2D(texture_landscape, x, y);
		vertex center3 = grid2Dread(grid3, x, y, width);
		int temp = 0.01*center3.y - center1.w;
		/*center1.w = 0.01*center3.y;
		center2.y = 0.01*center3.y;
		if (temp >= center1.x)
		{
		center1.x = 0;
		}
		else if (temp > 0)
		{
		center1.x -= temp;
		}*/
		if (temp > 0)
		{
			if (center1.y < 1000)
			{
				center1.y += 1000;
			}

		}
		grid2Dwrite(grid1, gridx, gridy, pitch1, center1);
		grid2Dwrite(grid2, x, y, pitch2, center2);


	}
}
__global__ void addWave1(gridpoint* grid, int width, int height, int pitch, float langg)	// 这里设置人工水源的位置和大小
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		//if (x > (0.00*width) && x < (1.00*width) && (y)<(0.10*height))
		//{
		//	grid[x + y * pitch].x = 4.0;
		//	//grid[x + y * pitch].y = 1.0;
		//	grid[x + y * pitch].z = 1.0;
		//}
		//
		//if (y > (0.00*height) && y < (1.00*height) && (x)<(0.15*width))
		//{
		//	grid[x + y * pitch].x = 4.0;
		//	grid[x + y * pitch].y = 1.0;
		//	//grid[x + y * pitch].z = 1.0;
		//}
		if (x <= 0/*x < (0.30*width)*/ /*|| (y)<(0.10*height)*/)
		{
			grid[x + y * pitch].x = 5;
			grid[x + y * pitch].y = langg;
			/*grid[x + 1 + y * pitch].y = 1;
			grid[x + 1 + y * pitch].z = 1;
			grid[x + (y + 1) * pitch].y = 1;
			grid[x + (y + 1) * pitch].z = 1;*/

		}


	}
}

__global__ void computetrans(double x_mid, double y_mid, double z_mid, vertex* model_data1, int begin, int end, double hudu, vertex* model_data2, double *tran, int i)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;

	if (x >= begin&&x < end)
	{
		double a[3], b[3], c[3];
		vertex center, centery;
		//tozhijiao(model_data1[x].x, model_data1[x].y, model_data1[x].z, a);
		b[0] = model_data2[x].x - x_mid;
		b[1] = model_data2[x].y - y_mid;
		b[2] = model_data2[x].z - z_mid;
		c[0] = b[0] + x_mid;
		c[1] = b[1] * cos(hudu) + b[2] * sin(hudu) + y_mid;
		c[2] = -b[1] * sin(hudu) + b[2] * cos(hudu) + z_mid;
		centery.x = c[0];
		centery.y = c[1];
		centery.z = c[2];
		model_data2[x] = centery;
		/*c[0] = b[0] * cos(hudu) - b[2] * sin(hudu) + x_mid;
		c[1] = b[1] + y_mid;
		c[2] = b[0] * sin(hudu) + b[2] * cos(hudu) + z_mid;*/
		/*c[0] = b[0] * cos(hudu) - b[1] * sin(hudu) + x_mid;
		c[1] = b[0] * sin(hudu) + b[1] * cos(hudu) + y_mid;
		c[2] = b[2] + z_mid;*/
		double b1, b2, b3;
		b1 = c[0] * tran[i * 16] + c[1] * tran[i * 16 + 4] + c[2] * tran[i * 16 + 8] + 1 * tran[i * 16 + 12];
		b2 = c[0] * tran[i * 16 + 1] + c[1] * tran[i * 16 + 5] + c[2] * tran[i * 16 + 9] + 1 * tran[i * 16 + 13];
		b3 = c[0] * tran[i * 16 + 2] + c[1] * tran[i * 16 + 6] + c[2] * tran[i * 16 + 10] + 1 * tran[i * 16 + 14];
		//tojingwei(c[0], c[1], c[2], b1, b2, b3);
		//center.x = b1;
		//center.y = b2;
		//center.z = b3;
		double c1, c2, c3;
		tojingwei(b1, b2, b3, c1, c2, c3);
		center.x = c1;
		center.y = c2;
		center.z = c3;
		model_data1[x] = center;
	}
}

void addWave(float* wave, float norm, int width, int height, int pitch_elements)
{
	cudaError_t error;
	size_t sizeInBytes = width * height * sizeof(float);

	error = cudaMemcpy(device_waves, wave, sizeInBytes, cudaMemcpyHostToDevice);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	int x = (width + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int y = (height + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGrid(x, y);

	addWave << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_waves, norm, width, height, pitch_elements);

	error = cudaGetLastError();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	error = cudaThreadSynchronize();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	gridpoint *grid_helper = device_grid;
	device_grid = device_grid_next;
	device_grid_next = grid_helper;

	error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, width + 2, height + 2, grid_pitch_elements * sizeof(gridpoint));
	//	CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);
}

void initWaterSurface(int width, int height, vertex *heightmapvertices, float *wave, int model_num, std::vector<std::vector<vertex>> model_v, std::vector<std::vector<int>> faceid, std::vector<std::vector<double>> baowei, std::vector<std::vector<int>> left_up2, std::vector<std::vector<int>> right_down2, std::vector<std::vector<double>> model_h2, std::vector<std::vector<vertex>> model_vy, std::vector<std::vector<std::vector<double>>> trans)
{

	cudaError_t error;
	model_nums = model_num;
	zhuan = 0;

	transt = new double[model_nums * 16];
	for (int i = 0; i < model_nums; i++)
	{
		for (int m = 0; m < 4; m++)
		{
			for (int n = 0; n < 4; n++)
			{
				transt[i * 16 + m * 4 + n] = trans[i][m][n];
			}
		}
	}
	model_index = new int[model_v.size()];
	face_in = new int[faceid.size()];
	for (int i = 0; i < model_v.size(); i++)
	{
		model_index[i] = model_nsize;
		model_nsize += model_v[i].size();
	}
	for (int i = 0; i < faceid.size(); i++)
	{
		face_in[i] = face_nsize;
		face_nsize += faceid[i].size() / 3;
	}
	model_data = new vertex[model_nsize];
	model_datay = new vertex[model_nsize];
	face_index = new vertex[face_nsize];
	for (int i = 0; i < model_v.size(); i++)
	{
		for (int j = 0; j < model_v[i].size(); j++)
		{
			model_data[model_index[i] + j] = model_v[i][j];
			model_datay[model_index[i] + j] = model_vy[i][j];
		}
	}
	for (int i = 0; i < faceid.size(); i++)
	{
		for (int j = 0; j < faceid[i].size(); j = j + 3)
		{
			face_index[face_in[i] + j / 3].x = faceid[i][j];
			face_index[face_in[i] + j / 3].y = faceid[i][j + 1];
			face_index[face_in[i] + j / 3].z = faceid[i][j + 2];
		}
	}
	baow = new double[model_nums * 6];
	for (int i = 0; i < baowei.size(); i++)
	{
		for (int j = 0; j < baowei[i].size(); j++)
		{
			baow[i * 6 + j] = baowei[i][j];

		}
	}
	fxiangl = new vertex[face_nsize];
	localindexm = new vertex[face_nsize];
	localindex = new vertex[model_nums];
	for (int i = 0; i < model_nums; i++)
	{
		localindex[i].x = left_up2[i][0];
		localindex[i].y = left_up2[i][1];
		localindex[i].z = right_down2[i][0];
		localindex[i].w = right_down2[i][1];
	}
	for (int i = 0; i < face_nsize; i++)
	{
		localindexm[i].x = 0;
		localindexm[i].y = 0;
		localindexm[i].z = 0;
		localindexm[i].w = 0;
	}
	/*model_land_index = new int[model_nums];
	int model_land_nsize = 0;
	for (int i = 0; i < model_nums; i++)
	{
	model_land_index[i] = model_land_nsize;
	model_land_nsize += model_h2[i].size()*model_h2[i][0].size();
	}*/
	model_land = new double[width*height];
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			model_land[i*width + j] = model_h2[i][j];
		}
	}
	size_t model_pitch;
	error = cudaMallocPitch(&model_land_g, &model_pitch, width * sizeof(double), height);
	error = cudaMemcpy2D(model_land_g, model_pitch, model_land, width * sizeof(double), width * sizeof(double), height, cudaMemcpyHostToDevice);

	model_pitch_elements = model_pitch / sizeof(double);
	if (state != UNINTIALISED)
	{
		return;
	}
	int gridwidth = width + 2;
	int gridheight = height + 2;

	size_t sizeInBytes;
	size_t grid_pitch;


	grid_channeldesc = cudaCreateChannelDesc<float4>();
	cudaChannelFormatDesc treshholds_channeldesc = cudaCreateChannelDesc<float>();
	cudaChannelFormatDesc reflections_channeldesc = cudaCreateChannelDesc<int>();

	//alloc pitched memory for device_grid
	error = cudaMallocPitch(&device_grid, &grid_pitch, gridwidth * sizeof(gridpoint), gridheight);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//	CHECK_NOTNULL(device_grid);

	size_t oldpitch = grid_pitch;

	//alloc pitched memoty for device_grid_next
	error = cudaMallocPitch(&device_grid_next, &grid_pitch, gridwidth * sizeof(gridpoint), gridheight);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//	CHECK_NOTNULL(device_grid_next);

	//	CHECK_EQ(oldpitch, grid_pitch);

	grid_pitch_elements = grid_pitch / sizeof(gridpoint);

	//alloc pitched memory for landscape data on device
	size_t heightmap_pitch;
	error = cudaMallocPitch(&device_heightmap, &heightmap_pitch, width * sizeof(vertex), height);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//	CHECK_NOTNULL(device_heightmap);
	error = cudaMalloc(&transt_g, model_nums * 16 * sizeof(double));
	error = cudaMemcpy(transt_g, transt, model_nums * 16 * sizeof(double), cudaMemcpyHostToDevice);
	//error = cudaMalloc(&model_data_g, model_nsize * sizeof(vertex));
	//error = cudaMemcpy(model_data_g, model_data, model_nsize * sizeof(vertex), cudaMemcpyHostToDevice);
	//error = cudaMalloc(&model_datay_g, model_nsize * sizeof(vertex));
	//error = cudaMemcpy(model_datay_g, model_datay, model_nsize * sizeof(vertex), cudaMemcpyHostToDevice);
	error = cudaMalloc(&face_index_g, face_nsize * sizeof(vertex));
	error = cudaMemcpy(face_index_g, face_index, face_nsize * sizeof(vertex), cudaMemcpyHostToDevice);
	error = cudaMalloc(&baow_g, model_nums * 6 * sizeof(int));
	error = cudaMemcpy(baow_g, baow, model_nums * 6 * sizeof(int), cudaMemcpyHostToDevice);
	error = cudaMalloc(&localindex_g, model_nums * sizeof(vertex));
	error = cudaMemcpy(localindex_g, localindex, model_nums * sizeof(vertex), cudaMemcpyHostToDevice);
	error = cudaMalloc(&localindexm_g, face_nsize * sizeof(vertex));
	error = cudaMemcpy(localindexm_g, localindexm, face_nsize * sizeof(vertex), cudaMemcpyHostToDevice);
	error = cudaMalloc(&fxiangl_g, face_nsize * sizeof(vertex));
	error = cudaMemcpy(fxiangl_g, fxiangl, face_nsize * sizeof(vertex), cudaMemcpyHostToDevice);
	//error = cudaMalloc(&model_land_index_g, model_nums * sizeof(int));
	//error = cudaMemcpy(model_land_index_g, model_land_index, model_nums * sizeof(int), cudaMemcpyHostToDevice);


	// copy landscape data to device

	error = cudaMemcpy2D(device_heightmap, heightmap_pitch, heightmapvertices, width * sizeof(vertex), width * sizeof(vertex), height, cudaMemcpyHostToDevice);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	height_pitch_elements = heightmap_pitch / sizeof(vertex);
	// bind heightmap to texture_landscape
	cudaChannelFormatDesc heightmap_channeldesc = cudaCreateChannelDesc<float4>();
	error = cudaBindTexture2D(0, &texture_landscape, device_heightmap, &heightmap_channeldesc, width, height, heightmap_pitch);
	//	CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);

	// malloc memory for watersurface vertices
	sizeInBytes = width * height * sizeof(vertex);
	error = cudaMalloc(&device_watersurfacevertices, sizeInBytes);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// malloc memory for watersurface colors
	sizeInBytes = height * width * sizeof(rgb);
	error = cudaMalloc(&device_watersurfacecolors, sizeInBytes);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// malloc memory for waves
	sizeInBytes = height * width * sizeof(float);
	error = cudaMalloc(&device_waves, sizeInBytes);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// make dimension
	int x = (gridwidth + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int y = (gridheight + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGrid(x, y);

	int x1 = (gridwidth + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	dim3 threadsPerBlock1(BLOCKSIZE_X, 1);
	dim3 blocksPerGrid1(x1, 1);

	int y1 = (gridheight + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock2(1, BLOCKSIZE_Y);
	dim3 blocksPerGrid2(1, y1);

	int xj = (5 + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int yj = (width*height + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlockj(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGridj(xj, yj);
	size_t jgrid_pitch;
	error = cudaMallocPitch(&juzhen, &jgrid_pitch, 5 * sizeof(float), width*height);

	grid_ju_elements = jgrid_pitch / sizeof(float);
	initjuzhen << < blocksPerGridj, threadsPerBlockj >> > (juzhen, 5, width*height, grid_ju_elements);

	//init grid with initial values
	initGrid << < blocksPerGrid, threadsPerBlock >> > (device_grid, gridwidth, gridheight, grid_pitch_elements);

	//init grid_next with initial values
	initGrid << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, gridwidth, gridheight, grid_pitch_elements);

	error = cudaThreadSynchronize();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);


	//bind the grid to texture_grid
	error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch);
	//	CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);

	//add the initial wave to the grid
	addWave(wave, 5.0f, width, height, grid_pitch_elements);

	state = INITIALISED;
}

void computeNext(int zhuanb, int width, int height, vertex* watersurfacevertices, rgb* watersurfacecolors, vertex* modelh, double* model_land1, int steps, int xr)
{
	if (state != INITIALISED)
	{
		return;
	}

	int gridwidth = width + 2;
	int gridheight = height + 2;

	cudaError_t error;

	// make dimension
	int x = (width + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int y = (height + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGrid(x, y);

	if (xr == 2)
	{
		for (int x = 0; x < steps; x++)
		{

			simulateWaveStep << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			error = cudaThreadSynchronize();
			gridpoint *grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;
			double tbj = clock();
			float langg = 20 * sin(tbj*0.001);
			addWave1 << < blocksPerGrid, threadsPerBlock >> > (device_grid, width, height, grid_pitch_elements, langg);
			error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
			//		
		}

	}
	else
	{
		for (int x = 0; x < steps; x++)
		{
			s_advection << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);

			//simulateWaveStep << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			error = cudaThreadSynchronize();
			gridpoint *grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;
			//s_Velocity_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			for (int dn = 0; dn < maxdiedai; dn++)
			{
				//jacob1 << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);
				jacob1 << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);

				error = cudaThreadSynchronize();
				grid_helper = device_grid;
				device_grid = device_grid_next;
				device_grid_next = grid_helper;
			}
			gaoduzhuan << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);
			error = cudaThreadSynchronize();
			grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;

			s_Velocity_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			error = cudaThreadSynchronize();
			grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;

			addWave1 << < blocksPerGrid, threadsPerBlock >> > (device_grid, width, height, grid_pitch_elements, 5);
			error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));

		}
	}
	//for (int i = 0; i < model_nums; i++)
	//{
	//	if (zhuan > 0.95&&zhuan<=1.05)
	//	{
	//		//jianmodel << < blocksPerGrid, threadsPerBlock >> > ((int)localindex[i].x, (int)localindex[i].y, (int)localindex[i].z, (int)localindex[i].w, height_pitch_elements, model_pitch_elements, grid_pitch_elements, device_heightmap, model_land_g, device_grid);
	//		//error = cudaBindTexture2D(0, &texture_landscape, device_heightmap, &heightmap_channeldesc, width, height, height_pitch_elements * sizeof(vertex));
	//	}
	//	if (zhuan<=2.65&&zhuan>=0.95)
	//	{
	//		//zhuan = zhuan + 0.1;
	//		//jianmodel << < blocksPerGrid, threadsPerBlock >> > ((int)localindex[i].x, (int)localindex[i].y, (int)localindex[i].z, (int)localindex[i].w, height_pitch_elements, model_pitch_elements, grid_pitch_elements, device_heightmap, model_land_g, device_grid);
	//		//error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
	//		//error = cudaBindTexture2D(0, &texture_landscape, device_heightmap, &heightmap_channeldesc, width, height, height_pitch_elements * sizeof(vertex));
	//		int x_m = (model_nsize + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	//		dim3 threadsPerBlock_m = 16;
	//		dim3 blocksPerGrid_m = x_m;
	//		int end1;
	//		if (i + 1 < model_nums) end1 = model_index[i + 1];
	//		else end1 = model_nsize;
	//		computetrans << < blocksPerGrid_m, threadsPerBlock_m >> > (baow[0], baow[1], baow[2], model_data_g, model_index[i], end1, 0.1, model_datay_g, transt_g, i);
	//		
	//		/*x_m = (model_nums + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	//		threadsPerBlock_m = 16;
	//		blocksPerGrid_m = x_m;
	//		gailocalindex << < blocksPerGrid_m, threadsPerBlock_m >> > (i, localindex_g);*/
	//		x_m = (face_nsize + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	//		threadsPerBlock_m = 16;
	//		blocksPerGrid_m = x_m;
	//		int end2;
	//		if (i + 1 < model_nums) end2 = face_in[i + 1];
	//		else end2 = face_nsize;
	//		computemodelh << < blocksPerGrid_m, threadsPerBlock_m >> > (width,i, face_in[i], end2, face_index_g, model_data_g, localindex_g, localindexm_g, fxiangl_g);
	//		error = cudaGetLastError();
	//		error = cudaThreadSynchronize();
	//		//error = cudaMemcpy(localindex, localindex_g, model_nums * sizeof(vertex), cudaMemcpyDeviceToHost);
	//		error = cudaMemcpy(localindexm, localindexm_g, face_nsize * sizeof(vertex), cudaMemcpyDeviceToHost);
	//		localindex[i].x = width-1;
	//		localindex[i].y = height-1;
	//		localindex[i].z = 0;
	//		localindex[i].w = 0;
	//		for (int mm = face_in[i]; mm < end2; mm++)
	//		{/*
	//			int b1 = localindexm[mm].x;
	//			int b2 = localindexm[mm].y;
	//			int e1 = localindexm[mm].z;
	//			int e2 = localindexm[mm].w;*/
	//			localindex[i].x = min(localindexm[mm].x, localindex[i].x);
	//			localindex[i].y = min(localindexm[mm].y, localindex[i].y);
	//			localindex[i].z = max(localindexm[mm].z, localindex[i].z);
	//			localindex[i].w = max(localindexm[mm].w, localindex[i].w);
	//			/*int index1 = face_index[mm].x;
	//			int index2 = face_index[mm].y;
	//			int index3 = face_index[mm].z;*/
	//			compute << < blocksPerGrid, threadsPerBlock >> > (mm, localindexm[mm].x, localindexm[mm].y, localindexm[mm].z, localindexm[mm].w, face_index[mm].x, face_index[mm].y, face_index[mm].z, model_data_g, fxiangl_g, model_land_g, model_pitch_elements);
	//			//error = cudaThreadSynchronize();
	//		}
	//		error = cudaThreadSynchronize();
	//		//jianmodel << < blocksPerGrid, threadsPerBlock >> > ((int)localindex[i].x, (int)localindex[i].y, (int)localindex[i].z, (int)localindex[i].w, height_pitch_elements, model_pitch_elements, grid_pitch_elements, device_heightmap, model_land_g, device_grid);

	//		jiamodel << < blocksPerGrid, threadsPerBlock >> > ((int)localindex[i].x, (int)localindex[i].y, (int)localindex[i].z, (int)localindex[i].w, height_pitch_elements, model_pitch_elements, grid_pitch_elements, device_heightmap, model_land_g, device_grid);
	//		
	//	}
	//	//else if(zhuan > 5.65&&zhuan<=5.75)
	//	//{
	//	//	//zhuan = zhuan + 0.1;
	//	//	bianmodel << < blocksPerGrid, threadsPerBlock >> > (1, 1,255, 255, height_pitch_elements, model_pitch_elements, grid_pitch_elements, device_heightmap, model_land_g, device_grid);
	//	//}
	//	
	//}
	//zhuan = zhuan + 0.1;
	//error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
	//error = cudaBindTexture2D(0, &texture_landscape, device_heightmap, &heightmap_channeldesc, width, height, height_pitch_elements * sizeof(vertex));


	/*addland << < blocksPerGrid, threadsPerBlock >> > (device_grid, device_heightmap, device_heightmap2, width, height, height_pitch_elements, grid_pitch_elements);
	error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
	error = cudaBindTexture2D(0, &texture_landscape, device_heightmap, &heightmap_channeldesc, width, height, height_pitch_elements * sizeof(vertex));*/

	//gitter "stepsperframe" zeitschritt
	//	for (int x = 0; x < steps; x++)
	//	{
	//		//s_advection << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
	//		simulateWaveStep << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
	//		//lvbo << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, grid_pitch_elements);
	//		//s_advection << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
	//		/*s_advection1 << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
	//		error = cudaThreadSynchronize();
	//		gridpoint *grid_helper = device_grid;
	//		device_grid = device_grid_next;
	//		device_grid_next = grid_helper;
	//		s_advection2 << < blocksPerGrid, threadsPerBlock >> > (device_grid, device_grid_next, width, height, timestep, grid_pitch_elements);
	//	*/	error = cudaThreadSynchronize();
	//		gridpoint *grid_helper = device_grid;
	//		device_grid = device_grid_next;
	//		device_grid_next = grid_helper;
	//		//error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
	//		/*s_advection1 << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
	//		error = cudaThreadSynchronize();
	//		grid_helper = device_grid;
	//		device_grid = device_grid_next;
	//		device_grid_next = grid_helper;
	//		error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));*/
	//
	//		
	//		
	//		
	//		
	//		//		CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//		//gridpoint *grid_next = device_grid;
	//		//s_Velocity_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
	//		//s_Height_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, timestep, grid_pitch_elements);
	//		//Red_Gauss_Seidel << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);
	//		//s_Height_Integration1 << < blocksPerGrid, threadsPerBlock >> > (juzhen, width, height, timestep, grid_ju_elements);
	//		//error = cudaThreadSynchronize();
	//		//for (int dn = 0; dn < maxdiedai; dn++)
	//		//{
	//		//	//jacob1 << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);
	//		//	jacob1 << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);
	//
	//		//	//Red_Gauss_Seidel << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid,  width, height,  grid_pitch_elements, timestep);
	//		//	error = cudaThreadSynchronize();
	//		//	grid_helper = device_grid;
	//		//	device_grid = device_grid_next;
	//		//	device_grid_next = grid_helper;
	//		//	/*Black_Gauss_Seidel << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height,  grid_pitch_elements, timestep);
	//		//	error = cudaThreadSynchronize();
	//		//	grid_helper = device_grid;
	//		//	device_grid = device_grid_next;
	//		//	device_grid_next = grid_helper;*/
	//		//}
	//		/*gaoduzhuan << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);
	//		error = cudaThreadSynchronize();
	//		grid_helper = device_grid;
	//		device_grid = device_grid_next;
	//		device_grid_next = grid_helper;*/
	//		//gaoduzhuan << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_grid, width, height, grid_pitch_elements, timestep);
	//		/*error = cudaThreadSynchronize();
	//		grid_helper = device_grid;
	//		device_grid = device_grid_next;
	//		device_grid_next = grid_helper;
	//		error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
	//		*///		CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//       // s_Velocity_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
	//
	//		//s_Velocity_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
	//		//s_Height_Integration << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
	//		/*error = cudaThreadSynchronize();
	//		grid_helper = device_grid;
	//		device_grid = device_grid_next;
	//		device_grid_next = grid_helper;*/
	//		//device_grid = grid_next;
	//
	//		addWave1 << < blocksPerGrid, threadsPerBlock >> > (device_grid, width, height, grid_pitch_elements);
	//		error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
	////		CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);
	//	}
	visualise << < blocksPerGrid, threadsPerBlock >> > (device_watersurfacevertices, device_watersurfacecolors, width, height);

	error = cudaGetLastError();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	error = cudaThreadSynchronize();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// copy back data
	error = cudaMemcpy(watersurfacevertices, device_watersurfacevertices, width * height * sizeof(vertex), cudaMemcpyDeviceToHost);
	//error = cudaMemcpy(modelh, model_data_g, model_nsize * sizeof(vertex), cudaMemcpyDeviceToHost);
	error = cudaMemcpy(model_land1, model_land_g, width*height * sizeof(double), cudaMemcpyDeviceToHost);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	error = cudaMemcpy(watersurfacecolors, device_watersurfacecolors, width * height * sizeof(rgb), cudaMemcpyDeviceToHost);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);



}

void destroyWaterSurface()
{
	if (state != INITIALISED)
	{
		return;
	}
	cudaUnbindTexture(texture_grid);
	cudaUnbindTexture(texture_landscape);
	cudaFree(device_grid);
	cudaFree(device_grid_next);

	cudaFree(device_heightmap);
	cudaFree(device_watersurfacevertices);

	cudaFree(model_data);
	cudaFree(device_waves);
	cudaFree(device_watersurfacecolors);
	cudaFree(model_data_g);
	cudaFree(face_index_g);
	cudaFree(baow_g);
	cudaFree(localindex_g);
	cudaFree(model_land_g);
	cudaFree(model_land_index_g);
	cudaFree(localindexm_g);
	cudaFree(fxiangl_g);
	cudaFree(model_index);//model_data的索引
	cudaFree(face_in);//face_index的索引

	cudaFree(model_datay);
	cudaFree(face_index);//每个模型的面片索引
	cudaFree(baow);//每个模型包围盒
	cudaFree(localindex);//每个模型产生的高度场的起始结束坐标
	cudaFree(model_land);//每个模型产生的高度场
						 //int* model_land_index;//model_land的索引

	cudaFree(localindexm);
	cudaFree(fxiangl);
	cudaFree(transt);


	cudaFree(model_datay_g);







	double *transt_g;
	state = UNINTIALISED;
}
