#pragma once
#include "types.h"

class sim
{
public:
	sim();
	sim(const int size, const int step, vertex *land, float *waveh, rgb *color, vertex *waves);
	~sim();
	void init();

	void compute_exp(vertex* watersurfacevertices, rgb* watersurfacecolors, bool renderOnly);//显式

	//void compute(vertex* watersurfacevertices, rgb* watersurfacecolors,bool renderOnly);//迎风

	//void compute1(vertex* watersurfacevertices, rgb* watersurfacecolors, bool renderOnly);//中心差分

	//void compute2(vertex* watersurfacevertices, rgb* watersurfacecolors, bool renderOnly);//迎风预测

	//void compute3(vertex* watersurfacevertices, rgb* watersurfacecolors, bool renderOnly);//中心差分预测

	void destroy();
	int gridsize;
	int steps;
	vertex *landscape;
	rgb *colors;
	vertex *wave;
	float *waveheights;
	
};