#include "simu.h"

#include <iostream>
#include "types.h"
#include <stdlib.h>
#include <stdio.h>
#include"timing.h"
#include<vector>
#include <time.h>

#define BLOCKSIZE_X 16
#define BLOCKSIZE_Y 16

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef mam
#define mam(a,b) 0
#endif


#define grid2Dwrite(array, x, y, pitch, value) array[(y)*pitch+x] = value
#define grid2Dread(array, x, y, pitch) array[(y)*pitch+x]


#define LIM  20.0f
#define EPSILON1 0.005f
const int UNINTIALISED = 0;
const int INITIALISED = 1;

int state = UNINTIALISED;


int stepsperframe = 50;

__constant__ float GRAVITY = 9.83219f * 0.5f; //0.5f * Fallbeschleunigung
__constant__ float g = 9.83219f;

__constant__ float NN = 4.5f;//水流基本高度，不会比这个值小，土地高度为3~7，一般取5

const float timestep = 0.01f;
const int maxdiedai = 40;

__constant__ float cha = 0.5;
__constant__ float h_d = 0.1;

texture<gridpoint, 2, cudaReadModeElementType> texture_grid;
texture<vertex, 2, cudaReadModeElementType> texture_landscape;

int grid_pitch_elements;
int height_pitch_elements;
int h_pitch_elements;
cudaChannelFormatDesc grid_channeldesc;
cudaChannelFormatDesc heightmap_channeldesc;

gridpoint* device_grid;
gridpoint* device_grid_next;
gridpoint* particles;

vertex* device_heightmap;
vertex* device_watersurfacevertices;


float* device_waves;
rgb* device_watersurfacecolors;
float* h_bef;


#define EPSILON 0.005f  //粘稠度

__constant__ float deltax = 1;
__constant__ float deltay = 1;
__constant__ float deltat = 0.01;
__constant__ float manning = 0.020;






__host__ __device__ gridpoint operator +(const gridpoint& x, const gridpoint& y)
{
	gridpoint z;
	z.x = x.x + y.x;
	z.y = x.y + y.y;
	z.z = x.z + y.z;
	z.w = x.w + y.w;
	return z;
}
__host__ __device__ gridpoint operator -(const gridpoint& x, const gridpoint& y)
{
	gridpoint z;
	z.x = x.x - y.x;
	z.y = x.y - y.y;
	z.z = x.z - y.z;
	z.w = x.w - y.w;
	return z;
}
__host__ __device__ gridpoint operator *(const gridpoint& x, const gridpoint& y)
{
	gridpoint z;
	z.x = y.x * x.x;
	z.y = y.y * x.y;
	z.z = y.z * x.z;
	z.w = y.w * x.w;
	return z;
}
__host__ __device__ gridpoint operator *(const gridpoint& x, const float& c)
{
	gridpoint z;
	z.x = c * x.x;
	z.y = c * x.y;
	z.z = c * x.z;
	z.w = c * x.w;
	return z;
}
__host__ __device__ gridpoint operator /(const gridpoint& x, const float& c)
{
	gridpoint z;
	z.x = x.x / c;
	z.y = x.y / c;
	z.z = x.z / c;
	z.w = x.w / c;
	return z;
}
__host__ __device__ gridpoint operator *(const float& c, const gridpoint& x)
{
	return x * c;
}



__host__ __device__ gridpoint jiex(gridpoint gpl, gridpoint gpr)
{
	float ul = gpl.x > EPSILON ? gpl.y / gpl.x : 0;
	float ur = gpr.x > EPSILON ? gpr.y / gpr.x : 0;
	float a_plus;
	float a_minus;
	a_plus = max(max(ul + sqrtf(g*gpl.x), ur + sqrtf(g*gpr.x)), 0);
	a_minus = min(min(ul - sqrtf(g*gpl.x), ur - sqrtf(g*gpr.x)), 0);

	gridpoint re;
	re.x = (a_plus*gpl.y - a_minus*gpr.y) / (a_plus - a_minus) + a_plus*a_minus*(gpr.x - gpl.x) / (a_plus - a_minus);
	re.y = 0.5*(a_plus*gpl.y*ul - a_minus*gpr.y*ur) / (a_plus - a_minus) + a_plus*a_minus*(gpr.y - gpl.y) / (a_plus - a_minus);
	re.z = (a_plus*gpl.z*ul - a_minus*gpr.z*ur) / (a_plus - a_minus) + a_plus*a_minus*(gpr.z - gpl.z) / (a_plus - a_minus);
	re.w = 0;
	if (ul == 0 && ur == 0)
	{
		re.x = 0;
		re.y = 0;
		re.z = 0;
	}
	return re;
}
__host__ __device__ gridpoint jiey(gridpoint gpl, gridpoint gpr)
{
	float vl = gpl.x > EPSILON ? gpl.z / gpl.x : 0;
	float vr = gpr.x > EPSILON ? gpr.z / gpr.x : 0;
	float a_plus;
	float a_minus;
	a_plus = max(max(vl + sqrtf(g*gpl.x), vr + sqrtf(g*gpr.x)), 0);
	a_minus = min(min(vl - sqrtf(g*gpl.x), vr - sqrtf(g*gpr.x)), 0);

	gridpoint re;
	re.x = (a_plus*gpl.z - a_minus*gpr.z) / (a_plus - a_minus) + a_plus*a_minus*(gpr.x - gpl.x) / (a_plus - a_minus);
	re.y = (a_plus*gpl.y*vl - a_minus*gpr.y*vr) / (a_plus - a_minus) + a_plus*a_minus*(gpr.y - gpl.y) / (a_plus - a_minus);
	re.z = 0.5*(a_plus*gpl.z*vl - a_minus*gpr.z*vr) / (a_plus - a_minus) + a_plus*a_minus*(gpr.z - gpl.z) / (a_plus - a_minus);
	re.w = 0;
	if (vl == 0 && vr == 0)
	{
		re.x = 0;
		re.y = 0;
		re.z = 0;
	}
	return re;
}
__global__ void sand_advection(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

		

	
		gridpoint u_center = center - timestep*(jiex(center, east) - jiex(west, center)) - timestep*(jiey(center, south) - jiey(north, center));


		float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		/*if (u_center.x != 0)
		{
			if (u_center.y / u_center.x > LIM) u_center.y = u_center.x*LIM;
			if (u_center.z / u_center.x > LIM) u_center.z = u_center.x*LIM;
			if (u_center.y / u_center.x < -LIM) u_center.y = -u_center.x*LIM;
			if (u_center.z / u_center.x < -LIM) u_center.z = -u_center.x*LIM;
		}*/
		if (x == 0 || x == width - 1) u_center.y = 0;
		if (y == 0 || y == height - 1) u_center.z = 0;
		u_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);

	}

}
__global__ void sand_comp(gridpoint* grid_next, int width, int height, float timestep, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint center = tex2D(texture_grid, gridx, gridy);

		gridpoint north = tex2D(texture_grid, gridx, gridy - 1);

		gridpoint west = tex2D(texture_grid, gridx - 1, gridy);

		gridpoint south = tex2D(texture_grid, gridx, gridy + 1);

		gridpoint east = tex2D(texture_grid, gridx + 1, gridy);

	
		
		
		float hu_new = center.y;
		float hv_new = center.z;
		float s = center.x + center.w;
		float sw = west.x + west.w;
		float se = east.x + east.w;
		float sn = north.x + north.w;
		float ss = south.x + south.w;
		if (sw - se > cha || se - sw > cha/*||center.y>0*/)
		{
		float xieb = sqrt((sw - se)*(sw - se) + 2 * 2);
		float xsin = (sw - se) / xieb;
		float xcos = sw > se ? 2.0 / xieb : -2.0 / xieb;
		hu_new= hu_new + timestep *g*min(h_d,center.x)*(west.x+west.w-east.x-east.w)*(xsin - cha*0.5*xcos);
		}
		if (sn - ss > cha || sn - ss > cha/*||center.z>0*/)
		{
		float xieb = sqrt((sn - ss)*(sn - ss) + 2 * 2);
		float ysin = (sn - ss) / xieb;
		float ycos = sn > ss ? 2.0 / xieb : -2.0 / xieb;
		hv_new = hv_new + timestep *g*min(h_d,center.x)*(north.x+north.w-south.x-south.w)*(ysin - cha*0.5*ycos);
		}


		gridpoint u_center;

		u_center.x = center.x;
		u_center.y = hu_new;
		u_center.z = hv_new;
		float totalH = center.x + center.w;
		if (u_center.x < EPSILON || ((east.w > totalH) || (west.w > totalH) || (north.w > totalH) || (south.w > totalH)))
		{
			u_center.y = 0;
			u_center.z = 0;
		}
		/*if (center.x > 0 && u_center.x == 0)
		{
			u_center.x = EPSILON;
		}*/
		/*if (u_center.x != 0)
		{
			if (u_center.y / u_center.x > LIM) u_center.y = u_center.x*LIM;
			if (u_center.z / u_center.x > LIM) u_center.z = u_center.x*LIM;
			if (u_center.y / u_center.x < -LIM) u_center.y = -u_center.x*LIM;
			if (u_center.z / u_center.x < -LIM) u_center.z = -u_center.x*LIM;
		}*/
		if (x == 0 || x == width - 1) u_center.y = 0;
		if (y == 0 || y == height - 1) u_center.z = 0;
		u_center.w = center.w;
		grid2Dwrite(grid_next, gridx, gridy, pitch, u_center);
	}
	
}

__global__ void initGrid(gridpoint *grid, float *g2, int gridwidth, int gridheight, int pitch, int p2)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x < gridwidth && y < gridheight)
	{
		float a = tex2D(texture_landscape, x - 1, y - 1).y;//取值3~7

		gridpoint gp;
		gp.x = max(NN - a, 0.0f);//取值为0~2，NN=5的情况下
								 //gp.x = NN - a;
								 // gp.x = 0.0f;

		gp.y = 0.0f;
		gp.z = 0.0f;

		gp.w = a;//土地高度，3~7

		if (x >= 110 && x < 136 && y >= 110 && y < 136)
		{
			gp.x = 5;
		}

		float g2f;
		g2f = gp.x;
		grid2Dwrite(grid, x, y, pitch, gp);//将土地数据存入device_grid里
		g2[(y)*p2 + x] = g2f;

	}
}

__host__ __device__ vertex gridpointToVertex(gridpoint gp, float x, float y)
{
	float h = gp.x;//这是波浪的相对高度
	vertex v;
	v.x = x * 20.0f - 10.0f;
	v.z = y * 20.0f - 10.0f;
	v.y = h + gp.w;//实际浪高，即浪高+土地高度
	return v;
}

__host__ __device__ rgb gridpointToColor(gridpoint gp)
{
	rgb c;
	c.x = min(130 + (gp.x + gp.w - NN) * 200.0f, 255);
	c.y = min(80 + (gp.x + gp.w - NN) * 200.0f, 255);
	c.z = min(40 + (gp.x + gp.w - NN) * 200.0f, 255);
	c.w = 255 - max(-50 * gp.x + 50, 0);

	return c;
}//高度很高，就都是白色，高度恰好为NN时，颜色rgb值为（20，40，100）

__global__ void visualise(vertex* watersurfacevertices,
	rgb* watersurfacecolors, int width, int height)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		gridpoint gp = tex2D(texture_grid, gridx, gridy);

		watersurfacevertices[y * width + x] = gridpointToVertex(gp, x / float(width - 1), y / float(height - 1));
		watersurfacecolors[y * width + x] = gridpointToColor(gp);
	}
}


__global__ void addWave(gridpoint* grid, float* wave, float norm, int width, int height, int pitch)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;

		float waveheight = grid2Dread(grid, gridx, gridy, pitch).x;
		//第一次会读取initGrid里的gp.x
		//会使高度为0的水流变为NN
		//float waveheight = 0.0;
		float toadd = max(grid2Dread(wave, x, y, width) - 0.2f, 0.0f) * norm;//0~0.8变为0~0.6*norm
		waveheight += toadd;
		grid[gridx + gridy * pitch].x = waveheight;//将新的高度值存入grid，即device_grid_next

	}
}




__global__ void addWave1(gridpoint* grid, float* g2, int width, int height, int pitch, int p2, int shh)
{
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (x < width && y < height)
	{
		int gridx = x + 1;
		int gridy = y + 1;
		/*if (x < 110 && x >100 && y > 200&&y<220)
		{
		gridpoint center = tex2D(texture_grid, gridx, gridy);
		center.x = 45;
		grid[gridx + gridy * pitch]= center;
		}*/
		if (x <= 5 && y <= 5)
		{
			gridpoint center = tex2D(texture_grid, gridx, gridy);
			center.x = 3;
			//center.y = 2;
			//center.z = 2;
			grid[gridx + gridy * pitch] = center;
			g2[gridx + gridy*p2] = center.x;
		}
	}
}

void addWave(float* wave, float norm, int width, int height, int pitch_elements)
{
	cudaError_t error;
	size_t sizeInBytes = width * height * sizeof(float);

	error = cudaMemcpy(device_waves, wave, sizeInBytes, cudaMemcpyHostToDevice);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//device_waves存储波浪信息

	int x = (width + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int y = (height + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGrid(x, y);

	addWave << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, device_waves, norm, width, height, pitch_elements);

	error = cudaGetLastError();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	error = cudaThreadSynchronize();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	gridpoint *grid_helper = device_grid;
	device_grid = device_grid_next;
	device_grid_next = grid_helper;
	//更新数据

	error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, width + 2, height + 2, grid_pitch_elements * sizeof(gridpoint));
	//	CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);
	//新的波浪信息在texture_grid里
}

void initWaterSurface(int width, int height, vertex *heightmapvertices, float *wave)
{

	if (state != UNINTIALISED)
	{
		return;
	}
	int gridwidth = width + 2;
	int gridheight = height + 2;

	size_t sizeInBytes;
	size_t grid_pitch;
	cudaError_t error;
	size_t h_pitch;


	grid_channeldesc = cudaCreateChannelDesc<float4>();
	cudaChannelFormatDesc treshholds_channeldesc = cudaCreateChannelDesc<float>();
	cudaChannelFormatDesc reflections_channeldesc = cudaCreateChannelDesc<int>();

	//alloc pitched memory for device_grid
	error = cudaMallocPitch(&device_grid, &grid_pitch, gridwidth * sizeof(gridpoint), gridheight);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//	CHECK_NOTNULL(device_grid);
	error = cudaMallocPitch(&h_bef, &h_pitch, gridwidth * sizeof(float), gridheight);
	h_pitch_elements = h_pitch / sizeof(float);
	size_t oldpitch = grid_pitch;

	//alloc pitched memoty for device_grid_next
	error = cudaMallocPitch(&device_grid_next, &grid_pitch, gridwidth * sizeof(gridpoint), gridheight);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//	CHECK_NOTNULL(device_grid_next);

	//	CHECK_EQ(oldpitch, grid_pitch);

	grid_pitch_elements = grid_pitch / sizeof(gridpoint);

	//alloc pitched memory for landscape data on device
	size_t heightmap_pitch;
	error = cudaMallocPitch(&device_heightmap, &heightmap_pitch, width * sizeof(vertex), height);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//	CHECK_NOTNULL(device_heightmap);
	height_pitch_elements = heightmap_pitch / sizeof(vertex);

	// copy landscape data to device
	error = cudaMemcpy2D(device_heightmap, heightmap_pitch, heightmapvertices, width * sizeof(vertex), width * sizeof(vertex), height, cudaMemcpyHostToDevice);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	//device_heightmap存储1维土地高度图

	// bind heightmap to texture_landscape
	heightmap_channeldesc = cudaCreateChannelDesc<float4>();
	error = cudaBindTexture2D(0, &texture_landscape, device_heightmap, &heightmap_channeldesc, width, height, heightmap_pitch);
	//	CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);
	//texture_landscape有2维土地高度图

	// malloc memory for watersurface vertices
	sizeInBytes = width * height * sizeof(vertex);
	error = cudaMalloc(&device_watersurfacevertices, sizeInBytes);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// malloc memory for watersurface colors
	sizeInBytes = height * width * sizeof(rgb);
	error = cudaMalloc(&device_watersurfacecolors, sizeInBytes);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// malloc memory for waves
	sizeInBytes = height * width * sizeof(float);
	error = cudaMalloc(&device_waves, sizeInBytes);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);

	// make dimension
	int x = (gridwidth + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int y = (gridheight + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGrid(x, y);

	int x1 = (gridwidth + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	dim3 threadsPerBlock1(BLOCKSIZE_X, 1);
	dim3 blocksPerGrid1(x1, 1);

	int y1 = (gridheight + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock2(1, BLOCKSIZE_Y);
	dim3 blocksPerGrid2(1, y1);

	//init grid with initial values
	initGrid << < blocksPerGrid, threadsPerBlock >> > (device_grid, h_bef, gridwidth, gridheight, grid_pitch_elements, h_pitch_elements);

	//init grid_next with initial values
	initGrid << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, h_bef, gridwidth, gridheight, grid_pitch_elements, h_pitch_elements);

	error = cudaThreadSynchronize();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);


	//bind the grid to texture_grid
	error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch);
	//	CHECK_EQ(cudaSuccess, error) << "Error at line " << __LINE__ << ": " << cudaGetErrorString(error);
	//经过initGrid的土地高度信息在texture_grid里

	//add the initial wave to the grid
	addWave(wave, 10.0f, width, height, grid_pitch_elements);//修改此处的第二个参数，可以改变波浪的高度

	state = INITIALISED;
}
void computeNext_exp(int width, int height, vertex* watersurfacevertices, rgb* watersurfacecolors, int steps, bool renderonly)
{

	if (state != INITIALISED)
	{
		return;
	}
	int gridwidth = width + 2;
	int gridheight = height + 2;

	cudaError_t error;
	// make dimension
	int x = (width + BLOCKSIZE_X - 1) / BLOCKSIZE_X;
	int y = (height + BLOCKSIZE_Y - 1) / BLOCKSIZE_Y;
	dim3 threadsPerBlock(BLOCKSIZE_X, BLOCKSIZE_Y);
	dim3 blocksPerGrid(x, y);



	if (!renderonly)
	{
		for (int x = 0; x < steps; x++)
		{

			//simulateWaveStep << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			sand_advection << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			error = cudaThreadSynchronize();
			gridpoint *grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;
			error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
			
			sand_comp << < blocksPerGrid, threadsPerBlock >> > (device_grid_next, width, height, timestep, grid_pitch_elements);
			error = cudaThreadSynchronize();
			grid_helper = device_grid;
			device_grid = device_grid_next;
			device_grid_next = grid_helper;
			double tbj = clock();
			//float langg = 20 * sin(tbj*0.001) + 10;
			//addWave1 << < blocksPerGrid, threadsPerBlock >> > (device_grid, width, height, grid_pitch_elements, langg);
			error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
			//		
		}
		error = cudaBindTexture2D(0, &texture_grid, device_grid, &grid_channeldesc, gridwidth, gridheight, grid_pitch_elements * sizeof(gridpoint));
	}

	visualise << < blocksPerGrid, threadsPerBlock >> > (device_watersurfacevertices, device_watersurfacecolors, width, height);

	error = cudaGetLastError();
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	error = cudaThreadSynchronize();
	error = cudaMemcpy(watersurfacevertices, device_watersurfacevertices, width * height * sizeof(vertex), cudaMemcpyDeviceToHost);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
	error = cudaMemcpy(watersurfacecolors, device_watersurfacecolors, width * height * sizeof(rgb), cudaMemcpyDeviceToHost);
	//	CHECK_EQ(cudaSuccess, error) << "Error: " << cudaGetErrorString(error);
}

void destroyWaterSurface()
{
	if (state != INITIALISED)
	{
		return;
	}
	cudaUnbindTexture(texture_grid);
	cudaUnbindTexture(texture_landscape);
	cudaFree(device_grid);
	cudaFree(device_grid_next);

	cudaFree(device_heightmap);
	cudaFree(device_watersurfacevertices);

	cudaFree(device_waves);
	cudaFree(device_watersurfacecolors);
	state = UNINTIALISED;
}
